'use strict';

process.setMaxListeners(0);
global.__base = __dirname + "/";

const main = "sheduler_tochka";
const environment = "development";

const config = {
	development: {		
		  staticDir        : __dirname+'/static'
		, dir              :__dirname
		, port             : 2035
		, mongoUrl         : "mongodb://localhost/"+main
		, mongoCacheUrl    : "mongodb://localhost/"+main+"_cache"
		, agendaConnect    : 'mongodb://localhost/'+main+"_agenda"
		, mochaConnect     : 'mongodb://localhost/'+main+"_mocha"
		, silent 	       : false
		, debugRequests    : true
		, errorCatch       : true
		, mongoSessionsDB : {
		  	  db               : main+'_sessions'
			, host             : 'localhost'
			, autoReconnect    : true
			, safe             : false
			, url              : 'mongodb://localhost:27017/'+main+"_sessions"
		}, 
		mongoGFS : {
		  	  db               : main+'_files'
			, host             : 'localhost'
			, autoReconnect    : true
			, safe             : false
			, url              : 'mongodb://localhost:27017/'+main+"_files"
		},
		cookieConfig: {
			secret            : main+" secret",
		 	key               : main+".sid",
			cookie            : { path:'/', httpOnly: true, maxAge: 24*60*60*1000},
			maxAge            : 24*60*60*1000,
			secure            : true,
			saveUninitialized : true,
			resave            : true
	   },
	},
	staging: {},
	production: {}
}


module.exports = config[environment];