var 
	mongoose  = require('mongoose')
	, _       = require('lodash')
	, async   = require('async')
;

String.prototype.toSearchString = function() {
  var search = this;
  search = search.replace(/<.*?>/g,"");
  var replacer = {
      "й":"q","ц":"w","у":"e","к":"r","е":"t","н":"y","г":"u","ш":"i","щ":"o","з":"p","х":"[","ъ":"]",
      "ф":"a","ы":"s","в":"d","а":"f","п":"g","р":"h","о":"j","л":"k","д":"l","ж":";","э":"'","я":"z",
      "ч":"x","с":"c","м":"v","и":"b","т":"n","ь":"m","б":",","ю":".",".":"/"
  };  // Ввод маленьких букв
  search +=  search.toLowerCase().replace(/[А-яЁё]/g, function (x){ return replacer[x];});
  replacer = {
      "й":"q","ц":"w","у":"e","к":"r","е":"t","н":"y","г":"u","ш":"i","щ":"o","з":"p","х":"{","ъ":"}",
      "ф":"a","ы":"s","в":"d","а":"f","п":"g","р":"h","о":"j","л":"k","д":"l","ж":":","э":'"',"я":"z",
      "ч":"x","с":"c","м":"v","и":"b","т":"n","ь":"m","б":"<","ю":">",".":"?"
  };  // Ввод больших букв
  search +=  search.toLowerCase().replace(/[А-яЁё]/g, function (x){return replacer[x];});
  search = search+' '+search.replace(/[^0-9]/g,''); //  достаем все цифровые последовательности - для телефонов
  return search;
}



module.exports = function(schema, options) {
    schema.add({ search:  {type : String, default : '', trim : true, select:false}  }) // Не включать index

    schema.post('save', function (next,done) {
        var self = this, search = "";
        var fields = [];
        if (_.isFunction(self.schema.statics.searchableFields)){
            fields = self.schema.statics.searchableFields();
        } else {
            return done();
        }
        var config = schema.statics.cfg();
        var modelName = config.modelName;
        mongoose.model(modelName).findOne({_id:self._id},'-_id '+fields.join(' ')).lean().exec(function(err,model){
            var search = _.values(model).join(' ').toSearchString();
            mongoose.model(modelName).findByIdAndUpdate(self._id,{$set:{search:search}},done);
        })
    })
}