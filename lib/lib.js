var mongoose = require("mongoose");
var fs = require("fs");
var async = require("async");
var _ = require("lodash");

module.exports = (new function(){
	var self = this;

	self.listConfigs = function(dirName,done){
	    fs.readdir(dirName, function(err, files) {
	        var modules = [];
	        async.each(files, function(module, done) {
	            var dir = `${dirName}/${module}`;
	            fs.stat(dir, function(err, stat) {
	                if (!stat.isDirectory()) return done();
	                fs.stat(`${dir}/config.json`, function(err, statcfg) {
	                	if (!statcfg || !statcfg.isFile()) return done();
		                fs.readFile(`${dir}/config.json`,function(err, config) {
		                    config = JSON.parse(config)
		                    modules.push({
		                        name: module,
		                        config: config
		                    })
		                    return done(err);
		                })
		            })
	            })
	        }, function(err) {
	            return done(err,_.filter(modules,function(p){
	                return p.config.is_enabled;
	            }));
	        })
	    })
	}    

	self.require = function(fields){
		return function(req,res,next){
			var rInfo = ((req.method=="GET")? req.query:req.body) || {};
			var missed = [];
			fields.forEach(function(field){
				if (!rInfo[field]) missed.push(field);
			});
			if (!_.isEmpty(missed)) return next('Необходимо передать: '+missed.join(", "));
			next();
		}
	}

	self.parseBoolean = function (test){
		return (test===true || test==="true");
	}

	self.ignoreEmpty = function(val) {
  		if ("" === val || val==undefined) {
    		return null;
  		} else {
    		return val
  		}
	}

	self.parseNumber = function (test){
		var test = self.ignoreEmpty(test);
		if (test===null || test==='null') return 0;
		if (test===undefined || test==='undefined') return 0;
		if (isNaN(test)) return 0;
		var s = (test+'').replace(',','.')
		var result = parseFloat(s);
		if (isNaN(result)) return 0;
		return result;
	}

	self.random = function(){
		return mongoose.Types.ObjectId()+'';
	}

	return self;
})
