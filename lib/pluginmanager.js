var fs = require("fs");
var async = require("async");
var _ = require("lodash");


module.exports = (new function(){
	var self = this;
	
	self.listConfigs = function(dirName,done){
	    fs.readdir(dirName, function(err, files) {
			if (err) {
				throw new Error(`Reading modules fail (${dirName}).\n${JSON.stringify(err, null, 2)}`);
			}
	        var modules = [];
	        async.each(files, function(module, done) {
	            var dir = `${dirName}/${module}`;
	            fs.stat(dir, function(err, stat) {
	                if (!stat.isDirectory()) return done();
	                fs.stat(`${dir}/config.json`, function(err, statcfg) {
	                	if (!statcfg || !statcfg.isFile()) return done();
		                fs.readFile(`${dir}/config.json`,function(err, config) {
		                    config = JSON.parse(config)
		                    modules.push({
		                        name: module,
		                        config: config
		                    })
		                    return done(err);
		                })
		            })
	            })
	        }, function(err) {
	            return done(err,_.filter(modules,function(p){
	                return p.config.is_enabled;
	            }));
	        })
	    })
	}    

	self.dbExtend = function(done){
		self.listConfigs(__base+"modules",function(err,list){
			var turnedOn = _.map(_.filter(_.map(list,"config"),{is_enabled:true}),"id");
			var extenders = {};
			turnedOn.forEach(function(moduleId){
				var dbFile = __base+'modules/'+moduleId+'/db.js';
				if (fs.existsSync(dbFile)){
					extenders[moduleId] = require(dbFile);
				}
			})
			return done(err,extenders);
		})
	}

	self.enabledExtensions = function(done){
		self.enabledExtensionsCfgs(function(err,list){
			return done(null, _.map(list,"name"));
		})
	}


	self.enabledExtensionsCfgs = function(done){
		self.listConfigs ("modules",function(err,list){  
		      list = _.filter(list,{config:{is_enabled:true}});
		      return done(null,list);
	    })
	}
	

	return self;
})
