var _ = require("lodash");
var mongoose = require("mongoose");

module.exports = (new function(){
	var self = this;

	self.createOrUpdate = function(modelName){
		return function(req, res, next){
			var modelToSave = req.body.model;
			if (_.isString(modelToSave)){
				modelToSave = JSON.parse(modelToSave);
			}
			if (!_.includes(_.keys(mongoose.models),modelName)) return next("Модель не зарегистрирована");
			var _getModel = function(modelName,data,done){
				var model2work = mongoose.model(modelName);
				if (_.isEmpty(data._id)) {
					var instance = new model2work(data);
					return done(null,instance);
				}
				model2work.findOne({_id:data._id}).exec(function(err,existed){
					if (!existed) return done ("Объект не найден в базе данных");
					for (var key in data){
						existed[key] = data[key];
					}
					return done(null,existed);
				})
			}
			_getModel(modelName,modelToSave,function(err,toSave){
				console.log(toSave,modelName);
				if (err) return next (err);
				toSave.save(function(err){
					if (err) return next (err);
					return res.json({_id:toSave._id});
				})
			})
		}
	}

	self.delete = function(modelName){
		return function(req, res, next){
			if (!_.includes(_.keys(mongoose.models),modelName)) return next("Модель не зарегистрирована");
			mongoose.model(modelName).findOne({_id:req.body._id}).exec(function(err,toRemove){
				if (_.isEmpty(toRemove)) return next ("Объект не найден в базе данных");
				toRemove.remove(function(err){
					if (err) return next (err);
					return res.json({});
				})
			})
		}
	}

	return self;
})

