var _ = require("lodash");

RoleHelper = (new function(){
	var self = this;

	self.isAdmin = function(req){
		return self.hasRole(req,["admin"]);
	}

	self.isManager = function(req){
		return self.hasRole(req,["manager"]);
	}

	self.isTeamLead = function(req){
		return self.hasRole(req,["teamlead"]);
	}

	self.hasRole = function(req, roles){
		return !_.isEmpty(_.intersection(req.user.Roles||[],roles))
	}

	self.departmentId = function(req, askedId){
		console.log("askedId",askedId);
		if (self.isAdmin(req)){
			return askedId;
		}

		return req.user.DepartmentId;
	}


	return self;
})

module.exports = RoleHelper;