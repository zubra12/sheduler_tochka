var _ = require("lodash");

module.exports = function(allowedRoles){

	return function(req, res, next){
		if (!_.isArray(allowedRoles)) allowedRoles = [allowedRoles];
		if (!_.isEmpty(req.user) && !_.isEmpty(_.intersection(allowedRoles,req.user.Roles))){
			return next();
		}
		return next("Нужны права: "+allowedRoles.join(", "));
	}
}