var mongoose = require('mongoose');
var sessions = require('express-session');
var _ = require("lodash");
var event = require('events').EventEmitter;

var SocketManager = (new function(){

	var self = this;

	self.socket = null;

	self.Events = new event();

	self.listners = {};

	self.registerListner = function(name,cb){
		self.listners[name] = cb;
	}

	self.emitEventAll = function(eventName,data){
		self.socket.sockets && self.socket.sockets.emit(eventName,data);
	}

	self.isSocketAlive = function(socketId){
		return self.socket.sockets.sockets[socketId].connected;
	}

	self.emitTo = function(_id,eventName,data){
		var sockets = self.userSockets(_id);
		if (sockets.length){
			sockets.forEach(function(socketId){
				self.socket.to(socketId).emit(eventName,data);
			})
		}
	}

	self.init = function(cookies, server){
		var sock = require('socket.io');
		self.socket = sock(server);
		var sessionMiddleware = sessions(cookies);
		var userMiddlware = function(req,res,next){
		  if (req.session && req.session.passport && req.session.passport['user']){
		      mongoose.model('user').findOne({_id:req.session.passport['user']},'NameUser UserPhoto').lean().exec(function(err,U){
		          req.session.user = U;
		          req.session.save(next);
		      })
		  } else {
		      req.session.user = null;
		      next();
		  }
		}
		self.socket.use((socket, next) => sessionMiddleware(socket.request, socket.request.res, next));
		self.socket.use((socket, next) => userMiddlware(socket.request, socket.request.res, next));
		self.socket.sockets.on("connection", function(socket) {
			if (socket.request.session.user && socket.request.session.user._id){
				for (var ev in self.listners){
					socket.on(ev,self.listners[ev].bind(socket));
				}
				self.Events.emit("userconnected",socket);
			}
			socket.on('disconnect', function () {
				self.Events.emit("userdisconnected",this);
			})
		})
	}

	self.userSockets = function(_id){
		var result = [];
		if (!self.socket.sockets || !self.socket.sockets.sockets) return result;
		var sockets = self.socket.sockets.sockets;
        for(var socket in sockets) {
        	var session = sockets[socket].request.session;
        	if (session.user && session.user._id==_id){
        		result.push(socket);
        	}
        }
        return result;
	}

	
	return self;
})

module.exports = SocketManager;
