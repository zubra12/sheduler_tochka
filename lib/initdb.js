var mongoose  = require('mongoose');
var lib = require('./lib.js');
var fs = require("fs");
var _ = require("lodash");

var ShemaCompiller = (new function(){
	
	var self = this;

	self.cache = {};

	self.get = function(done){
		if (!_.isEmpty(self.cache)) return done(null,self.cache);
		var models = require(__base+"lib/models.js");
		models(function(err, modelConfig, modelExtend){
			var update = {};
			for (var moduleName in modelExtend){
				for (var className in modelExtend[moduleName].schema){
					if (!update[className]) update[className] = [];
					update[className].push(modelExtend[moduleName].schema[className]);
				}
			}
			for (var modelName in modelConfig){
				var info = modelConfig[modelName];
				var fields = info.fields;
				var booleans = [], numeric = [], editFields = [], name = "", fieldsInfo = {};
				for (var fieldName in fields){
					var fieldCfg = fields[fieldName];
					if (!_.isEmpty(fieldCfg.type)){
						if (fieldCfg.type.name=="Boolean") {
							fieldCfg.set = lib.parseBoolean;
						}
						if (fieldCfg.type.name=="ObjectId") {
							fieldCfg.set = lib.ignoreEmpty;
						}
						if (fieldCfg.type.name=="Number"){
							fieldCfg.set = lib.parseNumber;	
						}
					}
					if (_.isBoolean(fieldCfg.select) && fieldCfg.select==false){
						;
					} else {
						editFields.push(fieldName);	
						fieldsInfo[fieldName] = fieldCfg;
					}	   				
				}
				var schema = mongoose.Schema(fields);
				schema.statics.cfg = schema.methods.cfg =  function(info){
					return function(){
						return info;	
					}			
				}(_.merge({},{modelName:modelName,editFields:editFields, fields:fieldsInfo}));
				if (update[modelName]){
					update[modelName].forEach(function(call){
						schema = call(schema);
					})
				}			
				self.cache[modelName] = schema;
			}
			return done(null, self.cache);
		});		

	}

	return self;
})




module.exports = function(done){

	var removeModel = function(modelName) {
		delete mongoose.models[modelName];
		delete mongoose.modelSchemas[modelName];
	};

	ShemaCompiller.get(function(err,shemas){
		for (var modelName in shemas){
			var schema = shemas[modelName];
			removeModel(modelName);
			mongoose.model(modelName, schema);
		}
		return done && done();
	})
}

