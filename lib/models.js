var _ = require ("lodash");
var async = require ("async");
var fs = require("fs");
var Pluginmanager = require(__base+"lib/pluginmanager.js");

module.exports = function(done){

	Pluginmanager.dbExtend(function(err,extenders){
		var fields = {};
		if (!_.isEmpty(extenders)){
			for (var moduleId in extenders){
				var add = extenders[moduleId].models;
				for (var modelName in add){
					fields[modelName] = _.merge(fields[modelName]||{},add[modelName].fields);
				}
			}
		}
		var result = {};
		for (var key in fields){
			if (!result[key]) {
				result[key] = {fields:fields[key]};
			} else {
				result[key].fields = _.merge(result[key].fields,fields[key]);	
			}			
		}
		return done(err,result, extenders);
	})
}




