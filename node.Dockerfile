FROM node:9

WORKDIR /app

COPY package.json .
COPY config.src.js config.js
COPY app.js .
COPY lib lib
COPY static/build static/build
COPY modules modules

RUN npm install

ENV PORT=2035
EXPOSE 2035/tcp

CMD [ "node", "app.js" ]
