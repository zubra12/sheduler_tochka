define(function() {
    return {
        pageGroups: [{
            "id": "5353e572-f721-7446-b00d-e08e08de713c",
            "name": "Default group",
            "pages": [
                // 28 - да, 12 - нет , всего 40
                { "id": "114bde50-a9c3-9723-143d-c85fbae119b9", "name": "1.0 Авторизация" },
               { "id": "fb99848b-1a61-edd4-7882-b4837ab1bb09", "name": "2.0 Стартовый" },
                { "id": "92330029-dd31-442e-4044-77485a627001", "name": "2.1 Создание подразделения" },
                { "id": "1810b439-3c24-147c-91c1-c8ec492d8693", "name": "2.2 Подразделения созданы" },
                { "id": "79bed6fb-2a72-9f9f-86c9-57725d0b98f1", "name": "2.3 Создание круга" },
                { "id": "3fa6f3ea-0870-ceb9-15eb-ec9c26a87c1c", "name": "2.4 Круги созданы" },
                { "id": "b985676b-1afa-0ed2-77dd-a3e29c15aead", "name": "2.5 Создание группы" },
                { "id": "e4172175-4c84-4548-5942-041cf5dc34af", "name": "3.0 Начал о работы со сменами" }, 
                {"id": "6d5fd0f4-2c36-f52d-aea1-ca05a87e8c2d","name": "3.1 Создание смены"},
                { "id": "3dedf693-0a11-eb63-4f74-815139fb84dc", "name": "3.2 Список смен" },
                { "id": "031f3f20-9596-2a98-1680-83d9bfef3e17", "name": "3.4 Редкатирование смены модалка" }, 
                { "id": "028de129-fa3a-1623-9b2d-6f40c961e3d8", "name": "3.5 Удаление смены модалка" }
                { "id": "751f818e-dfd5-cec7-84b9-65bf06b6823f", "name": "4.0 Создание сотрудника" }, 
                { "id": "7a8e701e-b76f-15fe-a630-e2f5104c2f12", "name": "4.1 Список сотрудников" }, 
                { "id": "7515519c-2327-c8f8-ff62-5fe10443fca0", "name": "4.2 Выбор смен" }, 
                { "id": "768bf359-88e2-c177-49c2-e14524c09564", "name": "4.3 Смены выбраны" }, 
                { "id": "188bbf40-64a5-eb54-a677-0cae2e29e8d2", "name": "4.4 Настройка отпусков" }, 
                { "id": "74fab5a1-5c53-142a-08f5-7ef4980938d4", "name": "4.5 Настройка отпусков 2" }, 
                { "id": "8e99b3ec-351b-d2fc-f0e5-556d1d3237b8", "name": "4.6 Отпуска назначены" }, 
                { "id": "1863d1ee-e8bf-0cff-7205-e238f0c1f6a4", "name": "5.0 Проставлены критичные смены" }, 
                { "id": "5f1405e1-14a1-d3a3-b773-b1c7fd7dd4ea", "name": "5.1 Проставлены обычные смены" }, 
                { "id": "ae894b65-1b88-9477-5255-a96c362aeecc", "name": "6.0 Готовый график" }, 
                { "id": "5ecc21e9-3d66-8d0c-1568-9c3d23bdec14", "name": "6.5 Готовый график переработка" }, 
                { "id": "2d55409c-888f-298b-ebc0-c33d5212c2d7", "name": "5.5 Сохранение шаблона" }, 
                { "id": "8215d039-b4be-58cc-610b-6e5a0728a7d8", "name": "5.6 Загрузка шаблона" }, 
                { "id": "ebc3d9e3-aa7b-44e9-d71a-e7d00b8eaac2", "name": "5.2 Обязательная смена перенос" }, 
                { "id": "860ef5a5-8fa8-0054-5325-448e78ec1631", "name": "5.3 Состав смен" }, 
                { "id": "eee09832-2253-f677-a445-737b31afa2fb", "name": "5.4 Уведомления" }, 
                { "id": "355dbcd6-9d85-3d0b-eff4-858cfe29ae9d", "name": "6.2 Удаление смены из графика" },
                 { "id": "8d9ccc1b-c4a2-da2d-350e-13a0eeeb5335", "name": "6.1 Меню полного дня" }, 
                 { "id": "9d6ecba8-3dc0-19dc-1a34-0c7d2ed5d6c6", "name": "4.7 Настройка особых смен" }, 
                 { "id": "1b733ecb-099d-c291-be39-e782dc84d447", "name": "4.8 Особая смена" }, 
                 { "id": "b045484a-f1ca-9244-a1a6-f81dc7079988", "name": "6.4 Создание смены готовый график" }, 
                 { "id": "d4c40e45-0e86-f28d-8221-a50bcbf37084", "name": "6.3 Меню пустого дня" },
                {"id": "b9fbf06f-4f64-53cc-8d78-a382dc50ef18", "name": "4.9 Закрыть день" }, 
                 { "id": "9e845ec9-840b-ed87-cb3f-5c1e30b65ae4", "name": "4.10 Особые смены проставлены" }, 
                 { "id": "cfcd6f49-7d0b-4821-648b-97b988b4961e", "name": "4.11 Редактирование меню" }, 
                 { "id": "84ff8bc2-72e5-536f-d589-86e85496746c", "name": "4.12 Редактирование модалка" }, 
                 { "id": "2164d182-7115-9998-c39b-1be0030a5998", "name": "4.13 Удаление модалка" },
                 { "id": "f8f0a22a-7384-f777-d18e-6861d4bd0be7", "name": "3.3 Список смен меню" }
            ]
        }],
        downloadLink: "//services.ninjamock.com/html/htmlExport/download?shareCode=JS15QTx&projectName=Управление расписанием",
        startupPageId: 0,

        forEachPage: function(func, thisArg) {
            for (var i = 0, l = this.pageGroups.length; i < l; ++i) {
                var group = this.pageGroups[i];
                for (var j = 0, k = group.pages.length; j < k; ++j) {
                    var page = group.pages[j];
                    if (func.call(thisArg, page) === false) {
                        return;
                    }
                }
            }
        },
        findPageById: function(pageId) {
            var result;
            this.forEachPage(function(page) {
                if (page.id === pageId) {
                    result = page;
                    return false;
                }
            });
            return result;
        }
    }
});