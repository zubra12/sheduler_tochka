FROM nginx

WORKDIR /app

COPY nginx/nginx.conf /etc/nginx/conf.d/default.conf
COPY static static
COPY modules modules

EXPOSE 80/tcp 443/tcp
