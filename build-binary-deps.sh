#!/bin/sh

mod="$1"
if [ "$#" -gt 0 ]; then
    shift
fi
args="$@"
module=manpower-distributer

mkdir -p build/$module
cd modules/$module
if [ "$mod" = "--test" ]; then
    CARGO_TARGET_DIR=../../build/$module cargo test -- --nocapture
elif [ "$mod" = "--debug" ]; then
    RUST_BACKTRACE=1 CARGO_TARGET_DIR=../../build/$module cargo run -- $args
else
    CARGO_TARGET_DIR=../../build/$module cargo build --release
fi
cd ../..
mkdir -p bin
cp build/$module/release/$module bin
