var HandsonComponent = function(dataobjects, columns, selector){
    var self = this;


    self.table = null;
    self.data = dataobjects;
    self.columns = columns;
    self.selector = selector;
    self.FixedWidths = [];
    self.Editable = [];

    self.AddChange = function(changes, source){
        switch(source) {
            case 'edit':
            case 'autofill':
            case 'paste':
            case 'undo':
                changes.forEach(function(Change){
                    self.data[Change[0]][Change[1]](Change[3]);
                })
            default:
                console.log(">>>",changes, source);
            break;
        }
    }

    self.config = {
        rowHeaders:true,
        colHeaders:true,
        autoRowSize:true,
        minSpareRows: 0,
        minSpareCols: 0,
        currentColClassName: 'currentCol',
        currentRowClassName: 'currentRow',
        fixedRowsTop: 1,
        fixedColumnsLeft: 1,
        manualColumnResize: true,
        afterChange: self.AddChange,
        trimDropdown: false,
    }

    self.GetSelectRender = function(Template){
        var Model = Template.Model;
        return  function(instance, td, row, col, prop, value, cellProperties) {
            $(td).html(Catalogue.GetHtmlWithCode(Model,value));
        };
    }


    self.RenderTries = 0;
    self.RenderTimeout = null;
    self.Render = function(){
        if ((++self.RenderTries)>10) return;
        if (self.RenderTimeout) clearTimeout(self.RenderTimeout);
        if (_.isEmpty($(self.selector))){            
            self.RenderTimeout = setTimeout(self.Render,50);
        } else {
            self._render();
            self.RenderTries = 0;
        }
    }

    self._render = function(){
        var Columns = [], Template = _.first(self.data), Fields = _.keys(self.columns);
        Fields.forEach(function(Field){
            var Type = Template.Types[Field].Type, D = {data:Field,type:'text',readOnly:self.Editable.indexOf(Field)==-1 ? true:false};
            switch (Type){
                case "Number":
                    D.type = 'numeric';
                    if (Template.Mask[Field]){
                        D.format = Template.Mask[Field];
                    }
                break;
                case "Select":
                    delete (D.type);
                    D.renderer = self.GetSelectRender(Template.Types[Field]);
                    if (Template.Mask[Field]){
                        D.format = Template.Mask[Field];
                    }
                break;
                default:
                    console.log("Unknown type:",Type);
            }
            Columns.push(D);
        })
        MModels.Create("valutarate").Types.Value1
        var settings = _.merge(_.clone(self.config),{
            data:_.map(self.data,function(d){
                return d.toJS();
            }),
            columns:Columns,
            maxRows:self.data.length,
            colHeaders:_.values(self.columns)
        })
        if (self.table){
            console.log("self.table");
            self.table.updateSettings({data:settings.data});
        } else {
            console.log("! self.table = Generate new");
            self.table =  new Handsontable($(self.selector)[0], settings);
            new HandsonTableHelper.WidthFix(self.table,200,300,self.FixedWidths);
        }
        

        
    }


    return self;
}






String.prototype.hashCode = function() {
    var hash = 0,
        i, chr;
    if (this.length === 0) return hash;
    for (i = 0; i < this.length; i++) {
        chr = this.charCodeAt(i);
        hash = ((hash << 5) - hash) + chr;
        hash |= 0; // Convert to 32bit integer
    }
    return hash;
};


var FormComponents = (new function() {
    var self = new Module("formcomponents");



    return self;
})

ModuleManager.Modules.FormComponents = FormComponents;


ko.bindingHandlers["mdl-component"] = {
    init: function(element, valueAccessor, allBindingsAccessor) {
        componentHandler.upgradeElement(element);
    }
}


ko.bindingHandlers["mdl-datepicker"] = {
    lang: {
        cancel: 'Отмена',
        clear: 'Очистить',
        done: 'Применить',
        previousMonth: '‹',
        nextMonth: '›',
        months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        monthsShort: ['Янв', 'Фвр', 'Мрт', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Нбр', 'Дкб'],
        weekdays: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"],
        weekdaysShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        weekdaysAbbrev: ['В', 'П', 'В', 'С', 'Ч', 'П', 'С']
    },
    init: function(element, valueAccessor, allBindingsAccessor) {
        var self = ko.bindingHandlers["mdl-datepicker"];
        var value = ko.utils.unwrapObservable(valueAccessor());
        $(element).datepicker({
            firstDay: 1,
            format: "dd.mm.yyyy",
            i18n: self.lang
        });
    }
}

ko.bindingHandlers["mdl-select"] = {
    init: function(element, valueAccessor, allBindingsAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        $(element).formSelect();
    },
    update: function(element, valueAccessor, allBindingsAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        $(element).val(value);
        $(element).formSelect();
    }
}


ko.bindingHandlers["mdl-float-buttons"] = {
    update: function(element, valueAccessor, allBindingsAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        setTimeout(function() {
            $(element).floatingActionButton();
        }, 0);
    }
}




ko.bindingHandlers["mdl-collapsible-expandable"] = {
    init: function(element, valueAccessor, allBindingsAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        setTimeout(function() {
            var instance = M.Collapsible.init(element, {
                accordion: false
            });

        }, 0);
    }
}

ko.bindingHandlers.letterAvatar = {
    palette: ["#E57373", "#BA68C8", "#9575CD", "#7986CB", "#64B5F6", "#4FC3F7", "#4DD0E1", "#4DB6AC", "#81C784", "#AED581", "#DCE775", "#A1887F", "#90A4AE"],
    size: 40,
    color: function(str) {
        var self = ko.bindingHandlers.letterAvatar;
        var index = _.sum(_.map((str + "").split(/.*?/g), Number));
        if (index > (self.palette.length - 1)) {
            return self.color(index);
        }
        return self.palette[index];
    },
    generate: function(size, name) {
        var self = ko.bindingHandlers.letterAvatar;
        var color = self.color((name + "").hashCode()); //self.palette[Math.floor(Math.random() * self.palette.length)];          
        var svg = self.createSVGTag(size + "px", size + "px", color);
        svg.append(self.createTextTag(name, size));
        var image = $('<img class="circle responsive-img" src="' + self.inlineImg(svg) + '" height="' + size + 'px" width="' + size + 'px" />');
        return image;
    },
    createSVGTag: function(width, height, color) {
        return $("<svg/>", {
            "attr": {
                'xmlns': 'http://www.w3.org/2000/svg',
                'pointer-events': 'none',
                'width': width,
                'height': height
            },
            "css": {
                'background-color': color,
                'width': width,
                'height': height,
            }
        })
    },
    createTextTag: function(abbr, size) {
        var textSize = {
            40: 17,
            100: 27
        } [size] || 17;
        return $('<text/>', {
            "attr": {
                'text-anchor': "middle",
                'x': '50%',
                'y': '50%',
                'dy': '0.35em',
                'pointer-events': 'auto',
                'fill': "#FFF",
            },
            "css": {
                'font-weight': "400",
                'font-size': textSize + "px",
            }
        }).html(abbr);
    },
    inlineImg: function(svg) {
        return 'data:image/svg+xml;base64,' + window.btoa(
            unescape(encodeURIComponent(
                $('<div>').append(svg.clone()).html()
            ))
        );
    },
    nameToAbbr: function(name) {
        var words = name.split(/[\s.,;\-_]/);
        var ret = words.reduce(function(initials, word) { return initials + word.charAt(0); }, '') || '';
        return ret.substr(0, 2).toUpperCase();
    },
    init: function(element, valueAccessor, allBindingsAccessor) {
        var self = ko.bindingHandlers.letterAvatar;
        var value = ko.utils.unwrapObservable(valueAccessor());
        var img = _.isFunction(value.img) ? value.img() : null;
        var size = value.size || self.size;
        var abbr = self.nameToAbbr(value.name());
        if (_.isEmpty(img)) {
            var img = self.generate(size, abbr);
            $(element).empty().append(img);
            $(element).attr("name-abbr", abbr);
        }
    },

    update: function(element, valueAccessor, allBindingsAccessor) {
        var self = ko.bindingHandlers.letterAvatar;
        var value = ko.utils.unwrapObservable(valueAccessor());
        var imgFile = _.isFunction(value.img) ? value.img() : null;
        var size = value.size || self.size;
        var abbr = self.nameToAbbr(value.name());
        if (_.isEmpty(imgFile) && !_.isObject(imgFile)) {
            if ($(element).attr("name-abbr") != abbr) {
                var img = self.generate(size, abbr);
                $(element).empty().append(img);
                $(element).attr("name-abbr", abbr);
            }
        } else {
            if (_.isObject(imgFile)) {
                FileAPI.Image(imgFile).preview(size).get(function(err, realImg) {
                    $(realImg).addClass("circle responsive-img");
                    $(element).empty().append(realImg);
                });
            } else if (_.isString(imgFile) && !_.isEmpty(imgFile)) {
                var realImg = $('<img/>', {
                    "attr": {
                        'src': imgFile,
                        'width': size + "px",
                        'height': size + "px"
                    },
                    "class": "circle responsive-img"
                })
                $(element).empty().append(realImg);
            }

        }
    }
}


ko.bindingHandlers.fileUpload = {
    init: function(element, valueAccessor, allBindingsAccessor) {
        FileAPI.event.on(element, "change", function(evt) {
            var file = FileAPI.getFiles(element)[0];
            valueAccessor()(file);
        })
    }
}



ko.bindingHandlers.wysiwyg = {
    init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
        var wysi = $(element).trumbowyg({
            btns: ['viewHTML', '|', 'strong', 'em', '|', 'unorderedList', 'orderedList', "|", "removeformat", 'fullscreen'],
            autogrow: true,
            lang: 'ru'
        });
        $(element).trumbowyg('html', ko.unwrap(valueAccessor()));

    },

    update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
        var updates = $(element).keyup(function() {
            if (ko.isWritableObservable(valueAccessor())) {
                valueAccessor()($(element).trumbowyg('html'));
            };
        });
        var updates = $(element).focusout(function() {
            if (ko.isWritableObservable(valueAccessor())) {
                valueAccessor()($(element).trumbowyg('html'));
            };
        });
    }
};



ko.bindingHandlers.colorpicker = {
    init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        $(element).colorpicker({
            component: '.btn',
            format: 'hex',
            color: value
        }).on('changeColor', function(ev) {
            valueAccessor()(ev.color.toHex());
        });
    },
    update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        $(element).colorpicker('setValue', value)
    }
};


ko.bindingHandlers.sortableTrs = {
    init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        console.log(value);
        $(element).sortable();
        $(element).on("sortchange", function(){
            console.log($(element).sortable("toArray",{attribute :"templateId"}));
        })
    },
    update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
        $(element).sortable("refresh");
    }
};


ko.bindingHandlers.modalBinding = {
    init: function(element, valueAccessor, allBindingsAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        $(element).modal(value);
    }
}


ko.bindingHandlers.time_mask = {
    init: function(element, valueAccessor, allBindingsAccessor) {
        var AddMask = { 'H': "[0-2]", 'h': "[0-9]", 'M': "[0-5]", 'm': "[0-9]" };
        for (var K in AddMask) {
            $.mask.definitions[K] = AddMask[K];
        }
        var value = ko.utils.unwrapObservable(valueAccessor());
        $(element).mask(value, {
            completed: function() {
                if (value == "Hh:Mm") {
                    var currentMask = $(this).mask();
                    if (isNaN(parseInt(currentMask))) {
                        $(this).val("");
                    } else if (parseInt(currentMask) > 2359) {
                        $(this).val("23:59");
                    };
                }
            }
        })
    },
};

ko.bindingHandlers.mask = {
    init: function(element, valueAccessor, allBindingsAccessor) {
        var mask = ko.utils.unwrapObservable(valueAccessor());
        setTimeout(function() {
            $(element).mask(mask);
        }, 0)
    }
};