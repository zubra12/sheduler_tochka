var TopMenu = (new function () {

    var self = this;

    self.menu = ko.observable();

    self.init = function(done){
        var configs = _.map(ModuleManager.ModulesConfigs,"config");
        var topMenuModules = _.filter(_.sortBy(configs,{sort_order:1}),{top_menu:true});
        var templates = [];
        topMenuModules.forEach(function(cfg){
            templates.push("topmenu_"+cfg.id);
        })

        self.menu(templates);
        //self.menu(menu);
        return _.isFunction(done) && done();
    }

    return self;
})

ModuleManager.Modules.TopMenu = TopMenu;