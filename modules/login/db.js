var mongoose = require("mongoose");
var  crypto   = require('crypto');
var  _   = require('lodash');

module.exports = {
	models:{
		 "user": {
	        "fields": {
	            "NameUser": {
	                "type": String,
	                "default": "Noname User",     
	                "role":"name"           
	            },
	            "LoginUser": {
	                "type": String,
	                "default": null
	            },
	            "Mail": {
	                "type": String,
	                "default": ""
	            },
	            "NewPassword": {
	                "type": String,
	                "default": null
	            },
	            "PassHash": {
	                "type": String,
	                "default": null,
	                "select":false
	            },
	            "PassSalt": {
	                "type": String,
	                "default": "",
	                "select":false
	            },
	            "Birthday": {
	                "type": Date,
	                "default": "1970-01-01T00:00:00.000Z"
	            },
	            "MobilePhone": {
	                "type": String,
	                "default": null,
	                "trim" : true,
	                "mask":"+7 (999) 999-9999"
	            }
        	} 
        } 
	},
	schema: {
		user: function(schema){
			schema.pre('save',function(next, done){
				var self = this;
				var similarQ = [];
				if (!_.isEmpty(self.LoginUser)){
					similarQ.push({LoginUser:self.LoginUser,_id:{$ne:self._id}});
				}
				if (!_.isEmpty(self.Mail)){
					similarQ.push({Mail:self.Mail,_id:{$ne:self._id}});
				}
				mongoose.model("user").findOne({$or:similarQ},"-_id LoginUser Mail").exec(function(err,similar){
					if (similar) {
						var Errs = [];
						if (!_.isEmpty(similar.Mail) && similar.Mail==self.Mail)return next("почта "+similar.Mail+' уже используется');
						if (!_.isEmpty(similar.LoginUser) && similar.LoginUser==self.LoginUser) return next("логин "+similar.LoginUser+' уже используется');
					}
					return next();
				})
			});

			schema.path('LoginUser').set(function(val){  return (val+'').toLowerCase().trim(); });
			schema.path('Mail').set(function(val){ return (val+'').toLowerCase().trim(); });

			schema.statics.SearchableFields = function(){
				return ["NameUser","LoginUser","JobTitle","Phone","Mail"];
			}

			schema.virtual('password')
			.set(function(password) {
				if (password) {
					this._plainPassword = password;
					this.PassSalt = Math.random() + '';
					this.PassHash = this.encryptPassword(password);
				}
			})
			.get(function() { return this._plainPassword; });
			schema.method({
				encryptPassword : function(password) {
				  return crypto.createHmac('sha512', this.PassSalt).update(password).digest('hex'); 
				},
				checkPassword : function(password) {
				  if ((this.PassHash)&&(password))
				  		return this.encryptPassword(password+'') === this.PassHash;
				  else return false;
				}
			});
			return schema; 
		}
	}
}

