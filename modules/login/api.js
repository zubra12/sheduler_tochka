var _ = require('lodash');
var async = require('async');
var router   = require('express').Router();
var passport = require(__base+'lib/passport.js');


router.get('/me',  function(req,res){
	res.json({me:req.user});
});

router.post('/logout',function(req,res){
	req.session.destroy();
	return res.end();
});



router.post('/login', function(req, res, next) {
	passport.authenticate('local', function(err, user, info) {
		if (info && info.message) {
			return res.json({err:info.message})
		} else {
			if (!user)  return next('usernotfound');
			req.logIn(user, function(err) {
				return res.json({status:'ok'});
			});
		}
	})(req, res, next);
});

module.exports = router;