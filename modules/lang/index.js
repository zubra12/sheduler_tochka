var Lang = (new function () {

    var self = new Module("lang");

    self.strs = {
        default: {
        }
    }

    self.isInited = ko.observable(false);

    self.init = function (done) {
        if (self.isInited()) return done();
        $.getJSON(self.base + "translates", function (data) {
            if (data.err) return done();
            for (var type in data) {
                self.register(type, data[type]);
            }
            self.isInited(true);
            return done();
        })
    }

    self.register = function (type, obj) {
        if (!self.strs[type]) self.strs[type] = {};
        self.strs[type] = _.merge(self.strs[type], obj);
        self.strs.default = _.merge(obj, self.strs.default);
    }

    self.tr = function (prefix, str) {
        if (typeof str == 'function') str = null;
        if (!str) {
            str = prefix;
            prefix = null;
        }
        if (_.isArray(str)) {
            var result = [];
            str.forEach(function (s) {
                result.push(self.tr(s));
            })
            return result;
        } else {
            var result = str;
            try {
                result = self.strs[prefix][str];
            } catch (e) {
                ;
            }
            if (result == str && !_.isEmpty(self.strs.default[str])) result = self.strs.default[str];
            if (_.isEmpty(result)) result = str;
            if(prefix && typeof prefix === "string"){
                str = prefix + '.' + str;
            }
            return result;
        }
    }

    return self;
})

var tr = Lang.tr;

ko.bindingHandlers.lang = {
    update: function (element, valueAccessor, allBindingsAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        var params = ko.utils.unwrapObservable(allBindingsAccessor());
        if (!Lang.isInited()) return value;
        var model = params.model;
        var text = Lang.tr(model, value);
        $(element).html(text);
    }
};



ModuleManager.Modules.Lang = Lang;
