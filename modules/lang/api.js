var _ = require('lodash');
var async = require('async');
var router   = require('express').Router();
var plugins = require(__base+"lib/pluginmanager.js");
var fs = require("fs");

var CacheManager = (new function(){
	var self = this;

	self.cachePlace = __base+"static/build/translates.json";

	self.get = function(done){
 		fs.stat(self.cachePlace, function(err, stat) {
 			if (err) return done(err);
 			fs.readFile(self.cachePlace,function(err,content){
 				 return done(err,JSON.parse(content.toString()));
 			})
	    })
	}

	self.set = function(lang,done){
		fs.writeFile(self.cachePlace,JSON.stringify(lang,null,2),function(err){
			return done(err);
		});
	}

	self.build = function(done){
	  plugins.enabledExtensions(function(err,list){
	    var exec = require('child_process').exec,files = [], fs = require("fs");
	    exec('find '+ __base+'modules/*/lang.json', function(err, stdout, stderr) {
	        files = stdout.split('\n');
	        var result = {};
	        async.each(_.compact(files),function(file,done){
	          var module = _.last(_.first(file.trim().split("/lang.json")).split("/"));
	          if (list.indexOf(module)==-1) return done();
	          fs.readFile(file,function(err,content){
	            result[module] = JSON.parse(content.toString());
	            return done();
	          })
	        },function(err){
	           if(err) return done(err);
	           self.set(result,done);
	        })
	    });
	  })
	}

	return self;
})

CacheManager.build(function(err){
	//console.log(">> Translates are builded");
});



router.get('/translates', function(req,res){
	CacheManager.get(function(err,lang){
		return res.json(lang);
	})
})




module.exports = router;