var HomePage = (new function () {

    var self = this;

    self.plugins = ko.observableArray();

    self.init = function(done){
       self.plugins(_.sortBy(_.filter(_.map(ModuleManager.ModulesConfigs,"config"),function(cfg){
            if (!cfg.start_page) return false;
            var cl = ModuleManager.Modules[cfg.class_name];
            if (!cl || !_.isFunction(cl.isAvailable)) return true;
            return cl.isAvailable();
        }),"sort_order"));
        return _.isFunction(done) && done();
    }


    return self;
})

ModuleManager.Modules.HomePage = HomePage;