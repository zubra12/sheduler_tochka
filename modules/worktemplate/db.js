var mongoose = require("mongoose");
var moment = require("moment");
var  _   = require('lodash');

module.exports = {
	models:{
		"user":{
			"fields":{
				"ShuffleWorkTemplates":{
       				"type": Boolean,
        			'template': 'form_switch',
            		"default": false
				},
		        "WorkTemplates": [{
       				"type": mongoose.Schema.Types.ObjectId,
        			'ref': 'worktemplate',
            		"default": null               
			    }],
 				"WorkTemplatesView": {
            		"type": Array,
            		"default": []
		        }
			}
		},
		"workshifttemplate":{
			 "fields": {
			 	"WorkShiftReadable":{
            		"type": String,
            		"default": ""
			 	},
			 	"WorkShiftId":{
	       			"type": mongoose.Schema.Types.ObjectId,
	        		'ref': 'workshift',
	            	"default": null               
		        },
		        "MinCount":{
		        	"type": Number,
	            	"default": 0
		        }, 
		        "MaxCount":{
		        	"type": Number,
	            	"default": 0
		        }
			 }
		},
		"worktemplate": {
	        "fields": {
	            "WorkShifts": [],
	            "DepartmentId":{
       				"type": mongoose.Schema.Types.ObjectId,
        			'ref': 'department',
            		"default": null               
	            },
	            "Name": {
	            		"type": String,
	            		"default": ""
		        },
	            "Type": {
	            		"type": String,
	            		"default": "exact5days",
						"variants":["exact5days","notexact5days","workshifts"],
						"template":"form_select"
		        },
	            "IsOneCalendarWeekend": {
	            		"type": Boolean,
	            		"default": false,
	            		"template":"form_switch"
		        },
	            "WorkDays": {
	            		"type": Number,
	            		"default": 0
		        },
	            "Weekends": {
	            		"type": Number,
	            		"default": 0
		        }
	        }           
        }         
	},
	schema: {
		user: function(schema){
			schema.post('save',function(self, done){
				var getReadable = function(templates,done){
					if (_.isEmpty(templates)) return done(null,[]);
					mongoose.model("worktemplate").find({_id:{$in:templates}}).lean().exec(function(err,list){
						return done(err,_.map(list,"Name"))
					})
				}
				getReadable(self.WorkTemplates,function(err,readable){
					mongoose.model("user").update({_id:self._id},{$set:{WorkTemplatesView:readable}}).exec(done);
				})
			});
			return schema; 
		}
	}
}

