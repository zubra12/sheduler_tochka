var _ = require('lodash');
var async = require('async');
var router   = require('express').Router();
var mongoose   = require('mongoose');




router.get('/workshifts', function(req, res, next) {
	mongoose.model("workshift").find({DepartmentId:req.query.DepartmentId}).lean().exec(function(err,workShifts){
		if (err) return next (err);
		return res.json(workShifts);
	})
})  
  


module.exports = router;