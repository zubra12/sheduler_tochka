var WorkTemplate = (new function(){

	var self = new Module("worktemplate");

	self.isAvailable = function(){
		return Permissions.check(["admin","manager","teamlead"]);
	}

	self.beforeHide = function () {
        ModelTableEdit.clear();
        Bus.off("modelcreated",self.subscribeType);
        Bus.off("modelloaded",self.subscribeType);
        Bus.off("modelcreated",self.updateOwner);        
        Bus.off("modelloaded",self.updateModelType);        
        Bus.off("modelloaded",self.updateModelType);        
        Bus.off("department-change",self.loadWorkShifts);
        Bus.off("department-change",self.show);
    }

    self.beforeShow = function () {
        self.show();
        Bus.on("modelcreated",self.subscribeType);
        Bus.on("modelloaded",self.subscribeType);
        Bus.on("modelcreated",self.updateOwner);
        Bus.on("modelloaded",self.updateModelType);
        Bus.on("department-change",self.loadWorkShifts);
        Bus.on("department-change",self.show);
    }

    self.updateOwner = function(){
    	var model = ModelTableEdit.loadedModel();
    	model.DepartmentId(Department.mainCircle());
    	ModelTableEdit.loadedModel(model);
    }

    self.updateModelType = function(){
        var model = ModelTableEdit.loadedModel();
        self.updateEditFields(model.Type());
    }

    self.editFields = ko.observableArray();

    self.typeSubcscribe = null;
    self.workDaysSubcscribe = null;

    self.validateWorkDays = function(){
        var model = ModelTableEdit.loadedModel();
        if (model.Type()=='workshifts' && model.WorkDays()>5){
            self.validationMessage("Рабочих дней должно быть не больше 5");
        } else {
            self.validationMessage(null);
        }
    }

    self.validateMin = ko.observable();
    self.validateMax = ko.observable();
    self.invalidIds = ko.observableArray();

    self.validateMinMax = function(){
        self.validateMin(null);
        self.validateMax(null);
        self.invalidIds([]);
        var minDays = 0, maxDays = 0, bad = [];
        self.workShiftsList().forEach(function(w){
            minDays+=Number(w.MinCount());
//            maxDays+=Number();
            if (w.MinCount()>w.MaxCount() || w.MaxCount()>5){
                bad.push(w.WorkShiftId());
            }
        })
        self.invalidIds(bad);
        var model = ModelTableEdit.loadedModel();
        if (model.Type()=='workshifts'){
            if (minDays>model.WorkDays()) self.validateMin(">"+model.WorkDays());
            if (maxDays>model.WorkDays()) self.validateMax(">"+model.WorkDays());
        } else {
            if (minDays>5) self.validateMin(">5");
            if (maxDays>5) self.validateMax(">5");
        }    
        return _.isEmpty(self.validateMin()) && _.isEmpty(self.validateMax()) && _.isEmpty(self.invalidIds());

    }

    self.subscribeType = function(){
        if (self.typeSubcscribe){
            self.typeSubcscribe.dispose();
        }
        if (self.workDaysSubcscribe){
            self.workDaysSubcscribe.dispose();
        }
        self.typeSubcscribe = ModelTableEdit.loadedModel().Type.subscribe(self.updateEditFields)
        self.workDaysSubcscribe = ModelTableEdit.loadedModel().WorkDays.subscribe(self.validateWorkDays)
    }

    self.onWorkTemplateMove = function(){
        console.log("arguments,",arguments)
    }

    self.updateEditFields = function(type){
        var fields = ["Type","Name"];
        switch(type){
            case "exact5days":
                fields = fields.concat([]);
            break;            
            case "notexact5days":
                fields = fields.concat(["IsOneCalendarWeekend"]);
            break;            
            case "workshifts":
                fields = fields.concat(["WorkDays","Weekends"]);
            break;
        }
        self.editFields(fields);
    }


    self.popupName = '#edit_worktemplate_workshifts';

    self.onClosePopup = function(){
        self.workShiftsList([]);
    }

    self.workShiftsList = ko.observableArray();

    self.editWorkShifts = function(model,field){
        var currentFill = model().toJS()[field];
        self.loadWorkShifts(currentFill,function(){
            self.validateMinMax();
            $(self.popupName).modal("open");    
        })        
    }

    self.applyWorkShiftsList = function(){
        if (!self.validateMinMax()) return;
        var model = ModelTableEdit.loadedModel();
        model.WorkShifts(_.filter(self.workShiftsList(),function(toFilter){
            return !(toFilter.MinCount()==0 && toFilter.MaxCount()==0);
        }))
        ModelTableEdit.loadedModel(model);
        $(self.popupName).modal("close");
    }

    self.loadWorkShifts = function(currentFill,done){
        self.rGet("workshifts",{DepartmentId:Department.mainCircle()},function(data){
            self.workShiftsList(_.map(data,function(wshift){
                var data2set = _.find(currentFill,{WorkShiftId:wshift._id}) || {};
                return MModels.create("workshifttemplate",_.merge({
                    WorkShiftReadable:[wshift.TimeStart,wshift.TimeEnd].join(" - "),
                    WorkShiftId:wshift._id,
                    MinCount:0,
                    MaxCount:0
                },data2set));
            }));
            return _.isFunction(done) && done();
        })
    }

    self.validationMessage = ko.observable();

    self.show = function(){
		ModelTableEdit.initModel({
			modelName:"worktemplate",
			tableFields:["Name","Type"],
            customfields:{
                WorkShifts:{
                    view:"form_workshifts_list",   
                    edit:"form_workshifts_list_edit"
                }
            },
			editFields:self.editFields,
			readOnlyFields:[],
			sort:{Type:1},
            validationMessage:self.validationMessage,
			fullListView:false,
			filter:{DepartmentId:Department.mainCircle()},
			limit:100
		});
        self.editFields(["Type","Name"]);
    }

	return self;
})



ModuleManager.Modules.WorkTemplate = WorkTemplate;



ko.bindingHandlers.textWorkshiftReadable = {
    update: function(element, valueAccessor, allBindingsAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        if (!_.isEmpty(value)) {
            var circle = _.find(Circle.circles(),function(c){
               return c._id()==Circle.choosedCircleId();
            }) || MModels.create("circle");

            $(element).html(toMskTime(value,circle.IsMskTime()));
        }
    }
}

