var mongoose = require("mongoose");
var moment = require("moment");
var  _   = require('lodash');

module.exports = {
	models:{
		 "circle": {
	        "fields": {
	            "Name": {
	                "type": String,
            		"default": ''
	            },	            
	            "IsInGeneral": {
	                "type": Boolean,
            		"default": false,
                	"template":"form_switch"
	            }, 
                "IsMskTime": {
                    "type": Boolean,
                    "default": false,
                    "template":"form_switch"
                } ,               
                /*"IsMoskowTime": {
                    "type": Boolean,
                    "default": false,
                    "template":"form_switch"
                },
	            "IsFiveDays": {
	                "type": Boolean,
            		"default": false,
                	"template":"form_switch"
	            },
				"ReservedTimeBefore": {
	                "type": String,
            		"default": '00:00',
                	"template":"form_time"
	            },
	            "ReservedTimeAfter": {
	                "type": String,
            		"default": '00:00',
                	"template":"form_time"
	            },*/                
	            "DepartmentId": {
	                "type": mongoose.Schema.Types.ObjectId,
            		'ref': 'department',
                	"default": null
	            }
        	}
        },
        "user": {
	        "fields": {
	            "Circles": [
		            {
	       				"type": mongoose.Schema.Types.ObjectId,
	        			'ref': 'circle',
	            		"default": null               
			        }
	            ]
	        }           
        } 

	},
	schema: {
        user: function(schema){
            schema.post('save',function(model, done){
                var self = this;
                if (_.isEmpty(self.DepartmentId) && !_.isEmpty(self.Circles)){
                	mongoose.model("circle").findOne({_id:{$in:self.Circles}}).lean().exec(function(err,circle){
                		mongoose.model("user").findByIdAndUpdate(self._id,{$set:{DepartmentId:circle.DepartmentId}}).exec(done);
                	})
                } else {
                	return done();
                }
            })
            return schema;
        },
        circle: function(schema){
            schema.post('remove',function(model, done){
                console.log("clearing circle ",model._id);
                mongoose.model("user").update({},{$pull:{Circles:model._id}},{multi:true}).exec(function(err){
                    mongoose.model("monthdistribute").remove({CircleId:model._id}).exec(function(err){
                        return done(err);
                    })
                })
            }) 
            schema.post('save',function(model, done){
                if (model.Name=='[HIDDEN]') return done();
                mongoose.model("circle").findOne({DepartmentId:model.DepartmentId,Name:"[HIDDEN]"}).exec(function(err,hiddenOne){
                    if (!hiddenOne) return done();
                    mongoose.model("user").update({Circles:hiddenOne._id},{$addToSet:{Circles:model._id}},{multi:true}).exec(function(err){
                        hiddenOne.remove(done);
                    })
                })
            })
            return schema;
        }
	}
}

