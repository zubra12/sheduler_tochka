var Circle = (new function(){

	var self = new Module("circle");

	self.choosedCircleId = ko.observable().extend({persistUrl:"circle"});

	self.subCircle = function(){
		var roles = MSite.Me().Roles();
		if (roles.indexOf("teamlead")!=-1){
			return  _.first(MSite.Me().Circles());
		} else {
			return self.choosedCircleId();
		}
	}

	self.initSubCircle = function(){
		self.choosedCircleId(self.subCircle());
	}

	self.chooseCircle = function(data){
		//pager.navigate("/");
		if (self.choosedCircleId()==data._id()){
			self.choosedCircleId(null);
		} else {
			self.choosedCircleId(data._id());	
		}		
		Bus.emit("circle-change");
	}


	self.editCircleModal = function(data){
		self.currentCircle(data);
		self.showModal();
	}



	self.modalName = "#add_edit_circle_modal";

	self.isAvailable = function(){
		return Permissions.check(["admin","manager"]);
	}

	self.circles  = ko.observableArray();
	self.currentCircle = ko.observable();

	self.activeCircles = function(){
		return _.filter(self.circles(),function(c){
			return c.Name()!='[HIDDEN]';
		})
	}


	self.addModal = function(){
		self.currentCircle(MModels.create("circle",{}));
		self.showModal();
	}

	self.removeCircleAction = function(){
		self.confirm("Вы собираетесь удалить круг",self.removeCircle.bind(self,self.currentCircle()));
	}

	self.removeCircle = function(data){
		var id = data._id();
		self.rDelete("circle",{_id:id},function(){
			$(self.modalName).modal("close");
			if(self.choosedCircleId()==id){
				self.choosedCircleId(null);
			}
			self.load();
		});
	}
	
	self.updateCircle = function(){
		var model = _.merge(self.currentCircle().toJS(),{DepartmentId:Department.mainCircle()});
		self.rPut("circle",{model:model},function(){
			$(self.modalName).modal("close");
			Bus.emit("circle-edit-finish");
			self.load();
		});
	}

	self.showModal = function(){
		$(self.modalName).modal("open");
	}

	self.updateCirclesList = function(){
		self.load(function(){
			if (!_.isEmpty(self.choosedCircleId())){
				var founded = _.find(self.circles(),function(circle){
					return circle._id()==self.choosedCircleId();
				})
				if (_.isEmpty(founded)){
					self.choosedCircleId(null);	
				}
			}
		});
	}

	self.unsetCircleId = function(){
		if (!_.isEmpty(Group.choosedGroupId())){
			self.choosedCircleId(null);
		}
	}


	self.init = function(done){
		Bus.on("department-change",self.updateCirclesList);
		Bus.on("user-init",self.initSubCircle);
		Bus.on("distribution-updated",self.updateCirclesList);
		self.load();
		return _.isFunction(done) && done();
	}

	self.load = function(done){
		var depId = Department.mainCircle();
		if (_.isEmpty(depId)){
			self.circles([]);
			return _.isFunction(done) && done();
		}
		self.rGet("list",{DepartmentId:depId},function(data){
			self.circles(_.map(data,function(dep){
				return MModels.create("circle",dep);
			}))		
			self.initSubCircle();	
			return _.isFunction(done) && done();
		})		
	}


	return self;
})



ModuleManager.Modules.Circle = Circle;