var _ = require('lodash');
var async = require('async');
var router   = require('express').Router();
var mongoose   = require('mongoose');

var roleCheck = require(__base + "lib/role.js");
var crudHelper = require(__base + "lib/crud.js");


router.get('/list', function(req, res, next) {
	mongoose.model("circle").find({DepartmentId:req.query.DepartmentId}).lean().exec(function(err,circles){
		if (err) return next (err);
		return res.json(circles);
	})
})

router.put('/circle', roleCheck(["admin"]), crudHelper.createOrUpdate("circle"))
router.delete('/circle', roleCheck(["admin"]), crudHelper.delete("circle"));

 
  




module.exports = router;