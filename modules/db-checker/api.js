const router = require('express').Router()
const mongoose = require('mongoose')

function getCircles(circleId) {
    const query = circleId ? { _id: circleId } : {}
    return mongoose.model('circle').find(query, { _id: true }).exec()
}

function getUsersForCircles(circles) {
    const circleIds = circles.map(circle => circle._id)
    return mongoose.model('user').find({
        Circles: {
            $elemMatch: { $in: circleIds }
        }
    }, { WorkTemplates: true })
}

function getWorkTemplates(users) {
    const ids = users.reduce((acc, user) => {
        user.WorkTemplates.forEach(id => acc.push(id))
        return acc
    }, [])
    return mongoose.model('worktemplate').find({ _id: { $in: ids }}, { WorkShifts: true }).exec()
}

function getWorkShifts(workTemplates) {
    const ids = workTemplates.reduce((acc, workTemplate) => {
        workTemplate.WorkShifts.forEach(workShift => acc.push(workShift.WorkShiftId))
        return acc
    }, [])
    return mongoose.model('workshift').find({ _id: { $in: ids }}, { TimeStart: true, TimeEnd: true, LunchStartEnd: true, ReservedTimeBefore: true, ReservedTimeAfter: true }).exec()
}

function getMonthDistributes(circles) {
    const circleIds = circles.map(circle => circle._id)
    return mongoose.model('monthdistribute').find({
        Type: 'circlemanual',
        CircleId: { $in: circleIds }
    }, { AllowedOverWork: true, NormalWorkHours: true, DataObjects: true }).exec();
}

function getDbObj(circleId) {
    const db = {}
    return getCircles(circleId)
        .then(circles => db.circles = circles)
        .then(circles => getMonthDistributes(circles))
        .then(monthDistributes => db.monthDistributes = monthDistributes)
        .then(() => getUsersForCircles(db.circles))
        .then(users => db.users = users)
        .then(users => getWorkTemplates(users))
        .then(workTemplates => db.workTemplates = workTemplates)
        .then(workTemplates => getWorkShifts(workTemplates))
        .then(workShifts => db.workShifts = workShifts)
        .then(() => db)
}

function checkForArrayField(errors, obj, field, collection) {
    const val = obj[field]
    if (!val) {
        errors.push({ collection, id: obj._id.toString(), msg: `${collection.slice(0, -1)} should has '${field}' array` })
        return false
    }
    if (!(val instanceof Array)) {
        errors.push({ collection, id: obj._id.toString(), msg: `${collection.slice(0, -1)} '${field}' field should be an array` })
        return false
    }
    return true
}

function checkForObjectId(errors, obj, path, collection) {
    let val = obj
    for (const key of path) val = val[key]
    if (!val) {
        errors.push({ collection, id: obj._id.toString(), path, msg: `${collection.slice(0, -1)} '${path.join('.')}' element should be a string or ObjectId` })
        return false
    }
    return true
}

function checkForPositiveNumber(errors, obj, path, collection) {
    let val = obj
    for (const key of path) val = val[key]
    val = parseInt(val)
    if (isNaN(val) || val < 0) {
        errors.push({ collection, id: obj._id.toString(), path, msg: `${collection.slice(0, -1)} '${path.join('.')}' element should be a positive number or be expressed so` })
        return false
    }
    return true
}

function checkForTime(errors, obj, path, collection) {
    let val = obj
    for (const key of path) val = val[key]
    const isStr = typeof(val) === 'string' || val instanceof String
    if (!isStr || (/^\d\d\:\d\d$/.exec(val) == null && /^\d\d\:\d\d\-\d\d\:\d\d$/.exec(val) == null)) {
        errors.push({ collection, id: obj._id.toString(), path, msg: `${collection.slice(0, -1)} '${path.join('.')}' element should be a string '00:00' or '00:00-01:00'` })
        return false
    }
    return true
}

function checkValueForWhiteList(errors, obj, path, collection, whiteList) {
    let val = obj
    for (const key of path) val = val[key]
    if (!whiteList.some(wlVal => wlVal === val)) {
        errors.push({ collection, id: obj._id.toString(), path, msg: `${collection.slice(0, -1)} '${path.join('.')}' element should be one of ${JSON.stringify(whiteList)}` })
        return false
    }
    return true
}

function checkForExists(errors, obj, path, collection, existsInList, existsInCollection) {
    let val = obj
    for (const key of path) val = val[key]
    const exists = existsInList.some(el => {
        if (el._id == val) return true
        if (el._id.toString() === val.toString()) return true
        return false
    })
    if (!exists) {
        errors.push({ collection, id: obj._id.toString(), path, msg: `${collection.slice(0, -1)} '${path.join('.')}' element should point to existed object in '${existsInCollection}' collection` })
        return false
    }
    return true
}

function checkUsers(db, errors = []) {
    for (const user of db.users) {
        if (!checkForArrayField(errors, user, 'WorkTemplates', 'users')) continue
        for (let i = 0; i < user.WorkTemplates.length; i++) {
            if (!checkForObjectId(errors, user, ['WorkTemplates', i], 'users')) continue
            if (!checkForExists(errors, user, ['WorkTemplates', i], 'users', db.workTemplates, 'workTemplates')) continue
        }
    }
    return errors
}

function checkWorkTemplates(db, errors = []) {
    for (const workTemplate of db.workTemplates) {
        if (!checkForArrayField(errors, workTemplate, 'WorkShifts', 'workTemplates')) continue
        for (let i = 0; i < workTemplate.WorkShifts.length; i++) {
            if (!checkForObjectId(errors, workTemplate, ['WorkShifts', i, 'WorkShiftId'], 'workTemplates')) continue
            checkForPositiveNumber(errors, workTemplate, ['WorkShifts', i, 'MinCount'], 'workTemplates')
            checkForPositiveNumber(errors, workTemplate, ['WorkShifts', i, 'MaxCount'], 'workTemplates')
            checkForExists(errors, workTemplate, ['WorkShifts', i, 'WorkShiftId'], 'workTemplates', db.workShifts, 'workShifts')
        }
    }
    return errors
}

function checkWorkShifts(db, errors = []) {
    for (const workShift of db.workShifts) {
        checkForTime(errors, workShift, ['TimeStart'], 'workShifts')
        checkForTime(errors, workShift, ['TimeEnd'], 'workShifts')
        checkForTime(errors, workShift, ['LunchStartEnd'], 'workShifts')
        checkForTime(errors, workShift, ['ReservedTimeBefore'], 'workShifts')
        checkForTime(errors, workShift, ['ReservedTimeAfter'], 'workShifts')
    }
    return errors
}

function checkMonthDistributes(db, errors = []) {
    for (const dist of db.monthDistributes) {
        checkForPositiveNumber(errors, dist, ['AllowedOverWork'], 'monthDistributes')
        checkForPositiveNumber(errors, dist, ['NormalWorkHours'], 'monthDistributes')
        checkForArrayField(errors, dist, ['DataObjects'], 'monthDistributes')
        for (let i = 0; i < dist.DataObjects.length; i++) {
            checkForTime(errors, dist, ['DataObjects', i, 'time'], 'monthDistributes')
            checkValueForWhiteList(errors, dist, ['DataObjects', i, 'type'], 'monthDistributes', ['start', 'end'])
        }
    }
    return errors
}

function mainCheck() {
    return getDbObj().then(db => {
        const errors = []
        checkUsers(db, errors)
        checkWorkTemplates(db, errors)
        checkWorkShifts(db, errors)
        checkMonthDistributes(db, errors)
        return errors
    })
}

router.get('/check', function(req, res, next) {
    mainCheck().then(errors => res.json(errors)).catch(e => next(e))
})

router.test = {
    getCircles,
    getUsersForCircles,
    getWorkTemplates,
    getWorkShifts,
    getDbObj,
    checkUsers,
    checkWorkTemplates,
    checkWorkShifts,
}

module.exports = router
