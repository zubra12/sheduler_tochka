const express        = require('express')
    , app            = express()
    , mongoose       = require('mongoose')
    , path           = require('path')
    , { test }       = require('./api')

global.__base = path.dirname(require.resolve('../..')) + path.sep
console.log(__base)

mongoose.Promise = global.Promise;

mongoose.connect('mongodb://10.66.80.5:27017/sheduler_tochka', {
    promiseLibrary: Promise,
    useNewUrlParser: true,
    connectTimeoutMS: 600000,
});

mongoose.connection.on('connected', function(){
    const ModelInit = require(__base + 'lib/initdb.js');
    ModelInit(function(){
        app.set('port',2035);

        test.getDbObj()
            .then(db => {
                console.log(db)
                console.log(test.checkUsers(db), test.checkWorkTemplates(db), test.checkWorkShifts(db))
            })
            .then(() => process.exit(0))
    });
})