var Development = (new function(){
	var self = new Module("development");

	self.loginAsRoles = [];
	self.currentRole = ko.observable();

	self.init = function(done){
		self.loginAsRoles = MModels.config().user.fields.Roles.variants;
		return done();
	}

	self.isAvailable = function(){
		return Permissions.check("admin");
	}


	self.loginAs = function(role){
		self.rPost("loginas",{role:role},function(){
			MSite.init(function(){
				MSocket.init(function(){
					pager.navigate("/");
				});
			})		
		})
	}



	return self;
})



ModuleManager.Modules.Development = Development;