use timesheet_plan;
use timesheet::*;
use best_fit_buffer::BestFitBuffer;
use best_fit_buffer::BestFitBufferItem;
use best_combination::*;
use man::*;
use std::collections::HashMap;
use chrono::naive::NaiveDate;
use bson::oid::ObjectId;

enum SpecialList {
    CustomWorkshiftList,
    PreclosedWorkshiftList,
}

pub struct ManpowerCache<'people> {
    main_list: &'people mut [Man],
    pub custom_workshift_list: Vec<(NaiveDate, TimesheetItem, usize)>,
    pub preclosed_workshift_list: Vec<(NaiveDate, TimesheetItem, usize)>,
    certain_result: Vec<CandidatesItem>,
    probable_result: Vec<CandidatesItem>,
    recount_buffer: Vec<TimesheetItem>,
    occupied_people_buffer: Vec<usize>,
}

impl<'people> ManpowerCache<'people> {
    pub fn clear(&mut self) {
        self.certain_result.clear();
        self.probable_result.clear();
    }

    pub fn create_and_add_result_item(&mut self, timespan: TimeSpan, required_manpower: isize) {
        let certain_candidates_item = CandidatesItem {
            timespan,
            required_manpower,
            current_manpower: 0,
            candidates: Vec::new(),
        };
        let probable_candidates_item = CandidatesItem {
            timespan,
            required_manpower,
            current_manpower: 0,
            candidates: Vec::new(),
        };
        self.certain_result.push(certain_candidates_item);
        self.probable_result.push(probable_candidates_item);
    }

    pub fn recount_results(certain_result: &mut [CandidatesItem], probable_result: &mut [CandidatesItem], workshift: &TimesheetItem) {
        for (i, ref mut certain_candidates_item) in certain_result.iter_mut().enumerate() {
            let probable_candidates_item = &mut probable_result[i];
            if workshift.is_inside(&certain_candidates_item.timespan) {
                certain_candidates_item.current_manpower += 1;
                probable_candidates_item.current_manpower = certain_candidates_item.current_manpower;
            }
        }
    }

    pub fn recount_certain_result(&mut self) {
        for ref workshift in self.recount_buffer.iter() {
            Self::recount_results(&mut self.certain_result, &mut self.probable_result, workshift);
        }
        self.recount_buffer.clear();
    }

    pub fn consume_probable_list(&mut self, day: NaiveDate) {
        self.recount_buffer.clear();
        self.occupied_people_buffer.clear();
        if let Some(best_state) = ::best_combination::get_best_state(&self.probable_result, &self.occupied_people_buffer) {
            for (i, digit) in best_state.iter().enumerate() {
                let certain_candidates_item = &mut self.certain_result[i];
                if !digit.is_null() {
                    let mut current_manpower: isize = 0;
                    self.recount_buffer.retain(|workshift| {
                        if workshift.is_inside(&certain_candidates_item.timespan) {
                            current_manpower += 1;
                            return true;
                        }
                        false
                    });
                    let required_manpower = certain_candidates_item.required_manpower - current_manpower;
                    if required_manpower > 0 {
                        for j in 0..digit.len() {
                            let best_fit_buffer_item = digit.get_candidate(&self.probable_result[i], j).to_owned();
                            println!("\t\t best candidate {:?} cur:{} req:{}", &best_fit_buffer_item, current_manpower, certain_candidates_item.required_manpower);

                            //TODO uncopypaste
                            let man: &mut Man = &mut self.main_list[best_fit_buffer_item.man_index];
                            man.add_time(day, &best_fit_buffer_item.workshift, best_fit_buffer_item.template_index, best_fit_buffer_item.workshift_index);
                            //end TODO

                            current_manpower += 1;
                            self.recount_buffer.push(best_fit_buffer_item.workshift);
                            self.occupied_people_buffer.push(best_fit_buffer_item.man_index);
                            certain_candidates_item.candidates.push(best_fit_buffer_item);
                        }
                    }
                    certain_candidates_item.current_manpower = current_manpower;
                } else {
                    certain_candidates_item.current_manpower = std::isize::MAX;
                }
            }
        }
        for i in 0..self.certain_result.len() {
            let certain_candidates_item = &self.certain_result[i];
            let probable_candidates_item = &mut self.probable_result[i];
            probable_candidates_item.current_manpower = certain_candidates_item.current_manpower;
            probable_candidates_item.candidates.clear();
        }
        // self.recount_certain_result();
        println!("consume_probable_list");
        ::best_combination::print_candidates(&self.certain_result);
    }

    pub fn push_to_results(&self, day: NaiveDate, results: &mut Vec<(NaiveDate, TimesheetItem, ObjectId, usize)>) {
        for candidates_item in self.certain_result.iter() {
            for best_fit_buffer_item in candidates_item.candidates.iter() {
                let man = &self.main_list[best_fit_buffer_item.man_index];
                results.push((day, best_fit_buffer_item.workshift, man.id.to_owned(), best_fit_buffer_item.man_index));
            }
        }
    }

    fn add_time(&mut self, date: NaiveDate, best_fit_buffer_item: &BestFitBufferItem)
    {
        let man: &mut Man = &mut self.main_list[best_fit_buffer_item.man_index];
        man.add_time(date, &best_fit_buffer_item.workshift, best_fit_buffer_item.template_index, best_fit_buffer_item.workshift_index);
    }

    fn search_special_list(
        &mut self,
        list_id: SpecialList,
        date: NaiveDate,
        result_index: usize
    ) -> isize
    {
        let mut counter = 0;
        let candidates_item = &mut self.probable_result[result_index];
        let timespan = candidates_item.timespan;

        let list = match list_id {
            SpecialList::CustomWorkshiftList => &self.custom_workshift_list,
            SpecialList::PreclosedWorkshiftList => &self.preclosed_workshift_list,
        };

        for (day, workshift, man_index) in list.iter() {
            if *day == date && workshift.is_inside(&timespan) {
                let best_fit_buffer_item = BestFitBufferItem::new(timespan, workshift, *man_index, std::usize::MAX, std::usize::MAX);
                candidates_item.candidates.push(best_fit_buffer_item);
                counter += 1;
            }
        }
        counter
    }

    pub fn search_main_list(
        &mut self,
        date: NaiveDate,
        result_index: usize,
        max_worktime: isize
    ) -> isize
    {
        let mut counter = 0;
        let candidates_item = &mut self.probable_result[result_index];
        let timespan = candidates_item.timespan;

        for (man_index, man) in self.main_list.iter_mut().enumerate() {
            println!("---- {} Man {} id: {} ---- start", date.format("%Y-%m-%d"), man_index, man.id);
            if max_worktime > 0 { man.max_worktime = max_worktime; }
            if let Some((template_index, workshift_index)) = man.is_fit(date, timespan) {
                let workshift = man.get_workshift(template_index, workshift_index);
                let best_fit_buffer_item = BestFitBufferItem::new(timespan, &workshift, man_index, template_index, workshift_index);
                candidates_item.candidates.push(best_fit_buffer_item);
                counter += 1;
            }
            println!("---- {} Man {} id: {} ---- stop", date.format("%Y-%m-%d"), man_index, man.id);
        }
        counter
    }

    pub fn with_people(people: &'people mut [Man]) -> ManpowerCache {
        let mut custom_workshift_list: Vec<(NaiveDate, TimesheetItem, usize)> = Vec::new();
        let mut preclosed_workshift_list: Vec<(NaiveDate, TimesheetItem, usize)> = Vec::new();
        for (man_index, man) in people.iter().enumerate() {
            for date_time_span in man.custom_workshifts.iter() {
                custom_workshift_list.push((date_time_span.start.date(), TimesheetItem::from_date_time_span(date_time_span), man_index));
            }
            for date_time_span in man.preclosed_workshifts.iter() {
                preclosed_workshift_list.push((date_time_span.start.date(), TimesheetItem::from_date_time_span(date_time_span), man_index));
            }
        }
        let cap = people.len();
        ManpowerCache {
            main_list: people,
            custom_workshift_list,
            preclosed_workshift_list,
            certain_result: Vec::with_capacity(8),
            probable_result: Vec::with_capacity(8),
            recount_buffer: Vec::with_capacity(cap),
            occupied_people_buffer: Vec::with_capacity(cap),
        }
    }
}

pub fn fit(timesheet: &timesheet_plan::TimesheetRequirements, people: &mut [Man]) -> Vec<(NaiveDate, TimesheetItem, ObjectId, usize)> {
    println!("Solve for {} men, work hours {}+{}", people.len(), timesheet.normal_work_hours, timesheet.allowed_over_work_hours);
    let mut result: Vec<(NaiveDate, TimesheetItem, ObjectId, usize)> = Vec::with_capacity(32*24);
    let mut manpower_cache = ManpowerCache::with_people(people);
    let normal_worktime = timesheet.normal_work_hours * TimeSpan::HOUR;
    let max_worktime = (timesheet.normal_work_hours + timesheet.allowed_over_work_hours) * TimeSpan::HOUR;
    for day in timesheet.list.iter() {

        let mut required_manpower = 0;
        manpower_cache.clear();
        for (timespan, manpower) in day.iter() {
            if manpower > 0 {
                required_manpower += manpower;
                manpower_cache.create_and_add_result_item(timespan, required_manpower);
            }
        }

        {
            for i in 0..manpower_cache.certain_result.len() {
                let mut required_manpower: isize;
                let mut current_manpower: isize;
                {
                    let candidates_item = &manpower_cache.certain_result[i];
                    required_manpower = candidates_item.required_manpower;
                    current_manpower = candidates_item.current_manpower;
                    println!("{} {} req {} cur {}", day.date.format("%Y-%m-%d"), candidates_item.timespan, required_manpower, current_manpower);
                }
                if current_manpower < required_manpower {
                    manpower_cache.search_special_list(SpecialList::CustomWorkshiftList, day.date, i);
                    manpower_cache.search_main_list(day.date, i, normal_worktime);
                }
            }
        }
        manpower_cache.consume_probable_list(day.date);

        {
            for i in 0..manpower_cache.certain_result.len() {
                let mut required_manpower: isize;
                let mut current_manpower: isize;
                {
                    let candidates_item = &manpower_cache.certain_result[i];
                    required_manpower = candidates_item.required_manpower;
                    current_manpower = candidates_item.current_manpower;
                    println!("{} {} req {} cur {}", day.date.format("%Y-%m-%d"), candidates_item.timespan, required_manpower, current_manpower);
                }
                if current_manpower < required_manpower {
                    manpower_cache.search_main_list(day.date, i, max_worktime);
                }
            }
        }
        manpower_cache.consume_probable_list(day.date);

        {
            for i in 0..manpower_cache.certain_result.len() {
                let mut required_manpower: isize;
                let mut current_manpower: isize;
                {
                    let candidates_item = &manpower_cache.certain_result[i];
                    required_manpower = candidates_item.required_manpower;
                    current_manpower = candidates_item.current_manpower;
                    println!("{} {} req {} cur {}", day.date.format("%Y-%m-%d"), candidates_item.timespan, required_manpower, current_manpower);
                }
                if current_manpower < required_manpower {
                    manpower_cache.search_special_list(SpecialList::PreclosedWorkshiftList, day.date, i);
                }
            }
        }
        manpower_cache.consume_probable_list(day.date);

        manpower_cache.push_to_results(day.date, &mut result);
    }
    result
}

pub fn get_required_manpower_by_day(timesheet: &timesheet_plan::TimesheetRequirements) -> Vec<isize> {
    let mut result: Vec<isize> = Vec::with_capacity(timesheet.list.len());
    for day in timesheet.list.iter() {
        let mut required_manpower = 0;
        for (timespan, manpower) in day.iter() {
            if manpower > 0 { required_manpower += manpower; }
        }
        result.push(required_manpower);
    }
    result
}
