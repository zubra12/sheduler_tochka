use std::cmp::max;
use std::cmp::min;
use std::fmt;
use chrono::naive::NaiveTime;
use chrono::Timelike;
use chrono::naive::NaiveDateTime;
use chrono::naive::NaiveDate;
use chrono::Duration;
use bson::ordered::OrderedDocument;
use bson::Bson;

#[derive(Copy, Clone, PartialEq, Eq)]
pub struct TimeSpan {
    pub start: isize, // seconds from 00:00
    pub duration: isize, // seconds
}

impl TimeSpan {
    pub const DAY: isize = 24 * 60 * 60;
    pub const HOUR: isize = 60 * 60;

    pub fn is_inside_of(&self, other: &TimeSpan) -> bool {
        let end: isize = self.start + self.duration;
        let other_end: isize = other.start + other.duration;
        end <= other_end && self.start >= other.start
    }

    pub fn is_crossed(&self, other: &TimeSpan) -> bool {
        let end: isize = self.start + self.duration;
        let other_end: isize = other.start + other.duration;
        self.start < other_end && end > other.start
    }

    pub fn intersection(&self, other: &TimeSpan) -> TimeSpan {
        let end: isize = self.start + self.duration;
        let other_end: isize = other.start + other.duration;
        let start: isize = max(self.start, other.start);
        let duration: isize = min(end, other_end) - start;
        TimeSpan {
            start,
            duration: max(duration, 0),
        }
    }

    pub const MAX: TimeSpan = TimeSpan { start: std::isize::MAX, duration: 0 };
    pub const MIN: TimeSpan = TimeSpan { start: std::isize::MIN, duration: 0 };
    pub const ZERO: TimeSpan = TimeSpan { start: 0, duration: 0 };
    pub const NULL: TimeSpan = TimeSpan { start: std::isize::MAX, duration: std::isize::MIN };

    pub fn with_start_and_end(start: isize, mut end: isize) -> TimeSpan {
        if end < start {
            let n = start - (start % Self::DAY);
            end += n + Self::DAY;
        }
        TimeSpan {
            start,
            duration: end - start,
        }
    }

    pub fn parse_from_str(str: &str) -> TimeSpan {
        let mut times: [isize; 2] = [0, 0];
        let mut counter = 0;
        for time_str in str.split('-') {
            if counter >= 2 { break; }
            if let Ok(time) = NaiveTime::parse_from_str(time_str.trim(), "%H:%M") {
                times[counter] = time.num_seconds_from_midnight() as isize;
                counter += 1;
            }
        }
        if counter != 2 { panic!("Cant parse time sequence ({})", str); }
        Self::with_start_and_end(times[0], times[1])
    }

    pub fn get_duration(field: isize) -> Duration {
        Duration::seconds(field as i64)
    }

    fn format(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let m0 = self.start % (60 * 60);
        let h0 = self.start / (60 * 60);
        let e1 = self.start + self.duration;
        let m1 = e1 % (60 * 60);
        let h1 = e1 / (60 * 60);
        write!(f, "{:02}:{:02}-{:02}:{:02}", h0, m0 / 60, h1, m1 / 60)
    }
}

impl fmt::Display for TimeSpan {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.format(f)
    }
}

impl fmt::Debug for TimeSpan {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.format(f)
    }
}

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub struct TimesheetItem {
    pub work: TimeSpan,
    pub breaktime: TimeSpan,
    pub whole: TimeSpan,
}

impl TimesheetItem {
    fn get_required_string<'a>(bson: &'a OrderedDocument, key: &'static str) -> &'a String {
        if let Some(&Bson::String(ref val)) = bson.get(key) {
            return val;
        } else {
            panic!("No '{}' field.", key);
        }
    }

    fn parse_time(str: &String) -> isize {
        if let Ok(time) = NaiveTime::parse_from_str(str, "%H:%M") {
            return time.num_seconds_from_midnight() as isize;
        }
        panic!("Cant parse time ({}) as (%H:%M)", str);
    }

    fn get_time_span(bson: &OrderedDocument, start_key: &'static str, end_key: &'static str) -> TimeSpan {
        let start = Self::parse_time(Self::get_required_string(bson, start_key));
        let end = Self::parse_time(Self::get_required_string(bson, end_key));
        TimeSpan::with_start_and_end(start, end)
    }

    pub const MAX: TimesheetItem = TimesheetItem {
        work: TimeSpan::MAX,
        breaktime: TimeSpan::ZERO,
        whole: TimeSpan::MAX,
    };

    pub fn with_bson(bson: &OrderedDocument) -> TimesheetItem {
        let work = Self::get_time_span(bson, "TimeStart", "TimeEnd");
        let breaktime = TimeSpan::parse_from_str(Self::get_required_string(bson, "LunchStartEnd"));
        let before = Self::parse_time(Self::get_required_string(bson, "ReservedTimeBefore"));
        let after = Self::parse_time(Self::get_required_string(bson, "ReservedTimeAfter"));
        TimesheetItem {
            work,
            breaktime,
            whole: TimeSpan { start: work.start - before, duration: work.duration + after },
        }
    }

    pub fn is_inside(&self, timespan: &TimeSpan) -> bool {
        if *timespan == TimeSpan::NULL { return true; }
        // TODO temporary breaktime disabled
        // if timespan.is_inside_of(&self.breaktime) { return false; }
        if timespan.is_inside_of(&self.work) {
            true
        } else {
            false
        }
    }

    pub fn after_work(&self) -> TimeSpan {
        let work_end = self.work.start + self.work.duration;
        TimeSpan {
            start: work_end,
            duration: self.whole.duration - work_end + self.whole.start,
        }
    }

    pub fn before_work(&self) -> TimeSpan {
        TimeSpan {
            start: self.whole.start,
            duration: self.work.start - self.whole.start,
        }
    }

    pub fn from_date_time_span(date_time_span: &DateTimeSpan) -> TimesheetItem {
        let duration = date_time_span.end.signed_duration_since(date_time_span.start).num_seconds() as isize;
        let start = date_time_span.start.time().num_seconds_from_midnight() as isize;
        TimesheetItem {
            work: TimeSpan { start, duration },
            breaktime: TimeSpan { start: 0, duration: 0 },
            whole: TimeSpan { start, duration },
        }
    }

    pub fn parse_from_str(str: &str) -> TimesheetItem {
        let mut result = TimesheetItem {
            work: TimeSpan::NULL,
            breaktime: TimeSpan::NULL,
            whole: TimeSpan::NULL,
        };
        let mut counter = 0;
        for timespan_str in str.split(',') {
            let timespan = TimeSpan::parse_from_str(timespan_str);
            match counter {
                0 => result.work = timespan,
                1 => result.breaktime = timespan,
                2 => result.whole = timespan,
                _ => {},
            }
            counter += 1;
        }
        if counter <= 2 {
            result.whole = result.work;
        }
        result
    }
}

#[derive(Copy, Clone, PartialEq, Eq)]
pub struct DateTimeSpan {
    pub start: NaiveDateTime,
    pub end: NaiveDateTime
}

impl DateTimeSpan {
    const DATE_FMT: &'static str = "%d.%m.%Y";
    const TIME_FMT: &'static str = "%H:%M";
    const DATE_TIME_FMT: &'static str = "%Y.%m.%d:%H:%M";

    pub fn from_4_str(start_date: &str, start_time: &str, end_date: &str, end_time: &str) -> DateTimeSpan {
        let day_start: NaiveDate = NaiveDate::parse_from_str(start_date, Self::DATE_FMT).unwrap();
        let time_start: NaiveTime = NaiveTime::parse_from_str(start_time, Self::TIME_FMT).unwrap();
        let day_end: NaiveDate = NaiveDate::parse_from_str(end_date, Self::DATE_FMT).unwrap();
        let time_end: NaiveTime = NaiveTime::parse_from_str(end_time, Self::TIME_FMT).unwrap();
        // println!("DateTimeSpan {} {} - {} {}", day_start.format(Self::DATE_FMT), time_start.format(Self::TIME_FMT), day_end.format(Self::DATE_FMT), time_end.format(Self::TIME_FMT));
        DateTimeSpan {
            start: day_start.and_time(time_start),
            end: day_end.and_time(time_end),
        }
    }

    pub fn from_date_and_time_span(date: NaiveDate, timespan: TimeSpan) -> DateTimeSpan {
        let d = date.and_time(NaiveTime::from_num_seconds_from_midnight(0, 0));
        DateTimeSpan {
            start: d.checked_add_signed(TimeSpan::get_duration(timespan.start)).unwrap(),
            end: d.checked_add_signed(TimeSpan::get_duration(timespan.start + timespan.duration)).unwrap(),
        }
    }

    pub fn from_2_date_times(start: NaiveDateTime, end: NaiveDateTime) -> DateTimeSpan {
        DateTimeSpan { start, end }
    }

    pub fn is_inside(&self, other: &DateTimeSpan) -> bool {
        self.start <= other.start && self.end >= other.end
    }

    pub fn is_crossed(&self, other: &DateTimeSpan) -> bool {
        self.start < other.end && self.end > other.start
    }

    pub fn get_time_span(&self, day: NaiveDate) -> TimeSpan {
        if self.start.date() > day || self.end.date() < day {
            return TimeSpan::MIN;
        }
        let mut timespan = TimeSpan::with_start_and_end(0, TimeSpan::DAY);
        if self.start.date() == day {
            timespan.start = self.start.time().num_seconds_from_midnight() as isize;
        }
        if self.end.date() == day {
            timespan.duration = self.end.time().num_seconds_from_midnight() as isize - timespan.start;
        }
        timespan
    }
}

impl fmt::Display for DateTimeSpan {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}-{}", self.start.format(Self::DATE_TIME_FMT), self.end.format(Self::DATE_TIME_FMT))
    }
}

#[cfg(test)]
mod tests {
    use timesheet::TimeSpan;
    use timesheet::DateTimeSpan;
    use chrono::naive::NaiveDate;

    #[test]
    fn time_span_parse_from_str() {
        let ts1 = TimeSpan::parse_from_str("20:00-09:00");
        let ts2 = TimeSpan::parse_from_str("20:00 - 09:00");
        assert!(ts1 == ts2);
    }

    #[test]
    fn time_span_is_inside_of_1() {
        let work = TimeSpan::parse_from_str("08:00-17:00");
        let t = TimeSpan::parse_from_str("11:00-17:00");
        assert!(t.is_inside_of(&work));
    }

    #[test]
    fn time_span_is_inside_of_2() {
        let work = TimeSpan::parse_from_str("11:00-17:00");
        let t = TimeSpan::parse_from_str("13:00-14:00");
        assert!(t.is_inside_of(&work));
    }

    #[test]
    fn time_span_is_crossed_1() {
        let t = TimeSpan { start: -TimeSpan::DAY, duration: 2 * TimeSpan::DAY};
        assert!(!t.is_crossed(&TimeSpan::MAX));
        assert!(!t.is_crossed(&TimeSpan::MIN));
        assert!(t.is_crossed(&TimeSpan::ZERO));
        assert!(!t.is_crossed(&TimeSpan::NULL));
    }

    #[test]
    fn time_span_from_date_and_time_span_1() {
        let d1 = DateTimeSpan::from_4_str("01.09.2018", "20:00", "02.09.2018", "08:00");
        let date = NaiveDate::parse_from_str("01.09.2018", DateTimeSpan::DATE_FMT).unwrap();
        let timespan = TimeSpan::parse_from_str("20:00-08:00");
        let d2 = DateTimeSpan::from_date_and_time_span(date, timespan);
        assert!(d1 == d2);
    }

    #[test]
    fn date_time_span_is_crossed_1() {
        let d1 = DateTimeSpan::from_4_str("01.09.2018", "20:00", "02.09.2018", "19:00");
        let d2 = DateTimeSpan::from_4_str("02.09.2018", "11:00", "02.09.2018", "20:00");
        assert!(d1.is_crossed(&d2));
        assert!(d2.is_crossed(&d1));
    }

    #[test]
    fn date_time_span_is_crossed_2() {
        let d1 = DateTimeSpan::from_4_str("01.09.2018", "10:00", "01.09.2018", "19:00");
        let d2 = DateTimeSpan::from_4_str("01.09.2018", "11:00", "01.09.2018", "17:00");
        assert!(d1.is_crossed(&d2));
        assert!(d2.is_crossed(&d1));
    }

    #[test]
    fn date_time_span_get_time_span_1() {
        let day = NaiveDate::parse_from_str("01.09.2018", DateTimeSpan::DATE_FMT).unwrap();
        let d1 = DateTimeSpan::from_4_str("01.09.2018", "10:00", "01.09.2018", "19:00");
        assert!(d1.get_time_span(day) == TimeSpan::parse_from_str("10:00-19:00"));
    }
}