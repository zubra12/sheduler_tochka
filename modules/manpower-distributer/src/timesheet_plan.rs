use timesheet::TimeSpan;
use std::slice::Iter;
use bson::ordered::OrderedDocument;
use bson::Bson;
use chrono::naive::NaiveTime;
use chrono::naive::NaiveDate;
use chrono::Weekday;
use chrono::Datelike;
use chrono::Timelike;

#[derive(Clone, Copy, PartialEq, Eq)]
pub struct TimeRequirement {
    start: i32, // seconds from 00:00
    man_power: i32,
}

pub struct Day {
    _recs: Vec<TimeRequirement>,
    pub date: NaiveDate,
}

pub struct DayIterator<'plan> {
    _ref: &'plan Day,
    prev: TimeRequirement,
    current: Iter<'plan, TimeRequirement>,
}

impl Day {
    pub fn iter(&self) -> DayIterator {
        let mut iter = DayIterator {
            _ref: self,
            prev: TimeRequirement { start: 0, man_power: 0 },
            current: self._recs.iter(),
        };
        if let Some(&first) = iter.current.next() {
            iter.prev = first;
        }
        iter
    }

    pub fn max_manpower(&self) -> i32 {
        let mut max_val: i32 = 0;
        for x in self._recs.iter() {
            if x.man_power > max_val { max_val = x.man_power; }
        }
        return max_val;
    }

    fn key(date: NaiveDate) -> String {
        let week: &'static str;
        match date.weekday() {
            Weekday::Mon => week = "пн",
            Weekday::Tue => week = "вт",
            Weekday::Wed => week = "ср",
            Weekday::Thu => week = "чт",
            Weekday::Fri => week = "пт",
            Weekday::Sat => week = "сб",
            Weekday::Sun => week = "вс",
        }
        format!("{}{}", date.day(), week)
    }

    pub fn with_bson(req_bson: &OrderedDocument, day: u32) -> Option<Day> {
        let month: u32 = ::parse_int_bson(req_bson.get("Month")) as u32;
        let year: i32 = ::parse_int_bson(req_bson.get("Year")) as i32;
        if let Some(date) = NaiveDate::from_ymd_opt(year, month, day) {
            let key = Self::key(date);
            let data_objects = req_bson.get("DataObjects").unwrap().as_array().unwrap();
            let mut reqs: Vec<TimeRequirement> = Vec::new();
            let mut prev_start: i32 = 0;
            let mut start_end_edge: bool = false;
            for data_obj_enum in data_objects.iter() {
                if let Bson::Document(ref data_obj) = data_obj_enum {
                    let otype = data_obj.get("type").unwrap().as_str().unwrap();
                    let time = NaiveTime::parse_from_str(data_obj.get("time").unwrap().as_str().unwrap(), "%H:%M").unwrap();
                    let mut man_power = ::parse_int_bson(data_obj.get(&key)) as i32;
                    let mut start = time.num_seconds_from_midnight() as i32;
                    match otype {
                        "start" => (),
                        "end" => {
                            man_power = -man_power;
                            if !start_end_edge {
                                start_end_edge = true;
                                prev_start = 0;
                            }
                        },
                        _ => panic!("Unknown type ({})", otype)
                    }
                    if start < prev_start { start += TimeSpan::DAY as i32; }
                    prev_start = start;
                    println!("date: {}, time: {}, man power: {}", date.format("%d.%m.%Y"), time.format("%H:%M"), man_power);
                    reqs.push(TimeRequirement {
                        start,
                        man_power,
                    });
                }
            }
            reqs.sort_by(|a, b| a.start.cmp(&b.start));
            Some(Day {
                _recs: reqs,
                date,
            })
        } else {
            None
        }
    }
}

impl<'plan> Iterator for DayIterator<'plan> {
    type Item = (TimeSpan, isize);
    fn next(&mut self) -> Option<Self::Item> {
        while let Some(&req) = self.current.next() {
            if req.start == self.prev.start {
                self.prev.man_power += req.man_power;
            } else {
                let time_span = TimeSpan {
                    start: self.prev.start as isize,
                    duration: (req.start - self.prev.start) as isize,
                };
                let man_power = self.prev.man_power;
                self.prev = req;
                return Some((time_span, man_power as isize));
            }
        }
        return None;
    }
}

pub struct TimesheetRequirements {
    pub list: Vec<Day>,
    pub normal_work_hours: isize,
    pub allowed_over_work_hours: isize,
    pub max_manpower_step: isize,
}
