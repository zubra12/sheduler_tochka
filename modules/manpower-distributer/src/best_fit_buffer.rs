use timesheet::*;
use std::cmp::Ord;
use std::cmp::PartialOrd;
use std::cmp::Eq;
use std::cmp::PartialEq;
use std::cmp::Ordering;
use std::collections::BinaryHeap;
use std::mem;

// TODO
// than greater than better
pub fn calc_weight(timespan: TimeSpan, x: &TimesheetItem) -> i64 {
    let v = x.work.start.saturating_sub(timespan.start) as i64;
    let duration_weight: i64 = (x.work.duration.saturating_sub(x.breaktime.duration) as i64).saturating_sub(100000);
    if v == 0 {
        return 100000000000_i64.saturating_add(duration_weight);
    } else if v > 0 {
        return 100000000000_i64.saturating_sub(100000_i64.saturating_mul(v)).saturating_add(duration_weight);
    } else {
        return 100000000000_i64.saturating_add(400000_i64.saturating_mul(v)).saturating_add(duration_weight);
    }
}

pub fn calc_ideal_weight() -> i64 {
    100000000000 - 100000 + 8 * (TimeSpan::HOUR as i64)
}

#[derive(Debug, Clone)]
pub struct BestFitBufferItem {
    pub workshift: TimesheetItem,
    pub man_index: usize,
    pub weight: i64,
    pub template_index: usize,
    pub workshift_index: usize,
}

impl BestFitBufferItem {
    pub fn new(timespan: TimeSpan, workshift: &TimesheetItem, man_index: usize, template_index: usize, workshift_index: usize) -> BestFitBufferItem {
        BestFitBufferItem {
            workshift: workshift.to_owned(),
            man_index,
            weight: calc_weight(timespan, workshift),
            template_index,
            workshift_index,
        }
    }
}

impl Ord for BestFitBufferItem {
    fn cmp(&self, other: &Self) -> Ordering {
        self.weight.cmp(&other.weight)
    }
}

impl PartialOrd for BestFitBufferItem {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Eq for BestFitBufferItem {}

impl PartialEq for BestFitBufferItem {
    fn eq(&self, other: &Self) -> bool {
        self.weight == other.weight
    }
}

pub struct BestFitBuffer {
    buffer: BinaryHeap<BestFitBufferItem>,
    pub timespan: TimeSpan,
    pub max_buffer_len: usize,
}

impl BestFitBuffer {
    pub fn with_capacity(capacity: usize) -> Self {
        Self {
            buffer: BinaryHeap::with_capacity(capacity),
            timespan: TimeSpan::ZERO,
            max_buffer_len: 0,
        }
    }

    pub fn clear(&mut self) {
        self.buffer.clear();
    }

    pub fn pop(&mut self) -> Option<BestFitBufferItem> {
        self.buffer.pop()
    }

    pub fn peek(&self) -> Option<&BestFitBufferItem> {
        self.buffer.peek()
    }

    pub fn len(&self) -> usize {
        self.buffer.len()
    }

    //TODO get rid of new memory allocation
    pub fn into_sorted_vec(&mut self) -> Vec<BestFitBufferItem> {
        let capacity = { self.buffer.capacity() };
        mem::replace(&mut self.buffer, BinaryHeap::with_capacity(capacity)).into_sorted_vec()
    }

    pub fn add(&mut self, workshift: &TimesheetItem, man_index: usize, template_index: usize, workshift_index: usize) {
        let item = BestFitBufferItem::new(self.timespan, workshift, man_index, template_index, workshift_index);
        self.buffer.push(item);
    }
}

mod test {
    use timesheet::*;
    use ::best_fit_buffer::calc_weight;

    fn calc_weight0(timespan_str: &str) -> [i64; 5] {
        let timespan = TimeSpan::parse_from_str(timespan_str);

        let workshift0 = TimesheetItem::parse_from_str("08:00-17:00,12:00-13:00");
        let workshift1 = TimesheetItem::parse_from_str("09:00-18:00,12:00-13:00");
        let workshift2 = TimesheetItem::parse_from_str("10:00-19:00,13:00-14:00");
        let workshift3 = TimesheetItem::parse_from_str("11:00-20:00,13:00-14:00");
        let workshift4 = TimesheetItem::parse_from_str("12:00-21:00,15:00-16:00");

        let w0 = calc_weight(timespan, &workshift0);
        let w1 = calc_weight(timespan, &workshift1);
        let w2 = calc_weight(timespan, &workshift2);
        let w3 = calc_weight(timespan, &workshift3);
        let w4 = calc_weight(timespan, &workshift4);

        // println!("{} {} {} {} {}", w0, w1, w2, w3, w4);
        [w0, w1, w2, w3, w4]
    }

    #[test]
    fn weight_0() {
        let mut w: [i64; 5];
        w = calc_weight0("08:00-09:00");
        assert!(w[0] > w[1] && w[1] > w[2] && w[2] > w[3] && w[3] > w[4]);
        w = calc_weight0("09:00-10:00");
        assert!(w[1] > w[2] && w[2] > w[3] && w[3] > w[4] && w[4] > w[0]);
        w = calc_weight0("10:00-11:00");
        assert!(w[2] > w[3] && w[3] > w[4] && w[4] > w[1] && w[1] > w[0]);
        w = calc_weight0("11:00-12:00");
        assert!(w[3] > w[4] && w[4] > w[2] && w[2] > w[1] && w[1] > w[0]);
        w = calc_weight0("12:00-17:00");
        assert!(w[4] > w[3] && w[3] > w[2] && w[2] > w[1] && w[1] > w[0]);
    }
}