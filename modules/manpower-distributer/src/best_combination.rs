use best_fit_buffer::BestFitBufferItem;
use timesheet::TimeSpan;
use std::cmp::Ordering;

#[derive(Debug)]
pub struct CandidatesItem {
    pub timespan: TimeSpan,
    pub required_manpower: isize,
    pub current_manpower: isize,
    pub candidates: Vec<BestFitBufferItem>,
}

pub fn sort_candidates(data: &mut [CandidatesItem]) {
    for candidates_item in data.iter_mut() {
        candidates_item.candidates.sort_by(|a, b| a.man_index.cmp(&b.man_index));
    }
}

#[derive(Clone, Debug)]
pub struct StateItem {
    pub indices: Vec<u32>
}

impl StateItem {
    pub fn is_null(&self) -> bool {
        self.indices.len() == 0
    }

    fn set_to_null(&mut self) {
        self.indices.clear();
    }

    fn new(cap: usize) -> Self {
        let indices: Vec<u32> = Vec::with_capacity(cap);
        Self { indices }
    }

    fn init(&mut self, last: bool) {
        if self.indices.capacity() == 0 { return; }
        while self.indices.len() < self.indices.capacity() {
            self.indices.push(0);
        }
        let l = self.indices.len() - 1;
        for i in 0..self.indices.len() {
            self.indices[i] = (l - i) as u32;
        }
        if last { self.indices[0] -= 1; }
    }

    // fn check(&self) -> bool {
    //     for i in 0..self.indices.len() {
    //         let indice_i = self.indices[i];
    //         for j in (i + 1)..self.indices.len() {
    //             if self.indices[j] == indice_i { return false; }
    //         }
    //     }
    //     true
    // }

    fn check_from(&self, index: usize, indice: u32) -> bool {
        for i in (0..index).rev() {
            let indice_i = self.indices[i];
            if indice == indice_i {
                return false;
            } else if indice < indice_i {
                return true;
            }
        }
        true
    }

    fn next(&mut self, size: usize) -> bool {
        if self.indices.len() == 0 { return false; }
        let mut i = 0;
        loop {
            let mut indice = self.indices[i] + 1;
            while !self.check_from(i, indice) { indice += 1; }
            if (indice as usize) < size {
                self.indices[i] = indice;
                break;
            }
            if (i + 1) >= self.indices.len() { return false; }
            let mut indice = self.indices[i + 1] + 1;
            if (i + 1 + indice as usize) < size {
                for j in (0..(i + 2)).rev() {
                    self.indices[j] = indice;
                    indice += 1;
                }
                break;
            }
            i += 1;
        }
        true
    }

    pub fn len(&self) -> usize {
        self.indices.len()
    }

    pub fn get_candidate<'a>(&self, candidates_item: &'a CandidatesItem, pos: usize) -> &'a BestFitBufferItem {
        &candidates_item.candidates[self.indices[pos] as usize]
    }
}

#[derive(Clone)]
pub struct State {
    counter: usize,
    items: Vec<StateItem>,
}

impl State {
    fn get_ideal_weight(&self) -> i64 {
        let mut weight = 0;
        for item in self.items.iter() {
            weight += ::best_fit_buffer::calc_ideal_weight() * (item.len() as i64);
        }
        weight
    }

    pub fn iter(&self) -> std::slice::Iter<StateItem> {
        self.items.iter()
    }

    fn check_item(items: &[StateItem], data: &[CandidatesItem], index: usize) -> bool {
        let item = &items[index];
        let candidates_item = &data[index];
        for i in 0..item.len() {
            let man_index = item.get_candidate(candidates_item, i).man_index;
            for j in 0..index {
                let item_j = &items[j];
                let candidates_item_j = &data[j];
                for k in 0..item_j.len() {
                    if man_index == item_j.get_candidate(candidates_item_j, k).man_index { return false; }
                }
            }
        }
        return true;
    }

    fn next_item(&mut self, data: &[CandidatesItem], i: usize) -> bool {
        let candidates_item = &data[i];
        loop {
            let succ = {
                self.items[i].next(candidates_item.candidates.len())
            };
            if succ {
                if Self::check_item(&self.items, data, i) {
                    return true;
                }
            } else {
                break;
            }
        }
        false
    }

    fn next(&mut self, data: &[CandidatesItem]) -> bool {
        let mut init_from = self.items.len();
        let mut result = false;
        for i in (0..self.items.len()).rev() {
            if self.next_item(data, i) {
                self.counter += 1;
                result = true;
                break;
            } else {
                init_from = i;
            }
        }
        for i in init_from..self.items.len() {
            {
                self.items[i].init(true);
            }
            if !self.next_item(data, i) {
                self.items[i].set_to_null();
            }
        }
        result
    }

    fn with_candidates_items(data: &[CandidatesItem]) -> Self {
        let mut state: Vec<StateItem> = Vec::with_capacity(data.len());
        let mut prev = 0;
        for candidates_item in data.iter() {
            let required_manpower = candidates_item.required_manpower - prev;
            if required_manpower > 0 {
                let mut item = StateItem::new(required_manpower as usize);
                item.init(true);
                state.push(item);
            }
            prev = candidates_item.required_manpower;
        }
        let mut result = Self {
            counter: 0,
            items: state,
        };
        for i in 0..(data.len() - 1) {
            result.next_item(data, i);
        }
        result
    }
}

impl std::fmt::Debug for State {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let mut result = write!(f, "State: {} {} ", self.counter, self.get_ideal_weight());
        for state_item in self.items.iter() {
            for i in 0..state_item.len() {
                result = write!(f, "{},", state_item.indices[i]);
            }
            result = write!(f, "; ");
        }
        result
    }
}

fn calc_weight(data: &[CandidatesItem], state: &State) -> i64 {
    let mut weight: i64 = 0;
    for (i, digit) in state.iter().enumerate() {
        if !digit.is_null() {
            for j in 0..digit.len() {
                weight += digit.get_candidate(&data[i], j).weight;
            }
        }
    }
    weight
}

pub fn print_candidates(data: &[CandidatesItem]) {
    for (i, candidates_item) in data.iter().enumerate() {
        print!("{}:{} req:{} cur:{}", i, candidates_item.candidates.len(), candidates_item.required_manpower, candidates_item.current_manpower);
        for best_fit_buffer_item in candidates_item.candidates.iter() {
            print!(" {},{}", best_fit_buffer_item.man_index, best_fit_buffer_item.workshift.work);
        }
        print!("\n");
    }
}

pub fn get_best_state(data: &[CandidatesItem], occupied_people: &[usize]) -> Option<State>{
    if data.len() == 0 { return None; }
    print_candidates(data);
    let mut best_weight = std::i64::MIN;
    let mut state = State::with_candidates_items(data);
    let ideal_weight = state.get_ideal_weight();
    let mut counter: usize = 0;
    println!("\t\t {:?}", state);
    let mut best_state = None;
    while state.next(data) {
        let weight = calc_weight(data, &state);
        counter += 1;
        // println!("\t\t{:?}, {}", state, weight);
        if weight > best_weight {
            best_weight = weight;
            best_state = Some(state.clone());
        }
        if weight >= ideal_weight { break; }
    }
    println!("\t\t {:?}, {}, count: {}", best_state, best_weight, counter);
    best_state
}

mod test {
    use ::best_combination::StateItem;

    #[test]
    fn state_item_1() {
        let mut item = StateItem::new(4);
        item.init(false);
        assert!(item.indices == [3, 2, 1, 0], "{:?}", item.indices);
        item.init(true);
        assert!(item.indices == [2, 2, 1, 0], "{:?}", item.indices);
        assert!(item.next(5));
        assert!(item.indices == [3, 2, 1, 0], "{:?}", item.indices);
        assert!(item.next(5));
        assert!(item.indices == [4, 2, 1, 0], "{:?}", item.indices);
        assert!(item.next(5));
        assert!(item.indices == [4, 3, 1, 0], "{:?}", item.indices);
        assert!(item.next(5));
        assert!(item.indices == [4, 3, 2, 0], "{:?}", item.indices);
        assert!(item.next(5));
        assert!(item.indices == [4, 3, 2, 1], "{:?}", item.indices);
        assert!(!item.next(5));
    }

    #[test]
    fn state_item_2() {
        let mut item = StateItem::new(4);
        item.init(false);
        assert!(item.indices == [3, 2, 1, 0], "{:?}", item.indices);
        item.init(true);
        assert!(item.indices == [2, 2, 1, 0], "{:?}", item.indices);
        assert!(item.next(6));
        assert!(item.indices == [3, 2, 1, 0], "{:?}", item.indices);
        assert!(item.next(6));
        assert!(item.indices == [4, 2, 1, 0], "{:?}", item.indices);
        assert!(item.next(6));
        assert!(item.indices == [5, 2, 1, 0], "{:?}", item.indices);
        assert!(item.next(6));
        assert!(item.indices == [4, 3, 1, 0], "{:?}", item.indices);
        assert!(item.next(6));
        assert!(item.indices == [5, 3, 1, 0], "{:?}", item.indices);
        assert!(item.next(6));
        assert!(item.indices == [5, 4, 1, 0], "{:?}", item.indices);
        assert!(item.next(6));
        assert!(item.indices == [4, 3, 2, 0], "{:?}", item.indices);
        assert!(item.next(6));
        assert!(item.indices == [5, 3, 2, 0], "{:?}", item.indices);
        assert!(item.next(6));
        assert!(item.indices == [5, 4, 2, 0], "{:?}", item.indices);
        assert!(item.next(6));
        assert!(item.indices == [5, 4, 3, 0], "{:?}", item.indices);
        assert!(item.next(6));
        assert!(item.indices == [4, 3, 2, 1], "{:?}", item.indices);
        assert!(item.next(6));
        assert!(item.indices == [5, 3, 2, 1], "{:?}", item.indices);
        assert!(item.next(6));
        assert!(item.indices == [5, 4, 2, 1], "{:?}", item.indices);
        assert!(item.next(6));
        assert!(item.indices == [5, 4, 3, 1], "{:?}", item.indices);
        assert!(item.next(6));
        assert!(item.indices == [5, 4, 3, 2], "{:?}", item.indices);
        assert!(!item.next(6));
    }

    #[test]
    fn state_item_3() {
        let mut item = StateItem::new(4);
        item.init(false);
        assert!(item.indices == [3, 2, 1, 0], "{:?}", item.indices);
        let mut counter = 1;
        while item.next(7) { counter += 1; }
        // n!/(r! * (n-r)!)    7!/4!/3!
        assert!(counter == 35, "{}", counter);
    }
}
