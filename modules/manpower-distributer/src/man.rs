use timesheet::*;
use workshift_template::*;
use bson::ordered::OrderedDocument;
use bson::ordered::ValueAccessResult;
use bson::oid::ObjectId;
use chrono::naive::NaiveDate;
use chrono::naive::NaiveDateTime;
use chrono::Datelike;

pub struct Man {
    pub id: ObjectId,
    workshift_templates: Vec<Box<WorkshiftTemplate>>,
    workshift_template_mut_stats: Vec<WorksiftTemplateContext>,
    pub worktime_accumulator: isize,
    should_off_range: Option<DateTimeSpan>,
    should_off_range_list: Vec<DateTimeSpan>,
    pub max_worktime: isize,
    off_time_ranges: Vec<DateTimeSpan>,
    pub custom_workshifts: Vec<DateTimeSpan>,
    pub preclosed_workshifts: Vec<DateTimeSpan>,
    shuffle_work_templates: bool,
}

impl Man {
    pub fn calc_fore_weight(&self, template_index: usize, timespan: TimeSpan) -> i64 {
        let template = &self.workshift_templates[template_index];
        let stats = &self.workshift_template_mut_stats[template_index];
        let mut weight: i64 = 0;
        for i in 0..template.len() {
            let workshift = template.get(i);
            let delta = workshift.workshift.work.start - timespan.start;
            if delta <= 0 { continue; }
            let counter = stats.workshifts_usage[i];
            if workshift.max_count > counter {
                weight += delta as i64;
            }
        }
        return weight;
    }

    fn get_work_week_for_date(date: NaiveDate) -> usize {
        let day = date.day0();
        let day0 = date.with_day0(0).unwrap();
        let offset = day0.weekday().num_days_from_monday();
        ((day + offset) / 7) as usize
    }

    fn get_workshift_template_index_for_date(&self, date: NaiveDate) -> usize {
        Self::get_work_week_for_date(date)
    }

    fn get_template_and_workshift_indexes_for_date(&self, date: NaiveDate, timespan: TimeSpan, forbiden: TimeSpan) -> Option<(usize, usize)> {
        let template_index = self.get_workshift_template_index_for_date(date);
        if template_index < self.workshift_templates.len() {
            let workshift_template = &self.workshift_templates[template_index];
            let workshift_template_mut_stats = &self.workshift_template_mut_stats[template_index];
            let workshift_index = workshift_template.is_fit(date, timespan, forbiden, workshift_template_mut_stats);
            if workshift_index < workshift_template.len() {
                return Some((template_index, workshift_index));
            }
        }
        None
    }

    pub fn add_time(&mut self, day: NaiveDate, workshift: &TimesheetItem, template_index: usize, workshift_index: usize) {
        let date_time_span = DateTimeSpan::from_date_and_time_span(day, workshift.whole);
        self.should_off_range = if date_time_span.start != date_time_span.end {
                self.should_off_range_list.push(date_time_span);
                Some(date_time_span)
            } else { None };
        self.worktime_accumulator += workshift.work.duration - workshift.breaktime.duration;
        let mut set_whole_month_first_use_day = false;
        if template_index < self.workshift_template_mut_stats.len() {
            let stats = &mut self.workshift_template_mut_stats[template_index];
            let usage = stats.workshifts_usage[workshift_index];
            stats.workshifts_usage[workshift_index] = usage + 1;
            if stats.whole_usage == 0 {
                stats.first_use_day = day.day0();
                if stats.whole_month_first_use_day == std::u32::MAX { set_whole_month_first_use_day = true; }
            }
            stats.whole_usage += 1;
            stats.add_day_off(day);
        }
        if set_whole_month_first_use_day {
            for stats in self.workshift_template_mut_stats.iter_mut() { stats.whole_month_first_use_day = day.day0(); }
        }
    }

    pub fn get_workshift(&self, template_index: usize, workshift_index: usize) -> TimesheetItem {
        self.workshift_templates[template_index].get(workshift_index).workshift
    }

    fn get_forbiden_timespan(&self, date: NaiveDate) -> TimeSpan {
        if let Some(should_off_range) = self.should_off_range {
            should_off_range.get_time_span(date)
        } else {
            TimeSpan::MIN
        }
    }

    fn search_fitted(&self, date: NaiveDate, timespan: TimeSpan, forbiden: TimeSpan) -> Option<(usize, usize)> {
        let mut best_workshift_index: usize = std::usize::MAX;
        let mut best_template_index: usize = self.workshift_templates.len();
        let mut weight: i64 = std::i64::MIN;
        for (i, workshift_template) in self.workshift_templates.iter().enumerate() {
            // println!("\tman search template={} {} {}", i, date.format("%d.%m.%Y"), timespan);
            let workshift_index = workshift_template.is_fit(date, timespan, forbiden, &self.workshift_template_mut_stats[i]);
            if workshift_index < workshift_template.len() {
                let workshift = self.get_workshift(i, workshift_index);
                let w = ::best_fit_buffer::calc_weight(timespan, &workshift);
                if w > weight {
                    weight = w;
                    best_template_index = i;
                    best_workshift_index = workshift_index;
                }
            }
        }
        if best_template_index < self.workshift_templates.len() {
            Some((best_template_index, best_workshift_index))
        } else {
            None
        }
    }

    pub fn is_fit(&self, date: NaiveDate, timespan: TimeSpan) -> Option<(usize, usize)> {
        println!("\ttime accum {} {}", self.worktime_accumulator, self.max_worktime);
        if self.worktime_accumulator < self.max_worktime {
            let date_time_span = DateTimeSpan::from_date_and_time_span(date, timespan); //TODO need to exterminate unnecesary conversions
                                                                                        //it is appear search_fitted & workshift_template.is_fit
                                                                                        //should take DateTimeSpan on input instead NaiveDate & TimeSpan
            if let Some(should_off_range) = self.should_off_range {
                println!("\tis should off {} | {}", should_off_range, date_time_span);
                if should_off_range.is_crossed(&date_time_span) {
                    println!("\tshould off {}", should_off_range);
                    return None;
                }
            }

            for off_time_range in self.off_time_ranges.iter() {
                if off_time_range.is_crossed(&date_time_span) {
                    println!("\toff time {}", off_time_range);
                    return None;
                }
            }

            for preclosed_time_range in self.preclosed_workshifts.iter() {
                if preclosed_time_range.is_crossed(&date_time_span) {
                    println!("\tpreclosed workshift {}", preclosed_time_range);
                    return None;
                }
            }

            let forbiden = self.get_forbiden_timespan(date);
            if self.shuffle_work_templates {
                return self.search_fitted(date, timespan, forbiden);
            } else {
                return self.get_template_and_workshift_indexes_for_date(date, timespan, forbiden);
            }
        }
        None
    }

    pub fn add_workshift_until_max_worktime(&mut self, month: NaiveDate, max_worktime: isize, day_num: u32) -> Option<(NaiveDate, TimesheetItem)> {
        if self.worktime_accumulator >= max_worktime {
            return None;
        }
        let day_opt = month.with_day0(day_num);
        if day_opt.is_none() { return None; }
        let mut day: NaiveDate = day_opt.unwrap();

        let mut should_off_range_iter = self.should_off_range_list.iter();
        let mut should_off_range_opt = should_off_range_iter.next();
        loop {
            let mut template_index_r = std::usize::MAX;
            let mut workshift_opt: Option<TimesheetItem> = None;
            if let Some(should_off_range) = should_off_range_opt {
                if day <= should_off_range.end.date() {
                    if let Some(custom_workshift) = self.custom_workshifts.iter().find(|span| {
                        !should_off_range.is_crossed(span) && span.start.date() <= day && span.end.date() >= day
                    }) {
                        let workshift = TimesheetItem::from_date_time_span(custom_workshift);
                        workshift_opt = Some(workshift);
                    } else {
                        let forbiden = should_off_range.get_time_span(day);
                        if let Some((template_index, workshift_index)) = self.get_template_and_workshift_indexes_for_date(day, TimeSpan::NULL, forbiden) {
                            template_index_r = template_index;
                            workshift_opt = Some(self.get_workshift(template_index, workshift_index));
                        }
                    }
                } else {
                    should_off_range_opt = should_off_range_iter.next();
                    continue;
                }
            } else {
                if let Some(custom_workshift) = self.custom_workshifts.iter().find(|span| {
                    span.start.date() <= day && span.end.date() >= day
                }) {
                    let workshift = TimesheetItem::from_date_time_span(custom_workshift);
                    workshift_opt = Some(workshift);
                } else {
                    if let Some((template_index, workshift_index)) = self.get_template_and_workshift_indexes_for_date(day, TimeSpan::NULL, TimeSpan::MIN) {
                        template_index_r = template_index;
                        workshift_opt = Some(self.get_workshift(template_index, workshift_index));
                    }
                }
            }

            // println!("fff {} {}", day.format("%d-%m-%Y"), day.succ().format("%d-%m-%Y"));
            if let Some(workshift) = workshift_opt {
                if template_index_r < self.workshift_template_mut_stats.len() {
                    self.workshift_template_mut_stats[template_index_r].whole_usage += 1;
                }
                self.worktime_accumulator += workshift.work.duration - workshift.breaktime.duration;
                return Some((day, workshift));
            }
            if let Some(next_day) = day.succ_opt() {
                if next_day.month0() == day.month0() {
                    day = next_day;
                    continue;
                }
            }
            return None;
        }
    }

    pub fn is_fit_by_custom_workshifts(&mut self, date: NaiveDate, timespan: TimeSpan) -> Option<TimesheetItem> {
        let date_time_span = DateTimeSpan::from_date_and_time_span(date, timespan); //TODO need to exterminate unnecesary conversions
        for custom_workshift in self.custom_workshifts.iter() {
            if custom_workshift.is_crossed(&date_time_span) {
                let workshift = TimesheetItem::from_date_time_span(custom_workshift);
                println!("\tCustom Workshift {} {}", workshift.work, workshift.breaktime);
                return Some(workshift);
            }
        }
        None
    }

    fn for_each_time_ranges<F>(user: &OrderedDocument, key: &'static str, mut value_callback: F)
        where F: FnMut(ValueAccessResult<&str>, DateTimeSpan)
    {
        if let Ok(array) = user.get_array(key) {
            for date_time_range_enum in array.iter() {
                if let Some(ref date_time_range_obj) = date_time_range_enum.as_document() {
                    let day_start = date_time_range_obj.get_str("DayStart").unwrap();
                    let day_end = date_time_range_obj.get_str("DayEnd").unwrap();
                    let time_start = date_time_range_obj.get_str("TimeStart").unwrap();
                    let time_end = date_time_range_obj.get_str("TimeEnd").unwrap();
                    let type_result = date_time_range_obj.get_str("Info");
                    value_callback(type_result, DateTimeSpan::from_4_str(day_start, time_start, day_end, time_end));
                }
            }
        }
    }

    fn get_exclude_timespans(user: &OrderedDocument) -> Vec<TimeSpan> {
        let mut exclude_timespans: Vec<TimeSpan> = Vec::new();
        if let Some(list_enum) = user.get("WorkShiftsView") {
            let list = list_enum.as_array().unwrap();
            for str_enum in list.iter() {
                exclude_timespans.push(TimeSpan::parse_from_str(str_enum.as_str().unwrap()));
            }
        }
        exclude_timespans
    }

    pub fn with_template_pars(user: &OrderedDocument, templates: &[(OrderedDocument, Vec<WorkshiftRule>)]) -> Man {
        let id: &ObjectId = user.get_object_id("_id").unwrap();
        let exclude_timespans = Self::get_exclude_timespans(user);
        let mut workshift_templates: Vec<Box<WorkshiftTemplate>> = Vec::with_capacity(templates.len());
        for template in templates.iter() {
            let ttype: &str = template.0.get_str("Type").unwrap();
            let is_one_calendar_weekend: bool = if ::parse_int_bson(template.0.get("IsOneCalendarWeekend")) == 0 { false } else { true };
            let days_to_work = ::parse_int_bson(template.0.get("WorkDays"));
            let days_off = ::parse_int_bson(template.0.get("Weekends"));
            let mut workshift_template: Box<WorkshiftTemplate>;
            match ttype {
                "workshifts" => workshift_template = Box::new(WorkshiftTemplateNMWholeMonth::create(&template.1[..], &exclude_timespans[..], days_to_work as u32, days_off as u32)),
                "notexact5days" => workshift_template = Box::new(WorkshiftTemplate5Days::create_weird(&template.1[..], &exclude_timespans[..], is_one_calendar_weekend)),
                "exact5days" => workshift_template = Box::new(WorkshiftTemplate5Days::create_regular(&template.1[..], &exclude_timespans[..])),
                _ => panic!("Unknown template type ({})", template.0),
            }
            workshift_templates.push(workshift_template);
        }
        // println!("\tid: {}, templates: {}", id.to_hex(), workshift_templates.len());
        let mut off_time_ranges: Vec<DateTimeSpan> = Vec::new();
        Self::for_each_time_ranges(user, "Vacations", |_type_r: ValueAccessResult<&str>, value: DateTimeSpan| off_time_ranges.push(value));
        let mut custom_workshifts: Vec<DateTimeSpan> = Vec::new();
        let mut preclosed_workshifts: Vec<DateTimeSpan> = Vec::new();
        Self::for_each_time_ranges(user, "CustomWorkShifts", |type_r: ValueAccessResult<&str>, mut value: DateTimeSpan| {
            value.end = NaiveDateTime::new(value.start.date(), value.end.time());
            match type_r {
                Ok("customworkshiftclosed") => off_time_ranges.push(value),
                Ok("precustomworkshiftclosed") => preclosed_workshifts.push(value),
                _ => custom_workshifts.push(value)
            };
        });
        let mut workshift_template_mut_stats: Vec<WorksiftTemplateContext> = Vec::with_capacity(templates.len());
        for workshift_template in workshift_templates.iter() {
            workshift_template_mut_stats.push(workshift_template.create_mut_context());
        }
        Man {
            id: id.to_owned(),
            workshift_templates,
            workshift_template_mut_stats,
            worktime_accumulator: 0,
            should_off_range: None,
            should_off_range_list: Vec::new(),
            max_worktime: 200*60*60,
            off_time_ranges,
            custom_workshifts,
            preclosed_workshifts,
            shuffle_work_templates: user.get_bool("ShuffleWorkTemplates").unwrap_or(false),
        }
    }
}

mod test {
    use chrono::naive::NaiveDate;
    use chrono::Datelike;
    use ::Man;

    #[test]
    fn get_work_week_for_date() {
        let date1 = NaiveDate::from_ymd(2019, 3, 1);
        let date2 = NaiveDate::from_ymd(2019, 3, 2);
        let date3 = NaiveDate::from_ymd(2019, 3, 3);
        let date4 = NaiveDate::from_ymd(2019, 3, 4);
        let date5 = NaiveDate::from_ymd(2019, 3, 17);
        let date6 = NaiveDate::from_ymd(2019, 3, 18);
        assert!(Man::get_work_week_for_date(date1) == 0);
        assert!(Man::get_work_week_for_date(date2) == 0);
        assert!(Man::get_work_week_for_date(date3) == 0);
        assert!(Man::get_work_week_for_date(date4) == 1);
        assert!(Man::get_work_week_for_date(date5) == 2);
        assert!(Man::get_work_week_for_date(date6) == 3);
    }
}