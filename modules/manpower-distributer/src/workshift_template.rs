extern crate rand;
extern crate num_traits;

use timesheet::*;
use chrono::naive::NaiveDate;
use chrono::Weekday;
use chrono::Datelike;
use self::num_traits::cast::FromPrimitive;

pub struct WorksiftTemplateContext {
    pub workshifts_usage: Vec<u32>,
    pub whole_usage: u32,
    pub first_use_day: u32,
    pub whole_month_first_use_day: u32,
    pub days_off: [Option<Weekday>; 2],
}

impl WorksiftTemplateContext {
    pub fn add_day_off(&mut self, day: NaiveDate) {
        for i in 0..self.days_off.len() {
            if self.days_off[i].is_none() {
                self.days_off[i] = Some(day.weekday());
                break;
            }
        }
    }
}

pub trait WorkshiftTemplate {
    fn is_fit(&self, date: NaiveDate, timespan: TimeSpan, forbiden: TimeSpan, context: &WorksiftTemplateContext) -> usize;
    fn len(&self) -> usize;
    fn get(&self, index: usize) -> &WorkshiftRule;
    fn create_mut_context(&self) -> WorksiftTemplateContext;
}

#[derive(Copy, Clone, PartialEq, Eq)]
pub struct WorkshiftRule {
    pub workshift: TimesheetItem,
    pub min_count: u32,
    pub max_count: u32,
}

pub struct WorkshiftVariants {
    workshifts: Vec<WorkshiftRule>,
}

impl WorkshiftVariants {
    fn create_mut_context(&self) -> WorksiftTemplateContext {
        WorksiftTemplateContext {
            workshifts_usage: vec![0; self.workshifts.len()],
            whole_usage: 0,
            first_use_day: std::u32::MAX,
            whole_month_first_use_day: std::u32::MAX,
            days_off: [None, None],
        }
    }

    fn get(&self, index: usize) -> &WorkshiftRule {
        &self.workshifts[index]
    }

    fn best_fit(&self, timespan: TimeSpan, forbiden: TimeSpan, context_opt: Option<&WorksiftTemplateContext>) -> usize {
        println!("\t- {} {}", timespan, forbiden);
        let mut weight: i64 = std::i64::MIN;
        let mut count_weight: u32 = 0;
        let mut index = self.workshifts.len();
        for (i, val) in self.workshifts.iter().enumerate() {
            let v: &TimesheetItem = &val.workshift;
            println!("\t-workshift {} best fit {} {} {} | {}-{}-{}-{}",
                i, v.work, v.breaktime, v.whole,
                val.min_count, val.max_count, if let Some(context) = context_opt { context.workshifts_usage[i] } else { 0 },
                if let Some(context) = context_opt { context.whole_usage } else { 0 }
            );
            if v.whole.is_crossed(&forbiden) { continue; }

            let mut counter = 0;
            if let Some(context) = context_opt {
                counter = context.workshifts_usage[i];
                if counter >= val.max_count { continue; }
            }

            if v.is_inside(&timespan) {
                let cw = if !context_opt.is_none() && counter < val.min_count { val.min_count - counter } else { 0 };
                let w = ::best_fit_buffer::calc_weight(timespan, v);
                println!("\t{}/{} {}/{}", cw, count_weight, w, weight);
                if cw > count_weight || (cw == count_weight && w > weight) {
                    count_weight = cw;
                    weight = w;
                    index = i;
                }
            }
        }
        if index < self.workshifts.len() {
            let w = self.get(index).workshift;
            println!("\tbest fit workshift {} {} for timespan {}", w.work, w.breaktime, timespan);
        }
        index
    }

    fn from_rule_list(list: &[WorkshiftRule], exclude: &[TimeSpan]) -> WorkshiftVariants {
        let mut workshifts: Vec<WorkshiftRule> = Vec::with_capacity(list.len());
        'outer: for rule in list.iter() {
            for exclude_timespan in exclude.iter() {
                if exclude_timespan.is_crossed(&rule.workshift.whole) { continue 'outer; }
            }
            workshifts.push(*rule);
        }
        WorkshiftVariants {
            workshifts,
        }
    }
}

pub struct WorkshiftTemplate5Days {
    workshift_variants: WorkshiftVariants,
    is_regular: bool,
    is_one_calendar_day_off: bool,
}

impl WorkshiftTemplate5Days {
    pub fn create_regular(rules: &[WorkshiftRule], exclude: &[TimeSpan]) -> WorkshiftTemplate5Days {
        // println!(\t"{}", "WorkshiftTemplate5Days::regular");
        WorkshiftTemplate5Days {
            workshift_variants: WorkshiftVariants::from_rule_list(rules, exclude),
            is_regular: true,
            is_one_calendar_day_off: false,
        }
    }

    fn get_random_weekday() -> Weekday {
        match Weekday::from_u8(rand::random::<u8>() & 0x7) {
            Some(weekday) => weekday,
            None => Weekday::Sun,
        }
    }

    pub fn create_weird(rules: &[WorkshiftRule], exclude: &[TimeSpan], one_calendar_day_off: bool) -> WorkshiftTemplate5Days {
        // println!("\t{}", "WorkshiftTemplate5Days::weird");
        WorkshiftTemplate5Days {
            workshift_variants: WorkshiftVariants::from_rule_list(rules, exclude),
            is_regular: false,
            is_one_calendar_day_off: one_calendar_day_off,
        }
    }
}

impl WorkshiftTemplate for WorkshiftTemplate5Days {
    fn is_fit(&self, date: NaiveDate, timespan: TimeSpan, forbiden: TimeSpan, context: &WorksiftTemplateContext) -> usize {
        let weekday = date.weekday();
        if self.is_regular {
            if weekday == Weekday::Sat || weekday == Weekday::Sun { return std::usize::MAX; }
            self.workshift_variants.best_fit(timespan, forbiden, Some(context))
        } else if self.is_one_calendar_day_off {
            if weekday.num_days_from_monday() < 5 {
                if context.whole_usage >= 4 { return std::usize::MAX; }
            } else {
                if context.whole_usage >= 6 { return std::usize::MAX; }
            }
            self.workshift_variants.best_fit(timespan, forbiden, Some(context))
        } else {
            if context.whole_usage >= 5 { return std::usize::MAX; }
            self.workshift_variants.best_fit(timespan, forbiden, Some(context))
        }
    }
    fn len(&self) -> usize {
        self.workshift_variants.workshifts.len()
    }
    fn get(&self, index: usize) -> &WorkshiftRule {
        self.workshift_variants.get(index)
    }
    fn create_mut_context(&self) -> WorksiftTemplateContext {
        self.workshift_variants.create_mut_context()
    }
}

pub struct WorkshiftTemplateNM {
    workshift_variants: WorkshiftVariants,
    days_to_work: u32,
    days_off: u32,
}

impl WorkshiftTemplateNM {
    pub fn create(rules: &[WorkshiftRule], exclude: &[TimeSpan], days_to_work: u32, days_off: u32) -> WorkshiftTemplateNM {
        println!("{} {}/{}", "WorkshiftTemplateNM::", days_to_work, days_off);
        WorkshiftTemplateNM {
            workshift_variants: WorkshiftVariants::from_rule_list(rules, exclude),
            days_to_work,
            days_off,
        }
    }
}

impl WorkshiftTemplate for WorkshiftTemplateNM {
    fn is_fit(&self, date: NaiveDate, timespan: TimeSpan, forbiden: TimeSpan, context: &WorksiftTemplateContext) -> usize {
        let weekday = date.weekday();
        let d = weekday.num_days_from_monday() % (self.days_to_work + self.days_off);
        // println!("weekday {:?} {}/{}", weekday, d, weekday.num_days_from_monday());
        if d < self.days_to_work {
            return self.workshift_variants.best_fit(timespan, forbiden, None);
        }
        std::usize::MAX
    }
    fn len(&self) -> usize {
        self.workshift_variants.workshifts.len()
    }
    fn get(&self, index: usize) -> &WorkshiftRule {
        self.workshift_variants.get(index)
    }
    fn create_mut_context(&self) -> WorksiftTemplateContext {
        self.workshift_variants.create_mut_context()
    }
}

pub struct WorkshiftTemplateNMWholeMonth {
    workshift_variants: WorkshiftVariants,
    days_to_work: u32,
    days_off: u32,
}

impl WorkshiftTemplateNMWholeMonth {
    pub fn create(rules: &[WorkshiftRule], exclude: &[TimeSpan], days_to_work: u32, days_off: u32) -> WorkshiftTemplateNMWholeMonth {
        println!("{} {}/{}", "WorkshiftTemplateNMWholeMonth::", days_to_work, days_off);
        WorkshiftTemplateNMWholeMonth {
            workshift_variants: WorkshiftVariants::from_rule_list(rules, exclude),
            days_to_work,
            days_off,
        }
    }
}

impl WorkshiftTemplate for WorkshiftTemplateNMWholeMonth {
    fn is_fit(&self, date: NaiveDate, timespan: TimeSpan, forbiden: TimeSpan, context: &WorksiftTemplateContext) -> usize {
        let d = date.day0();
        // println!("first use day {} {} {}", d, context.whole_month_first_use_day, context.first_use_day);
        if context.whole_month_first_use_day == std::u32::MAX {
            return self.workshift_variants.best_fit(timespan, forbiden, None);
        }
        let offset = context.whole_month_first_use_day;
        if d >= offset {
            let r = (d - offset) % (self.days_to_work + self.days_off);
            if r < self.days_to_work {
                return self.workshift_variants.best_fit(timespan, forbiden, None);
            }
        }
        std::usize::MAX
    }
    fn len(&self) -> usize {
        self.workshift_variants.workshifts.len()
    }
    fn get(&self, index: usize) -> &WorkshiftRule {
        self.workshift_variants.get(index)
    }
    fn create_mut_context(&self) -> WorksiftTemplateContext {
        self.workshift_variants.create_mut_context()
    }
}
