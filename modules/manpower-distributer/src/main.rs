#[macro_use]
extern crate clap;
extern crate mongodb;
#[macro_use(bson, doc)]
extern crate bson;
extern crate chrono;

use clap::{Arg, App};
use mongodb::Client;
use mongodb::ThreadedClient;
use mongodb::db::ThreadedDatabase;
use mongodb::coll::options::FindOptions;
use bson::Bson;
use bson::oid::ObjectId;
use bson::ordered::OrderedDocument;
use man::Man;
use chrono::naive::NaiveTime;
use chrono::naive::NaiveDate;
use chrono::Datelike;
use std::collections::HashMap;

mod algorithm;
mod timesheet_plan;
mod man;
mod timesheet;
mod workshift_template;
mod best_fit_buffer;
mod best_combination;

use timesheet::*;

fn get_users_for_circle(db: &mongodb::db::Database, circle_id: &bson::oid::ObjectId) -> mongodb::cursor::Cursor {
    let collection = db.collection("users");
    let bson_doc = doc! {
        "Circles" => doc! {
            "$elemMatch" => doc! { "$eq" => circle_id.to_owned() }
        }
    };
    collection.find(Some(bson_doc), None).unwrap()
}

fn get_requirements_for_find_result(obj: &OrderedDocument) -> timesheet_plan::TimesheetRequirements {
    let mut days: Vec<timesheet_plan::Day> = Vec::new();
    let normal_work_hours = parse_int_bson(obj.get("NormalWorkHours")) as isize;
    let allowed_over_work_hours = parse_int_bson(obj.get("AllowedOverWork")) as isize;
    let mut max_manpower_step: isize = 0;
    // println!("{:?}", obj);
    for day in 1..32 {
        if let Some(timesheet_plan_day) = timesheet_plan::Day::with_bson(obj, day) {
            let max_manpower = timesheet_plan_day.max_manpower() as isize;
            if max_manpower_step < max_manpower { max_manpower_step = max_manpower; }
            days.push(timesheet_plan_day);
        }
    }
    timesheet_plan::TimesheetRequirements {
        list: days,
        normal_work_hours,
        allowed_over_work_hours,
        max_manpower_step,
    }
}

fn get_requirements_for_circle(db: &mongodb::db::Database, month: NaiveDate, circle_id: &bson::oid::ObjectId) -> timesheet_plan::TimesheetRequirements {
    const CIRCLEAUTO: &'static str = "circleauto";
    const CIRCLEMANUAL: &'static str = "circlemanual";
    let collection = db.collection("monthdistributes");
    let bson = doc! {
        "Type" => doc! { "$in": [CIRCLEAUTO, CIRCLEMANUAL] },
        "CircleId" => circle_id.to_owned(),
        "Year" => month.year(),
        "Month" => month.month(),
    };
    let cursor = collection.find(Some(bson), None).unwrap();
    let mut circleauto: Option<OrderedDocument> = None;
    let mut circlemanual: Option<OrderedDocument> = None;
    let mut counter = 2;
    for result in cursor {
        if let Ok(obj) = result {
            if let Ok(type_val) = obj.get_str("Type") {
                println!("{}", type_val);
                if circleauto.is_none() && type_val == CIRCLEAUTO {
                    println!("Found {}", CIRCLEAUTO);
                    counter -= 1;
                    circleauto = Some(obj.to_owned());
                } else if circlemanual.is_none() && type_val == CIRCLEMANUAL {
                    println!("Found {}", CIRCLEMANUAL);
                    counter -= 1;
                    circlemanual = Some(obj.to_owned());
                }
            }
        }
        if counter <= 0 { break; }
    }

    if circlemanual.is_none() {
        get_requirements_for_find_result(&circleauto.unwrap())
    } else {
        get_requirements_for_find_result(&circlemanual.unwrap())
    }
}

fn parse_int_bson(bson_enum_opt: Option<&Bson>) -> i64 {
    match bson_enum_opt {
        Some(ref bson_enum) => match bson_enum {
            Bson::FloatingPoint(v) => return *v as i64,
            Bson::I32(v) => return *v as i64,
            Bson::I64(v) => return *v as i64,
            Bson::String(v) => return v.parse::<i64>().unwrap(),
            Bson::Boolean(v) => return *v as i64,
            _ => panic!("Not a Number"),
        },
        None => panic!("No such key."),
    }
}

fn get_object_id(bson_enum_opt: Option<&Bson>) -> ObjectId {
    let bson_enum = bson_enum_opt.unwrap();
    match bson_enum {
        Bson::ObjectId(v) => v.to_owned(),
        Bson::String(v) => ObjectId::with_string(v).unwrap(),
        _ => panic!("Can't be considered as ObjectId ({:?})", bson_enum)
    }
}

fn get_work_templates_for_user(db: &mongodb::db::Database, user_obj: &OrderedDocument) -> Vec<(OrderedDocument, Vec<workshift_template::WorkshiftRule>)> {
    if let Some(&Bson::Array(ref work_templates_obj)) = user_obj.get("WorkTemplates") {
        let collection0 = db.collection("worktemplates");
        let collection1 = db.collection("workshifts");
        let mut result: Vec<(OrderedDocument, Vec<workshift_template::WorkshiftRule>)> = Vec::new();

        // let bson_req_worktemplates = doc! {
        //     "_id" => doc! {
        //         "$in" => work_templates_obj.iter().map(|id| get_object_id(id) ).collect()
        //     }
        // };
        // let mut req_worktemplates_opt = FindOptions::new();
        // req_worktemplates_opt.projection = Some(doc! {
        //     "WorkShifts" => 1
        // });

        // let cursor_worktemplates = db.collection("worktemplates").find(Some(bson_req_worktemplates), Some(req_worktemplates_opt)).unwrap();
        // let mut workshifts_map: HashMap<ObjectId, (u32, u32)> = HashMap::new();
        // for work_template_enum in cursor_worktemplates.iter() {
        //     let work_template_obj = work_template_enum.unwrap();
        //     let workshifts = work_template_obj.get_array("WorkShifts").unwrap();
        //     for workshift_enum in workshifts.iter() {
        //         let workshift = workshift_enum.as_document().unwrap();
        //         let min_count: u32 = parse_int_bson(workshift.get("MinCount")) as u32;
        //         let max_count: u32 = parse_int_bson(workshift.get("MaxCount")) as u32;
        //         let workshift_id: ObjectId = get_object_id(workshift.get("WorkShiftId"));
        //         workshifts_map.insert(workshift_id, (min_count, max_count));
        //     }
        // }

        // let bson_req_workshifts = doc! {
        //     "_id" => doc! {
        //         "$in" => workshifts_map.iter().map(|id, _v| id.to_owned() ).collect()
        //     }
        // };

        // let cursor_workshifts = db.collection("workshifts").find(Some(bson_req_workshifts), None).unwrap();
        // let mut workshift_rules: Vec<workshift_template::WorkshiftRule> = Vec::with_capacity(cursor_workshifts.len());
        // for workshift_enum in cursor_workshifts.iter() {
        //     let workshift_obj = workshift_enum.unwrap();
        //     let workshift_id: ObjectId = get_object_id(workshift_obj.get("_id"));
        //     let (min_count, max_count) = workshifts_map.get(&workshift_id).unwrap();
        // }


        for work_template_id in work_templates_obj.iter() {
            let bson_req0 = doc! {
                "_id" => work_template_id.to_owned()
            };
            if let Some(work_template_obj) = collection0.find_one(Some(bson_req0), None).unwrap() {
                if let Some(&Bson::Array(ref workshifts)) = work_template_obj.get("WorkShifts") {
                    let mut workshift_rules: Vec<workshift_template::WorkshiftRule> = Vec::with_capacity(workshifts.len());
                    for workshift_enum in workshifts.iter() {
                        if let &Bson::Document(ref workshift) = workshift_enum {
                            let min_count: u32 = parse_int_bson(workshift.get("MinCount")) as u32;
                            let max_count: u32 = parse_int_bson(workshift.get("MaxCount")) as u32;
                            let workshift_id: ObjectId = get_object_id(workshift.get("WorkShiftId"));
                            let bson_req1 = doc! {
                                "_id" => workshift_id,
                            };
                            if let Some(ref workshift_obj) = collection1.find_one(Some(bson_req1), None).unwrap() {
                                workshift_rules.push(workshift_template::WorkshiftRule {
                                    workshift: timesheet::TimesheetItem::with_bson(workshift_obj),
                                    min_count,
                                    max_count,
                                });
                            }
                        }
                    }
                    result.push((work_template_obj.to_owned(), workshift_rules));
                }
            }
        }
        return result;
    }
    panic!("No 'WorkTemplates' field in 'user' bson.");
}

fn main() {
    let matches = App::new("Jetteam sheduletochka manpower distributer")
        .version(crate_version!())
        .author("Jetteam hhemul <hhhhemul@gmail.com>")
        .about("Does manpower distribution")
        .arg(Arg::with_name("month")
            .long("month")
            .help("Month of schedule in 'dd-mm-yyyy' format, example '01-09-2018'")
            .takes_value(true)
            .required(true))
        .arg(Arg::with_name("mongodb")
            .long("mongodb")
            .help("Mongodb config uri")
            .takes_value(true)
            .default_value("mongodb://10.66.80.5:27017/sheduler_tochka"))
        .arg(Arg::with_name("circle_id")
            .long("circle_id")
            .help("Distribute only people in circle with this id")
            .takes_value(true))
        .arg(Arg::with_name("dont_maximize_worktime")
            .long("dont-maximize-worktime")
            .help("Maximize worktime for all people to maximum worktime value")
            .takes_value(false))
        .get_matches();

    if let Some(ref month_str) = matches.value_of("month") {
        let month = NaiveDate::parse_from_str(month_str, "%d-%m-%Y").unwrap();
        let mongodb_uri = matches.value_of("mongodb").unwrap();
        let circle_id = matches.value_of("circle_id");
        let force_maximum_worktime = !matches.is_present("dont_maximize_worktime");
        get_from_mongodb_and_solve(month, mongodb_uri, circle_id, force_maximum_worktime);
    } else {
        println!("{}", matches.usage());
    }
}

fn get_from_mongodb_and_solve(month: NaiveDate, mongodb_uri: &str, circle_id_opt: Option<&str>, force_maximum_worktime: bool) {
    let config = mongodb::connstring::parse(mongodb_uri).unwrap();
    let db_name = config.database.to_owned().unwrap();
    let client = Client::with_config(config, None, None).unwrap();
    let db = client.db(&db_name);
    let collection = db.collection("circles");
    let bson_req = if let Some(circle_id_str) = circle_id_opt {
        Some(doc! {
            "_id" => ObjectId::with_string(circle_id_str).unwrap()
        })
    } else { None };

    let cursor = collection.find(bson_req, None).unwrap();
    for result in cursor {
        if let Ok(item) = result {
            if let Some(&Bson::ObjectId(ref _id)) = item.get("_id") {
                println!("Circle Id: {}", _id.to_string());
                solve(&db, month, &_id, force_maximum_worktime);
            }
        }
    }
}

fn build_workshift_result_bson(date: NaiveDate, workshift: &TimesheetItem, man_id: &ObjectId) -> OrderedDocument {
    let timespan = workshift.work;
    let breaktime = workshift.breaktime;
    let start = NaiveTime::from_num_seconds_from_midnight(timespan.start as u32, 0);
    doc! {
        "date" => format!("{}", date.format("%Y-%m-%d")),
        "start" => format!("{}", start.format("%H:%M")),
        "man" => man_id.to_owned(),
        "duration" => (timespan.duration - breaktime.duration) as i64,
    }
}

fn build_result_bson(circle_id: &ObjectId, month: NaiveDate, result: &[(NaiveDate, TimesheetItem, ObjectId, usize)]) -> OrderedDocument {
    let mut data: Vec<Bson> = Vec::with_capacity(result.len());
    for res_item in result.iter() {
        let inner_doc = build_workshift_result_bson(res_item.0, &res_item.1, &res_item.2);
        data.push(Bson::Document(inner_doc));
    }
    doc! {
        "circle_id" => Bson::ObjectId(circle_id.to_owned()),
        "year" => month.year(),
        "month" => month.month(),
        "data" => data,
    }
}

fn add_workshift_until_max_worktime(result: &mut OrderedDocument, month: NaiveDate, max_worktime: isize, people: &mut [Man]) {
    if let Bson::Array(ref mut data) = result.get_mut("data").unwrap() {
        for man in people.iter_mut() {
            println!("man {} {} {}", man.id, man.worktime_accumulator, max_worktime);
            let mut day_num = 0;
            while let Some((day, workshift)) = man.add_workshift_until_max_worktime(month, max_worktime, day_num) {
                println!("day {}", day.format("%d-%m-%Y"));
                day_num = day.day0() + 1;
                let inner_doc = build_workshift_result_bson(day, &workshift, &man.id);
                data.push(Bson::Document(inner_doc));
            }
        }
    } else {
        panic!();
    }
}

fn solve(db: &mongodb::db::Database, month: NaiveDate, circle_id: &bson::oid::ObjectId, force_maximum_worktime: bool) {
    let mut people: Vec<Man> = Vec::new();
    for u_result in get_users_for_circle(db, circle_id) {
        let user: &OrderedDocument = &u_result.unwrap();
        let work_templates = get_work_templates_for_user(&db, &user);
        people.push(Man::with_template_pars(user, &work_templates[..]));
    }
    let requirements = get_requirements_for_circle(db, month, circle_id);
    let result = algorithm::fit(&requirements, &mut people);
    let mut bson_result = build_result_bson(circle_id, month, &result);
    if force_maximum_worktime {
        println!("--force maximum worktime--");
        add_workshift_until_max_worktime(&mut bson_result, month, requirements.normal_work_hours * 3600, &mut people);
    }
    println!("{:?}", bson_result);
    let collection = db.collection("manpower_distributions");
    collection.insert_one(bson_result, None).unwrap();
}
