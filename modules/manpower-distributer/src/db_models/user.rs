extern crate bson;

use bson::oid::ObjectId;
use bson::Array;

#[derive(Serialize, Deserialize, Debug)]
pub struct User {
    #[serde(rename = "_id")]
    _id: ObjectId;
    #[serde(rename = "Circles")]
    circles: Array<ObjectId>;
}
