extern crate bson;

use bson::oid::ObjectId;

#[derive(Serialize, Deserialize, Debug)]
pub struct Circle {
    #[serde(rename = "_id")]
    pub id: ObjectId;
    #[serde(rename = "Name")]
    pub name: String;
}
