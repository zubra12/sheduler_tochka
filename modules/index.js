var Module = function(id) {

  var self = this;

  self.isAvailable = function() {
    return true;
  }

  self.base = '/api/modules/' + id + '/';

  self.toast = function(text,clickAction){
      var toastHTML = '<span>'+text+'</span>';
      if (!_.isEmpty(clickAction)){
        toastHTML +="<button class='btn-flat toast-action' onclick='"+clickAction.click+"'>"+clickAction.text+"</button>"
      }
      M.toast({html: toastHTML,displayLength:20*1000});
  }
  self.removeToasts = function(){
      M.Toast.dismissAll();
  }

  self.error = ko.observable(null)
  self.isLoading = ko.observable(false);

  self._isLoading = null;
  self._error = null;

  self.saveChanges = function() {
    Logger.log("saving..."+id);
  }

  self.modelIsLoaded = function() {};
  self.modelIsSaved = function() {};
  self.modelIsCreated = function() {};
  self.modelIsDeleted = function() {};

  self.mode = ko.observable();

  self.mode.subscribe(function(v) {
    self.changeModPath();
    self.show();
  });

  self.initSetMode = function(Value) {
    var q = window.location.search.queryObj();
    var init = q.mode || value;
    var choosed = q.choosed;
    if (choosed) {
      setTimeout(function() {
        var query = {};
        if (!_.isEmpty(window.location.search)) {
          query = window.location.search.queryObj()
        }
        query.choosed = choosed;
        var state = window.location.origin + window.location.pathname + toQueryString(query);
        history.replaceState({}, '', state);
      }, 500)
    }
    self.mode(init);
  }

  self.show = function() {
      Logger.log("show module");
  }

  self.confirm = function(message,onAgree,override){
      swal(
        _.merge({
          title: "",
          text: message,
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          cancelButtonText: "Нет, оставить!",
          confirmButtonText: "Да, удалить!",
          cancelButtonClass: "btn waves-effect waves-light btn-margin20",
          confirmButtonClass: "btn waves-effect waves-light red lighten-2 btn-margin20",
          buttonsStyling: false
        },override||{})
     ).then(function(result){
        if (result.value) return _.isFunction(onAgree) && onAgree();
     }); 
  }

  self.notify = function(message,override){
      swal(
        _.merge({
          title: "",
          text: message,
          type: "warning",
          showCancelButton: false,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Ok",
          confirmButtonClass: "btn waves-effect waves-light btn-margin20",
          buttonsStyling: false
        },override||{})
     ).then(function(result){
        return;
     }); 
  }

  self.changeModPath = function() {
    var current = self.mode();
    if (_.isEmpty(current) || moduleManager.choosed() != id) return;
    var query = {};
    if (!_.isEmpty(window.location.search)) {
      query = window.location.search.queryObj()
    }
    query.mode = current;
    if (query.choosed) {
      delete query.choosed;
    }
    var state = window.location.origin + window.location.pathname + toQueryString(query);
    history.replaceState({}, '', state);
  }

  self.subscribe = function(options) {
    console.log(".... subscribe");
    self.unSubscribe(options);
    Bus.on("modelloaded", self.modelIsLoaded);
    Bus.on("modelsaved", self.modelIsSaved);
    Bus.on("modelcreated", self.modelIsCreated);
    Bus.on("modeldeleted", self.modelIsDeleted);
    self._error = ModelTableEdit.error.subscribe(self.error);
    self._isLoading = ModelTableEdit.isLoading.subscribe(self.isLoading);
    if (!_.isEmpty(options)) {
        MSite.subscribe(options);
    }   
  }


  self.unSubscribe = function(options) {
    console.log(".... unsubscribe");
    Bus.off("modelloaded", self.modelIsLoaded);
    Bus.off("modelsaved", self.modelIsSaved);
    Bus.off("modelcreated", self.modelIsCreated);
    Bus.off("modeldeleted", self.modelIsDeleted);
    if (self._error) self._error.dispose();
    if (self._isLoading) self._isLoading.dispose();
    if (!_.isEmpty(options)) {
      MSite.unSubscribe(options);
    }
    ModelTableEdit.clear();
  }

  self.beforeShow = function() {
      Logger.log("module beforeShow");
  }

  self.beforeHide = function() {
      Logger.log("module beforeHide");
  }

  self.init = function(done) {
      return _.isFunction(done) && done();
  }

  self.progressMessage = ko.observable("");
  self.progressCurrentLine = ko.observable(0);
  self.progressLines = ko.observable(0);
  self.progress = ko.observable(0);
  self.progressBarOn = ko.observable(false);

  self.onProgress = function(data) {
    self.progressLines(data.lines);
    self.progressCurrentLine(data.line);
    self.progress(data.progress);
    self.progressMessage(data.message);
  }

  self._request = function(method, urlpart, data, done) {
    self.error(null);
    self.isLoading(true);
    $.ajax({
      url: self.base + urlpart,
      method: method,
      data: data,
      success: function(data) {
        self.isLoading(false);
        if (data.err) {
          Logger.error(data.err);
          return self.error(data.err);
        }
        if (data.progressbar) {
          self.isLoading(true);
          self.progressMessage("");
          self.progressCurrentLine(0);
          self.progress(0);
          self.progressCurrentLine(0);
          self.progressBarOn(true);
          ProgressBar.create(data.progressbar, self.onProgress, function() {
            self.isLoading(false);
            self.progressBarOn(false);
            return _.isFunction(done) && done();
          });
        } else {
          return _.isFunction(done) && done(data);
        }
      },
      error: function(data) {
        self.isLoading(false);
      }
    })
  }

  self.rPost = function(urlpart, data, done) {
    self._request("post", urlpart, data, done);
  }

  self.rGet = function(urlpart, data, done) {
    self._request("get", urlpart, data, done);
  }

  self.rPut = function(urlpart, data, done) {
    self._request("put", urlpart, data, done);
  }

  self.rDelete = function(urlpart, data, done) {
    self._request("delete", urlpart, data, done);
  }

  self.rPing = function(urlpart) {
    self._request("get", urlpart, {}, function() {});
  }

  self.ShowError = function(Message){
      swal("",Message,"error");
  }

  return self;
}










var ModuleManager = (new function() {

  var self = this;

  self.isLoaded = ko.observable(false);

  self.Modules = {};
  self.ModulesConfigs = [];

  self.choosed = ko.observable();

  self.isChoosed = function(name) {
    return (name == self.choosed());
  }

  self.isInstalled = function(moduleName) {
    return !_.isEmpty(ModuleManager.Modules[moduleName]);
  }

  self.opened = ko.observable();
  self.open = function(moduleName) {
    if (self.opened() && self.opened() != moduleName) {
      self.Modules[self.opened()].toggleBox();
    }
    self.opened(ModuleName);
  }

  self.close = function(ModuleName) {
    self.opened(null);
  }

  self.forceСlose = function() {
    var M = self.opened();
    if (M) self.Modules[M].toggleBox();
  }

  self.start = [];
  self.initer = [];
  self.guestIniter = [];

  self.subscriptions = [];

  self._isLoading = null;
  self.isLoading = ko.observable(false);


  self.setModule = function() {
    var route = MBreadCrumbs.сurrentRoute(), old = self.choosed();
    var testId = "", found = null;
      var pageInfo = {};
      try {
        pageInfo = pager.getActivePage().valueAccessor();
      } catch (e) {;}      
      var needed = _.first(_.filter(ModuleManager.ModulesConfigs, function(m) {
        return !_.isEmpty(m.config.pages) && _.find(m.config.pages, {
          id: pageInfo.id
        });
      }))
      if (needed) {
        found = needed.config;
        self.choosed(found.id);
      } else {
        self.choosed("schedule");
        try {
          found = _.find(ModuleManager.ModulesConfigs,{name:"schedule"}).config;
        } catch(e){
          ;
        }
      }

    if (self.choosed() && found && _.isFunction(self.Modules[found.class_name].changeModPath)) {
        self.Modules[found.class_name].changeModPath();
    }
    if (self.choosed() && found && _.isFunction(self.Modules[found.class_name].isLoading)) {
      if (self._isLoading) self._isLoading.dispose();
      self._isLoading = self.Modules[found.class_name].isLoading.subscribe(self.isLoading);
    }
    if (old!= self.choosed()) Bus.emit("current_module_changed",self.choosed());
  }

  self.load = function(done) {
    var Debug = {
      start: [],
      guest: [],
      auth: []
    }
    if (self.isLoaded()) return done(null);
    $.getJSON('/api/modules', function(modules) {
      self.ModulesConfigs = modules;
      var initer = [],
        gIniter = [],
        sIniter = [];
      self.ModulesConfigs.forEach(function(M) {
        if (!self.Modules[M.config.class_name]) {
          Logger.log(M.config.class_name+ " Нет в списке");
        } else if (M.config.initial_load && _.isFunction(self.Modules[M.config.class_name].init)) {
          if (M.config.start_load) {
            Debug.start.push(M.config.id);
            sIniter.push(self.Modules[M.config.class_name].init);
          } else if (M.config.guest_load) {
            Debug.guest.push(M.config.id);
            gIniter.push(self.Modules[M.config.class_name].init);
          } else {
            Debug.auth.push(M.config.id);
            initer.push(self.Modules[M.config.class_name].init);
          }
        }
      })
      self.Start = sIniter;
      self.Initer = initer;
      self.GuestIniter = gIniter;
      self.isLoaded(true);
      return _.isFunction(done) && done();
    })
  }

  self.init = function(done) {
    self.startInit(function() {
      self.guestInit(function() {
        Bus.on("initialnavigate", function() {
          if (window.location.pathname != "/error") return;
          var Q = window.location.search.queryObj();
          if (!Q.Module) return;
          var module = Q.Module;
          var mode = Q.Mode;
          var redirect = Q.Redirect;
          try {
            ModuleManager.Modules[module].Mode(mode);
            pager.navigate(redirect);
          } catch (e) {
            Logger.info(e);
          }
        });
        Bus.on("navigate", function() {
          ModuleManager.forceСlose();
        });
        Bus.on("pagechanged", self.setModule);
        if (!_.isEmpty(MSite.Me())) {
          self.authInit(done);
        } else {
          Bus.emit("modulesinited");
          return done();
        }
      })
    })
  }

  self._init = function(arr,done){
    if (_.isEmpty(arr)) return _.isFunction(done) && done();
    async.each(arr, function(f, cb) {
      f(cb);
    }, function(err) {
      self.isLoaded(true);
      return _.isFunction(done) && done();
    })
  }

  self.authInit = function(done) {
    self._init(self.Initer,function(){
      Bus.emit("modulesinited");
      return _.isFunction(done) && done();
    });
  }

  self.startInit = function(done) {
    self._init(self.Start,done);
  }

  self.guestInit = function(done) {
    self._init(self.GuestIniter,done);
  }

  self.checkAvailable = function(data) {
    var mod = ModuleManager.Modules[data.class_name];
    if (mod && _.isFunction(mod.IsAvailable) && mod.IsAvailable()) {
      return true;
    }
    return false;
  }

  self.pages = function() {
    return _.filter(self.allPages(), function(M) {
      return !M.guest;
    });
  }

  self.guestPages = function() {
    return _.filter(self.allPages(), {
      guest: true
    });
  }

  self.leftMenu = function() {
    return self._filter("leftmenu");
  }

  self.rightMenu = function() {
    return self._filter("rightmenu");
  }

  self.isLeftMenu = function() {
    return self.leftMenu().length > 0;
  }

  self.isRightMenu = function() {
    return self.rightMenu().length > 0;
  }

  self.aAdminPageInner = function() {
    return self._filter("adminplugin");
  }

  self.adminPage = function() {
    return self._filter("adminpage");
  }

  self.homePage = function() {
    return self._filter("homepage");
  }

  self.startPageInner = function() {
    return self._filter("homepage");
  }

  self.topMenu = function() {
    return self._filter("topmenu");
  }

  self._filter = function(type) {
    return _.sortBy(_.map(
      _.filter(
        ModuleManager.ModulesConfigs,
        function(M) {
          return M.config.places && M.config.places[type];
        }), "config"), "sort_index");
  }

  self.allPages = function() {
    var pages = [];
    _.map(ModuleManager.ModulesConfigs, "config").forEach(function(config) {
      if (config.pages) {
        config.pages.forEach(function(page) {
          if (!page.source && !page.sourceOnShow) {
            var merger = {sourceOnShow: "/modules/" + config.id + "/index.html"};
            if (_.isFunction(ModuleManager.Modules[config.class_name].beforeShow)){
                merger.beforeShow =  ModuleManager.Modules[config.class_name].beforeShow;
            }
            if (_.isFunction(ModuleManager.Modules[config.class_name].beforeHide)){
                merger.beforeHide =  ModuleManager.Modules[config.class_name].beforeHide;
            }
            page = _.merge(page, merger);
          }
          pages.push(page);
        })
      }
    })
    return pages;
  }

  return self;
})


