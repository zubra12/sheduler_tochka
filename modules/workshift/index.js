var WorkShift = (new function(){

	var self = new Module("workshift");

	self.isAvailable = function(){
		return Permissions.check(["admin","manager","teamlead"]);
	}

    self.saveNotify = function(){
        self.notify("Информация о смене обновлена");
    }

	self.beforeHide = function () {
        ModelTableEdit.clear();
        Bus.off("modelcreated",self.updateOwner);        
        Bus.off("modelloaded",self.subscribeLunch);
        Bus.off("modelsaved",self.saveNotify);
        Bus.off("department-change",self.show);
    }

    self.beforeShow = function () {
        Bus.on("department-change",self.show);        
        Bus.on("modelcreated",self.updateOwner);
        Bus.on("modelloaded",self.subscribeLunch);
        Bus.on("modelsaved",self.saveNotify);
        self.show();
    }


    self.lunchSubscription = null;
    self.editFields = ["TimeStart","TimeEnd","NoLunch","LunchTimes","Color","ReservedTimeBefore","ReservedTimeAfter"];    //"IsPublic"

    self.subscribeLunch = function(){
        if (self.lunchSubscription){
            self.lunchSubscription.dispose();
        }
        self.lunchSubscription = ModelTableEdit.loadedModel().NoLunch.subscribe(function(val){
            if (val){
                ModelTableEdit.editFields.remove("LunchTimes")
            } else {
                ModelTableEdit.editFields.splice(self.editFields.indexOf("NoLunch")+1,0,"LunchTimes");
            }
        })
    }

    self.updateOwner = function(){
    	var model = ModelTableEdit.loadedModel();
        model.DepartmentId(Department.mainCircle());    
    	ModelTableEdit.loadedModel(model);
        self.subscribeLunch();
    }

    self.beforeCreate = function(){

    }



    self.show = function(){
        var filter = {DepartmentId:Department.mainCircle()};
		ModelTableEdit.initModel({
			modelName:"workshift",
			tableFields:["TimeStart","TimeEnd","LunchTime","WorkingTime"],
			editFields:self.editFields,
			readOnlyFields:[],
			sort:{TimeStart:1},
			fullListView:false,
			filter:filter,
			limit:100
		});
    }

	return self;
})



ModuleManager.Modules.WorkShift = WorkShift;