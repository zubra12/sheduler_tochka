var mongoose = require("mongoose");
var moment = require("moment");
var  _   = require('lodash');

module.exports = {
	models:{
		"user": {
	        "fields": {
	            "WorkShifts": [
		            {
	       				"type": mongoose.Schema.Types.ObjectId,
	        			'ref': 'workshift',
	            		"default": null               
			        }
	            ],
	            "WorkShiftsView": {
	            		"type": Array,
	            		"default": []
		        }
	        }           
        },
		"workshift": {
	        "fields": {
	            "TimeStart": {
	                "type": String,
            		"default": '09:00',
                	"template":"form_time"
	            },	            
	            "IsPublic": {
	                "type": Boolean,
            		"default": false,
                	"template":"form_switch"
	            },	 
	            "DepartmentId": {
	   				"type": mongoose.Schema.Types.ObjectId,
	    			'ref': 'department',
	        		"default": null
		        },	            
	            "TimeEnd": {
	                "type": String,
            		"default": '18:00',
                	"template":"form_time"
	            },
	            "ReservedTimeBefore": {
	                "type": String,
            		"default": '00:00',
                	"template":"form_time"
	            },
	            "ReservedTimeAfter": {
	                "type": String,
            		"default": '00:00',
                	"template":"form_time"
	            },	            
	            "LunchTime": {
	                "type": String,
	                "default": "1"
	            },
	            "WorkingTime": {
	                "type": String,
	                "default": "8"
	            },
	            "LunchStartEnd": {
	                "type": String,
            		"default": '12:00-13:00',
                	"template":"form_time_range"
	            },
	            "LunchTimes": {
	                "type": [String],
            		"default": [],
                	"template":"form_time_ranges"
	            },
	            "NoLunch":{
	            	"type":Boolean,
	            	"default":false,
	            	"template":"form_switch"
	            },
	            "Color": {
	                "type": String,
	                "default": "#000",
                	"template":"form_color"	                
	            }
        	} 
        } 
	},
	schema: {
		user: function(schema){
			schema.post('save',function(self, done){
				var getReadable = function(shifts,done){
					if (_.isEmpty(shifts)) return done(null,[]);
					mongoose.model("workshift").find({_id:{$in:shifts}}).lean().exec(function(err,list){
						return done(err,_.map(list,function(record){
							return [record.TimeStart,record.TimeEnd].join(" - ");
						}))
					})
				}
				getReadable(self.WorkShifts,function(err,readable){
					mongoose.model("user").update({_id:self._id},{$set:{WorkShiftsView:readable}}).exec(done);
				})
			});
			return schema; 
		},
		workshift: function(schema){
			schema.pre('save',function(next, done){
				var self = this;
				if (!self.NoLunch){
					if (!_.isEmpty(self.LunchTimes)){
						self.LunchTime = 0;
						self.LunchTimes.forEach(function(timeLine){
							var lunch = timeLine.split("-");
							var duration = moment.duration((moment(_.last(lunch),"HH:mm")).diff(moment(_.first(lunch),"HH:mm"))).asMinutes();
							self.LunchTime = Number(self.LunchTime) + Number(duration);
						})
						self.LunchTime = Math.ceil((self.LunchTime*10/60))/10;
					} else {
						self.LunchTime = 1;
					}
				} else {
					self.LunchTime = 0;
				}
				var mSt = moment(self.TimeStart,"HH:mm");
				var mEn = moment(self.TimeEnd,"HH:mm");
				if (mEn<mSt){
					mEn.add(1,"day");
				}
				self.WorkingTime = moment.duration(mEn.diff(mSt)).asHours() - self.LunchTime;
				return next();
			});

			return schema; 
		}
	}
}

