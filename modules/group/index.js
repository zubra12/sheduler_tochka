var Group = (new function(){

	var self = new Module("group");


	self.choosedGroupId = ko.observable().extend({persistUrl:"group"});

	self.chooseGroup = function(data){
		//pager.navigate("/");
		if (self.choosedGroupId()==data._id()){
			self.choosedGroupId(null);
		} else {
			self.choosedGroupId(data._id());	
		}		
		Bus.emit("group-change");
	}


	self.editGroupModal = function(data){
		self.currentGroup(data);
		self.showModal();
	}



	self.modalName = "#add_edit_group_modal";

	self.isAvailable = function(){
		return Permissions.check("admin");
	}

	self.groups  = ko.observableArray();
	self.currentGroup = ko.observable();


	self.addModal = function(){
		self.currentGroup(MModels.create("group",{}));
		self.showModal();
	}

	self.removeGroupAction = function(){
		self.confirm("Вы собираетесь удалить группу",self.removeGroup.bind(self,self.currentGroup()));
	}

	self.removeGroup = function(data){
		self.rDelete("group",{_id:data._id()},function(){
			$(self.modalName).modal("close");
			self.load();
		});
	}
	
	self.updateGroup = function(){
		var model = _.merge(self.currentGroup().toJS(),{DepartmentId:Department.mainCircle()});
		self.rPut("group",{model:model},function(){
			$(self.modalName).modal("close");
			Bus.emit("group-edit-finish");
			self.load();
		});
	}


	self.showModal = function(){
		$(self.modalName).modal("open");
	}

	self.unsetGroupId = function(){
		if (!_.isEmpty(Circle.choosedCircleId())){
			self.choosedGroupId(null);
		}
	}

	self.init = function(done){
		Bus.on("department-change",self.updateGroupsList);	
		self.load(done);
	}

	self.unsetChoosedGroup = function(){
		self.choosedGroupId(null);
	}

	self.updateGroupsList = function(){
		self.load(function(){
			if (!_.isEmpty(self.choosedGroupId())){
				var founded = _.find(self.groups(),function(group){
					return group._id()==self.choosedGroupId();
				})
				if (_.isEmpty(founded)){
					self.choosedGroupId(null);	
				}
			}			
		});
	}

	self.load = function(done){
		self.rGet("list",{DepartmentId:Department.mainCircle()},function(data){
			self.groups(_.map(data,function(dep){
				return MModels.create("group",dep);
			}))			
			Bus.emit("group-list-updated");
			return _.isFunction(done) && done();
		})		
	}


	return self;
})



ModuleManager.Modules.Group = Group;