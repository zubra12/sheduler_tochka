var mongoose = require("mongoose");
var moment = require("moment");
var  _   = require('lodash');

module.exports = {
	models:{
		 "group": {
	        "fields": {
	            "Name": {
	                "type": String,
            		"default": ''
	            },
	            "DepartmentId":{
       				"type": mongoose.Schema.Types.ObjectId,
        			'ref': 'department',
            		"default": null               
	            },
	            "Color": {
	                "type": String,
	                "default": "#000",
                	"template":"form_color"	                
	            }
        	}
        },
        "user": {
	        "fields": {
	            "Groups": [
		            {
	       				"type": mongoose.Schema.Types.ObjectId,
	        			'ref': 'group',
	            		"default": null               
			        }
	            ]
	        }           
        } 
	},
	schema: {
	}
}

