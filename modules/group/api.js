var _ = require('lodash');
var async = require('async');
var router   = require('express').Router();
var mongoose   = require('mongoose');

var roleCheck = require(__base + "lib/role.js");
var crudHelper = require(__base + "lib/crud.js");


router.get('/list', function(req, res, next) {
	mongoose.model("group").find({DepartmentId:req.query.DepartmentId}).lean().exec(function(err,groups){
		if (err) return next (err);
		return res.json(groups);
	})
})

router.put('/group', roleCheck(["admin"]), crudHelper.createOrUpdate("group"))
router.delete('/group', roleCheck(["admin"]), crudHelper.delete("group"));

 
  




module.exports = router;