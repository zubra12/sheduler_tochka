var _ = require('lodash');
var async = require('async');
var router = require('express').Router();
var mongoose = require('mongoose');
var roleCheck = require(__base + "lib/role.js");


router.get('/config', function(req, res, next) {
    var models = mongoose.models;
    var result = {};
    for (var modelName in models) {
        var model = mongoose.model(modelName);
        if (_.isFunction(model.cfg)) {
            result[modelName] = model.cfg();
        }
    }
    return res.json(JSON.parse(JSON.stringify(result, function(k, v) {
        return (typeof v == "function") ? v.name : v;
    })));
})

router.get('/models', roleCheck(["admin","manager","teamlead"]), function(req, res, next) {
    var params = JSON.parse(req.query.params);
    var model = mongoose.model(params.modelName);
    if (!model) return next("Модель " + params.modelName + " не найдена");
    var query = (!_.isEmpty(params.filter)) ? params.filter : {};
    if (!_.isEmpty(params.search)){
    	query['search'] = {$regex :  new RegExp(params.search, "i")};
    }
    var runQuery = model.find(query);
    var countQuery = model.count(query);
    if (!_.isEmpty(params.sort)) {
        runQuery.sort(params.sort);
    }
    if (_.isNumber(params.skip)) {
        runQuery.skip(params.skip);
    }
    if (_.isNumber(params.limit)) {
        runQuery.limit(params.limit);
    }
    countQuery.exec(function(err, counter) {
        if (err) return next(err);
        runQuery.lean().exec(function(err, list) {
            if (err) return next(err);
            return res.json({
                counter: counter,
                models: list
            });
        })
    })
})

router.get('/model', roleCheck(["admin","manager","teamlead"]), function(req, res, next) {
    var model = mongoose.model(req.query.modelName);
    if (!model) return next("Модель " + req.query.modelName + " не найдена");
    model.findOne({ _id: req.query._id }).lean().exec(function(err, instance) {
        if (!instance) return next("Объект не найден");
        return res.json(instance);
    })
})

router.put('/model', roleCheck(["admin","manager","teamlead"]), function(req, res, next) {
	var model = mongoose.model(req.body.model);
	var cfg = model.cfg();
	var editFields = cfg.editFields;
	var getOne = function(model, _id, done){
		if (_.isEmpty(_id)) return done(null, new model());
		model.findOne({_id:_id}).exec(function(err,instance){	
			if (_.isEmpty(instance)) return done(null, new model());
			return done(err,instance);
		})
	}
	getOne(model,req.body._id,function(err,instance){
		editFields.forEach(function(field){
			instance[field] = req.body.data[field];
		})
		instance.save(function(err){
			if (err) return next(err);
			return res.json({_id:instance._id});
		})
	})	
})

router.delete('/model', roleCheck(["admin","manager","teamlead"]), function(req, res, next) {
    var model = mongoose.model(req.body.model);
    model.findOne({ _id: req.body._id }).exec(function(err, instance) {
        if (!instance) return next("Объект не найден");
        if (req.body.model=='user' && (req.user._id+"")==req.body._id){
            return next("Не удаляйте себя");
        }
        instance.remove(function(err){
            if (err) return next (err);
        })
        return res.json({});
    })
})



module.exports = router;