var LeftMenu = (new function () {

    var self = this;

    self.isMenuToggled = ko.observable(false, {
        persist: "isLeftMenuToggled"
    });

    self.menu = ko.observable();

    self.menuBlocks = ko.observableArray();

    self.init = function(done){
        var groupOrders = [];
        var byGroups = {};
        var configs = _.filter(_.map(ModuleManager.ModulesConfigs,"config"),function(cfg){
            var cl = ModuleManager.Modules[cfg.class_name];
            if (!cl || !_.isFunction(cl.isAvailable)) return true;
            return cl.isAvailable();
        });
        configs.forEach(function(cfg){
            if (cfg.menu_group_order && _.isEmpty(_.find(groupOrders,{name:cfg.menu_group}))){
                groupOrders.push({name:cfg.menu_group,order:cfg.menu_group_order});
            }
            if (!_.isEmpty(cfg.menu_group)){
                if (!byGroups[cfg.menu_group]) byGroups[cfg.menu_group] = [];
                byGroups[cfg.menu_group].push(cfg);
            }
        })
        groupOrders = _.sortBy(groupOrders,"order");
        var menu = {};
        groupOrders.forEach(function(grp){
            if (!_.isEmpty(byGroups[grp.name]))
            menu[grp.name] = _.map(_.sortBy(byGroups[grp.name],"sort_order"),function(c){
                return {name:c.title,phref:'/'+(c.start_page?'':c.id)};
            });
        })
        self.menu(menu);
        self.menuBlocks(_.sortBy(_.filter(_.map(ModuleManager.ModulesConfigs,"config"),function(cfg){
            if (!cfg.menu_part) return false;
            var cl = ModuleManager.Modules[cfg.class_name];
            if (!cl || !_.isFunction(cl.isAvailable)) return true;
            return cl.isAvailable();
        }),"sort_order"));
        return _.isFunction(done) && done();
    }


    self.closeMenu = function(){
    	self.isMenuToggled(true);
    }

    self.openMenu = function(){
    	self.isMenuToggled(false);
    }

    self.toggleMenu = function () {
        self.isMenuToggled(!self.isMenuToggled());
    }

    return self;
})

ModuleManager.Modules.LeftMenu = LeftMenu;