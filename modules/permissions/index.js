var Permissions = (new function(){
	var self = new Module("permissions");

	self.isAvailable = function(){
		return self.check("admin");
	}


	self.check = function(roles){
		if (!_.isArray(roles)) roles = [roles];
		if (_.isEmpty(MSite.Me())) return false;
		var currentRoles = MSite.Me().Roles();
		var result = false;
		roles.forEach(function(role){
			result = result || currentRoles.indexOf(role)!=-1;
		})
		return result;
	}

	

	return self;
})



ModuleManager.Modules.Permissions = Permissions;



ko.bindingHandlers["permissions"] = {
	init:function(element, valueAccessor, allBindingsAccessor){
		var value = ko.utils.unwrapObservable(valueAccessor());
		var user =  ko.utils.unwrapObservable(MSite.Me);
		var roles = user.Roles();
		var allow = (!_.isEmpty(_.intersection(value,roles)) || _.isEmpty(roles)) ? true:false;
		if (!allow){
			$(element).hide();
		} else {
			$(element).show();
		}		
	}
}

