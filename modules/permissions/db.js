var mongoose = require("mongoose");
var  crypto   = require('crypto');
var  _   = require('lodash');

module.exports = {
	models:{
		 "user": {
	        "fields": {
	            "Roles": {
	                "type": Array,
	                "default": [],
	                "variants":["admin","manager","teamlead","worker"]        
	            }
			}	
		}
	},
	schema: {
	
	}
}
