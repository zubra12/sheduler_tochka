var _ = require('lodash');
var async = require('async');
var router   = require('express').Router();
var mongoose   = require('mongoose');



router.get('/worklog',function(req,res,next){
	mongoose.model("worktime").find({IdUser:req.user._id}).sort({DayStart:1}).lean().exec(function(err,logs){
		return res.json(logs);
	})
});


router.put('/worklog',function(req,res,next){
	var workTimeModel = mongoose.model("worktime");
	var newLogs = _.map(req.body.logs,function(l){
		return (new workTimeModel(_.merge(l,{IdUser:req.user._id})));
	});
	workTimeModel.remove({IdUser:req.user._id}).exec(function(err){
		async.eachSeries(newLogs,function(l,cb){
			l.save(cb);
		},function(err){
			if (err) return next(err);
			return res.json({});
		})
	})
});





module.exports = router;