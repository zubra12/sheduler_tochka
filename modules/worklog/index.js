var WorkLog  = (new function () {

    var self = new Module("worklog");

    self.weekDays = 'пн_вт_ср_чт_пт_сб_вс'.split('_');

    self.init = function (done) {
    	self.load(function(){
    		return _.isFunction(done) && done();	
    	})
    }

    self.openModal = function(){
		$("#edit_worklog_modal").modal("open");
    }

    self.addWorkLog = function(){
		var M = MModels.create("worktime");
		self.WorkLogs.push(M);
		return false;
    }

    self.removeWorkLog = function(data){
		self.WorkLogs.remove(data);
    }

	self.saveWorkLogs = function(){
		self.rPut("worklog",{logs:_.map(self.WorkLogs(),function(l){
			return l.toJS();
		})},function(){
			$("#edit_worklog_modal").modal("close");
		})
	}



    self.WorkLogs = ko.observableArray();

    self.load = function(done){
		self.rGet("worklog",{},function(data){
			self.WorkLogs(_.map(data,function(Log){
				return MModels.create("worktime",Log);
			}))
			return _.isFunction(done) && done();
		})
    }

    return self;
})




ModuleManager.Modules.WorkLog  = WorkLog ;


