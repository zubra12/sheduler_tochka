var mongoose = require("mongoose");
var  _   = require('lodash');

module.exports = {
	models:{
 	    "worktime": {
	        "fields": {
				"IdUser": {
	                type: mongoose.Schema.Types.ObjectId,
	                ref: 'user',
	                default: null
	            },         
            	"DayStart": {
                	"type": Number,
                	"default": 0,
                	"template":"form_day"
            	},
            	"DayEnd": {
                	"type": Number,
                	"default": 4,
                	"template":"form_day"
            	},            
            	"TimeStart": {
                	"type": String,
                	"default": '09:00',
                	"template":"form_time"
            	},            
            	"TimeEnd": {
                	"type": String,
                	"default": '09:00',
                	"template":"form_time"
            	}
        	}
    	}
	},
	schema: {
	}
}

