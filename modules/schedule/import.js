var _ = require('lodash');
var async = require('async');
var moment = require('moment');
var mongoose = require('mongoose');

moment.locale("ru-RU");

var ExcelParser = (new function() {
    var self = this;

    var XLSX = require('xlsx');

    self.workName = "Общий график";
    self.circleName = "Распределение смен";
    self.total = "Количество сотрудников";
    self.fioField = 'ФИО/ Дата';

    self.year = moment().format("YYYY");
    self.month = moment().format("MM");
    self.format = "DD.MM.YYYY";

    self.loadFile = function(fileName, done) {
        var workbook = XLSX.readFile(fileName, { cellStyles: true, cellNF: true });
        var workSheetNames = _.keys(workbook.Sheets);
        if (workSheetNames.indexOf(self.workName) == -1) {
            return done('не найдена вкладка с названием "' + self.workName + '"');
        }
        if (workSheetNames.indexOf(self.circleName) == -1) {
            return done('не найдена вкладка с названием "' + self.workName + '"');
        }
        return done(null, {
            users: XLSX.utils.sheet_to_json(workbook.Sheets[self.workName], { raw: false }),
            circles: XLSX.utils.sheet_to_json(workbook.Sheets[self.circleName], { raw: false }),
            rawData: workbook.Sheets[self.workName]
        });
    }

    self.nameRegexp = /[А-ЯЁ][а-яё]+\s[А-ЯЁ][а-яё]+/;

    self.users = function(json) {
        var usersList = [],
            chunk = {};
        json.forEach(function(row, index) {
            var testName = row[self.fioField];
            if (!_.isEmpty(testName)) {
                if (testName.match(self.nameRegexp)) {
                    chunk[index] = testName;
                } else {

                    if (!_.isEmpty(chunk)) {
                        if (self.total == testName) {
                            usersList.push(chunk);
                        } else {;
                        }
                    }
                    chunk = {};
                }
            }
        })
        if (!_.isEmpty(chunk)) {
            usersList.push(chunk);
        }
        return usersList;
    }

    self.circles = function(json) {
        var circles = [];
        json.forEach(function(row) {
            if (!_.isEmpty(row[self.fioField])) {
                var testName = row[self.fioField].replace(/\s+/g, " ").trim();
                var isCircle = /[0-9]* ночь \/ [0-9]* спец/;
                if (testName.match(isCircle)) {
                    circles.push(testName.replace(isCircle, '').trim());
                }
            }
        })
        return circles;
    }

    self.worshifts = function(json, rawData) {
        var workshifts = {},
            findEnd = [];
        json.forEach(function(row, index) {
            var testName = row[self.fioField];
            if (!_.isEmpty(testName) && testName.match(/[0-9]*:[0-9]*/)) {
                var realIndex = index + 2;
                var ind2set = testName;
                if (ind2set.length==4){
                    ind2set = "0"+testName;
                }
                workshifts[testName] = {
                    start: testName,
                    style: rawData["A" + (realIndex)].s,
                    distribution: _.omit(row, self.fioField)
                };
                findEnd.push(testName);
            }
        })
        for (var cellName in rawData) {
            var cell = rawData[cellName];
            if (!_.isEmpty(cell.s) && !_.isEmpty(cell.w) && findEnd.indexOf(cell.w) != -1) {
                var nextInRow = cellName.replace(/[0-9]+/g, '') + (Number(cellName.replace(/[^0-9]+/g, '')) + 1);
                var nextCell = rawData[nextInRow];
                if (nextCell && !_.isEmpty(nextCell.w) && nextCell.w.indexOf(":") == -1 && (Number(nextCell.w) + '') == nextCell.w) {
                    if (_.isEqual(workshifts[cell.w].style.fgColor, nextCell.s.fgColor)) {
                        workshifts[cell.w].end = nextCell.w;
                        workshifts[cell.w].color = "#" + nextCell.s.fgColor.rgb;
                        findEnd.splice(findEnd.indexOf(cell.w), 1);
                    }
                }
            }
        }
        return workshifts;
    }



    self.workTemplatesWithUsers = function(workshifts, json) {
        var templates = {},
            workShiftNames = _.keys(workshifts);
        _.values(workshifts).forEach(function(ws) {
            if (ws.end == '8') {
                templates[ws.start] = {
                    Name: "Пятидневка c " + ws.start,
                    Type: "exact5days",
                    WorkDays: 5,
                    Weekends: 2
                }
            }
        })
        var byUsers = {};
        json.forEach(function(row, index) {
            if (row[self.fioField] && row[self.fioField].match(self.nameRegexp)) {
                var set = {
                    vocations: [],
                    illness: [],
                    shifts: []
                }
                var nextRow = json[index + 1];
                for (var rowInd in row) {
                    var value = row[rowInd];
                    if (value == 'о' && rowInd.indexOf("_")==-1) {
                        set.vocations.push(rowInd);
                    } else if (value == 'б') {
                        set.illness.push(rowInd);
                    } else if (workShiftNames.indexOf(value) != -1 && nextRow[rowInd] == '8') {
                        set.shifts.push(value);
                        set.shifts = _.uniq(set.shifts);
                    }
                }
                byUsers[row[self.fioField]] = set;
            }
        })
        return {
            users: byUsers,
            templates: templates
        };
    }


    self.compact = function(json, done) {
        var users = self.users(json.users);
        var circles = self.circles(json.circles);
        var answer = { usersWithCircles: {} };
        circles.forEach(function(circleName, index) {
            answer.usersWithCircles[circleName] = users[index];
        })
        var workshifts = self.worshifts(json.users, json.rawData);
        answer.workshifts = workshifts;
        answer.workTemplatesWithUsers = self.workTemplatesWithUsers(workshifts, json.users);

        return done(null, answer);
    }

    self.parse = function(year, month, fileName, done) {
        self.year = year;
        self.month = month;
        self.loadFile(fileName, function(err, json) {
            self.compact(json, function(err, result) {
                return done(err, result);
            })
        })
    }

    return self;
})



var Import = (new function() {
    var self = this;

    var shiftModel = mongoose.model("workshift");
    var templateModel = mongoose.model("worktemplate");
    var circleModel = mongoose.model("circle");
    var userModel = mongoose.model("user");
    var departmentModel = mongoose.model("department");
    var monthdistributemodel = mongoose.model("monthdistribute");


    self.importPreview = function(year, month, filePlace, done) {
        ExcelParser.parse(year, month, filePlace, done);
    }


    self.clearData = function(done) {
        async.each(["workshift", "circle", "department", "user", "manpower_distribution", "monthdistribute", "worktemplate"], function(modelName, next) {
            var q = {};
            if (modelName == 'user') {
                q = { Roles: { $ne: ["admin"] } };
            }
            mongoose.model(modelName).remove(q).exec(next);
        }, done);
    }

    self.adminUser = null;

    self.month = null;
    self.year = null;
    self.departmentId = null;



    self.importWorkShiftsWithDistribution = function(data, done) {
        var shifts = _.values(data);
        var indexed = {};
        async.each(shifts, function(wShift, next) {
            var m = new shiftModel({
                TimeStart: (wShift.start.length==4)? "0"+wShift.start:wShift.start,
                TimeEnd: moment(wShift.start, "HH:mm").add((Number(wShift.end) + 1), "hours").format("HH:mm"),
                OwnerId: self.adminUser,
                IsPublic: true,
                Color: wShift.color
            })
            indexed[wShift.start] = m._id;
            m.save(next);
        }, function(err) {
            var distribution = new monthdistributemodel({
                Month: self.month,
                Year: self.year,
                Type: "department",
                CreatedBy: self.adminUser,
                DepartmentId: self.departmentId,
            })
            var DataObjects = [];
            shifts.forEach(function(shift) {
                var dObj = { type: "start", time: (shift.start.length==4)?"0"+shift.start:shift.start };
                var start = moment("01-" + self.month + "-" + self.year, "DD-MM-YYYY");
                var brCounter = 100;
                while (((start.month() + 1) + '') == (self.month + '')) {
                    if (--brCounter < 0) throw "Break counter";
                    var indDate = start.format("D") + '';
                    dObj[start.format("Ddd")] = (shift.distribution[indDate]) ? Number(shift.distribution[indDate]) : 0;
                    start = start.add(1, "day");
                }
                DataObjects.push(dObj);
            })
            shifts.forEach(function(shift) {
                var dObj = { type: "end", time: moment(shift.start, "HH:mm").add((Number(shift.end) + 1), "hours").format("HH:mm") };
                var start = moment("01-" + self.month + "-" + self.year, "DD-MM-YYYY");
                var brCounter = 100;
                while (((start.month() + 1) + '') == (self.month + '')) {
                    if (--brCounter < 0) throw "Break counter";
                    var indDate = start.format("D") + '';
                    dObj[start.format("Ddd")] = (shift.distribution[indDate]) ? Number(shift.distribution[indDate]) : 0;
                    start = start.add(1, "day");
                }
                DataObjects.push(dObj);
            })
            distribution.DataObjects = DataObjects;
            distribution.save(done);
        });
    }

    self.importCirclesWithUsers = function(circlesInfo, done) {
        var circles = {};
        async.each(_.keys(circlesInfo), function(cName, next) {
            var model2save = new circleModel({
                Name: cName,
                IsInGeneral: true,
                DepartmentId: self.departmentId
            });
            circles[cName] = model2save._id;
            model2save.save(next);
        }, function(err) {
            if (err) return next(err);
            async.each(_.keys(circlesInfo), function(circleName, cb1) {
                async.each(circlesInfo[circleName], function(userToSave, cb2) {
                    var circleId = circles[circleName];
                    var us = new userModel({
                        NameUser: userToSave,
                        Circles: [circleId],
                        DepartmentId: self.departmentId,
                        IsInCircleDistribution: true,
                        IsInDistribution: true,
                        Roles: ["worker"]
                    })
                    us.save(cb2);
                }, cb1)
            }, done)
        });
    }



    self._ranges = function(arr){
        var rep = _.uniq(_.map(arr,Number));
        var ranges = [], start = _.first(rep), end = _.last(rep);
        rep.forEach(function(num){
            if ((end+1)==num){
                end++;
            } else {
                ranges.push({
                    start:start,
                    end:end
                })
                start = num; end = num;
            }
        })
        return ranges;
    }

    self.assignTemplates = function(data,done){
        async.eachSeries(_.keys(data.users),function(userName,next){
            userModel.findOne({NameUser:userName}).exec(function(err,userToModify){
                if (!userToModify){
                    return  next();
                }
                var data2parse = data.users[userToModify.NameUser];
                var set = [], read = [];
                data2parse.shifts.forEach(function(tStart){
                    if (tStart.length==4) tStart = "0"+tStart;
                    set.push(self.templates[tStart]._id);
                    read.push(self.templates[tStart].Name);
                })
                userToModify.WorkTemplates = set;
                userToModify.WorkTemplatesView = read;
                var vocations = [];
                var toreparse = data2parse.vocations.concat(data2parse.illness)
                if (!_.isEmpty(toreparse)){
                    var ranges = self._ranges(toreparse);
                    ranges.forEach(function(range){
                        vocations.push({
                             "DayStart" : moment([range.start,self.month,self.year].join("."),"D.M.YYYY").format("DD.MM.YYYY"),
                             "DayEnd" :  moment([range.end,self.month,self.year].join("."),"D.M.YYYY").format("DD.MM.YYYY"),
                             "IsAllDay" : true,
                             "IsOneTime" : true,
                             "TimeStart" : "00:00",
                             "TimeEnd" : "00:00",
                             "WeekDays" : [],
                             "Info" : "Vacations"
                        })
                    })

                    userToModify.Vacations = vocations;
                    console.log(userToModify.Vacations);
                }

                userToModify.save(next);    
            })
        },done);
    }

    self.createDepartment = function(departmentId, done) {
        if (departmentId) {
            self.departmentId = departmentId;
            return done();
        }
        var dep = new departmentModel({
            Name: "Основной"
        });
        dep.save(function(err) {
            self.departmentId = dep._id;
            return done(err);
        })
    }

    self.templates = {};

    self.importTemplates = function(data, done) {
        var workshiftsIndexed = {};
        mongoose.model("workshift").find().lean().exec(function(err, shifts) {
            shifts.forEach(function(wShift) {
                workshiftsIndexed[wShift.TimeStart] = wShift;
            })
            var toSave = [];
            for (var start in data.templates) {
                if (start.length==4) {
                    start2set = "0"+start;
                } else {
                    start2set = start;
                }
                var shift2set = workshiftsIndexed[start2set];
                var newT = new templateModel(_.merge(data.templates[start], {
                    OwnerId:self.adminUser,
                    WorkShifts: [{
                        WorkShiftReadable: [shift2set.TimeStart, shift2set.TimeEnd].join(" - "),
                        WorkShiftId: shift2set._id,
                        OwnerId: self.adminUser,
                        MinCount: 5,
                        MaxCount: 5
                    }]
                }))
                self.templates[start2set] = newT;
                toSave.push(newT);
            }
            async.each(toSave, function(tS, next) {
                tS.save(next);
            }, done);
        })
    }

    self.import = function(departmentId, year, month, filePlace, done) {
        self.month = month;
        self.year = year;
        self.importPreview(year, month, filePlace, function(err, data) {
            if (err) return done(err);
            self.clearData(function(err) {
                if (err) return done(err);
                self.createDepartment(departmentId, function(err) {
                    if (err) return done(err);
                    userModel.findOne({ Roles: ["admin"] }).exec(function(err, admin) {
                        if (!admin) return done("Администратор не найден");
                        self.adminUser = admin._id;
                        self.importCirclesWithUsers(data.usersWithCircles, function(err) {
                            self.importWorkShiftsWithDistribution(data.workshifts, function(err) {
                                self.importTemplates(data.workTemplatesWithUsers, function(err) {
                                    self.assignTemplates(data.workTemplatesWithUsers, function(err) {


                                    })
                                })
                            })
                        })
                    })
                })
            })
        })
    }


    return self;
})


/*
Import.import(null, 2018, 9, __base + "data/9.xlsx", function(err, data) {
    if (err) console.log(err);

})
*/



module.exports = Import;