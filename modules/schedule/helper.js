var mongoose = require("mongoose");
var async = require("async");
var _ = require('lodash');
var moment = require("moment");
var config = require(__base + "config.js");
var socket = require(__base + "lib/socket.js");


var SubCirclesAuto = (new function() {
    var self = this;

    self.createHidden = function(departmentId, done) {
        var circleModel = mongoose.model("circle");
        var newCircle = new circleModel({
            Name: "[HIDDEN]",
            IsInGeneral: true,
            DepartmentId: departmentId
        })
        newCircle.save(function(err) {
            if (err) return done(err);
            mongoose.model("user").update({ DepartmentId: departmentId }, { $addToSet: { Circles: newCircle._id } }, { multi: true }).exec(function(err) {
                return done(err);
            });
        })
    }

    self.removeHidden = function(departmentId, done) {
        var circleModel = mongoose.model("circle");
        circleModel.find({ DepartmentId: departmentId }).exec(function(err, circles) {
            var hiddenOne = _.find(circles, { Name: "[HIDDEN]" });
            if (!_.isEmpty(hiddenOne)) {
                var unhiddenOne = _.first(_.filter(circles, function(c) {
                    return c.Name != "[HIDDEN]";
                }))
                if (_.isEmpty(unhiddenOne)) {
                    return done(err);
                }
                mongoose.model("user").update({ DepartmentId: departmentId }, { $pull: { Circles: hiddenOne._id } }, { multi: true }).exec(function(err) {
                    mongoose.model("user").update({ DepartmentId: departmentId }, { $addToSet: { Circles: unhiddenOne._id } }, { multi: true }).exec(function(err) {
                        hiddenOne.remove(done);
                    })
                });
            } else {
                return done(err);
            }
        })
    }


    self.ensureUsersInHidden = function(departmentId, circleId, done){
        console.log("updaeting hidden ",{DepartmentId:departmentId},{$set:{Circles:[circleId]}});
        mongoose.model("user").update({DepartmentId:departmentId},{$set:{Circles:[circleId]}}, {multi:true}).exec(done);
    }

    self.checkSubCircles = function(departmentId, done) {
        mongoose.model("circle").find({ DepartmentId: departmentId }).lean().exec(function(err, circles) {
            if (_.isEmpty(circles)) {
                return self.createHidden(departmentId, done);
            }
            var hidden = _.find(circles, { Name: "[HIDDEN]" });
            if (!_.isEmpty(hidden)) {
                if (circles.length > 1){
                    return self.removeHidden(departmentId, done);    
                } else {
                    return self.ensureUsersInHidden(departmentId, hidden._id, done);    
                }                
            }
            return done(err);
        })
    }

    self.getUsersForDistribution = function(departmentId, done) {
        mongoose.model("circle").find({ DepartmentId: departmentId }, "_id").lean().exec(function(err, circles) {
            if (err) return done(err);
            mongoose.model("user").find({ Circles: { $in: _.map(circles, "_id") } }).lean().exec(function(err, users) {
                if (err) return done(err);
                var groupped = {};
                users.forEach(function(user) {
                    !_.isEmpty(user.Circles) && user.Circles.forEach(function(circleId) {
                        var cId = circleId.toString();
                        if (!groupped[cId]) {
                            groupped[cId] = [];
                        }
                        if (user.IsInDistribution) { // Здесь исключать новичков IsGroupNew  IsNew
                            groupped[cId].push(user._id.toString());
                        }
                    })
                })
                var counters = {};
                for (var cId in groupped) {
                    counters[cId.toString()] = groupped[cId].length;
                }
                return done(err, counters);
            })
        })
    }

    self.getAuto = function(departmentId, month, year, done) {
        var monthDistribute = mongoose.model("monthdistribute");
        monthDistribute.findOne({ Type: "department", Month: month, Year: year, DepartmentId:departmentId }).exec(function(err, department) {
            monthDistribute.find({ Type: "circleauto", Month: month, Year: year }).exec(function(err, current) {
                mongoose.model("circle").find({ DepartmentId: departmentId }).lean().exec(function(err, circles) {
                    var answer = {}, toAdd = [];
                    circles.forEach(function(circle){
                        var existed = _.find(current,{CircleId:circle._id});
                        if (existed){
                            answer[circle._id.toString()] = existed;
                        } else {
                            var adder = new monthDistribute({
                                Month:month,
                                Year:year,
                                Type:"circleauto",
                                DepartmentId:departmentId,
                                CircleId:circle._id,
                                AllowedOverWork:department.AllowedOverWork,
                                NormalWorkHours:department.NormalWorkHours,
                            });
                            toAdd.push(adder);
                        }
                    })
                    if (_.isEmpty(toAdd)) return done(err,answer);
                    async.each(toAdd,function(ad,cb){
                        ad.save(function(err){
                            if (err) return cb(err);
                            answer[ad.CircleId.toString()] = ad;
                            return cb();
                        })
                    },function(err){
                        return done(err,answer)
                    })
                })
            })
        })
    }


    self.balance = function(counters, num) {
        var distribution = _.values(counters);
        var sum = distribution.reduce((sum, value) => sum + value, 0);
        var percentages = distribution.map(value => value / sum);
        var goalDistribution = percentages.map(percentage => num * percentage);
        var result = goalDistribution.map(value => Math.round(value));
        var answer = {},
            remain = num,
            ind = 0;
        for (var k in counters) {
            var r = result[ind];
            answer[k] = r;
            remain = remain - r;
            if (remain < 0) {
                answer[k]--;
            }
            ind++;
        }
        if (remain > 0) {
            answer[_.last(_.keys(answer))] += remain;
        }
        return answer;
    }

    self.makeDataObjects = function(counters, shablon) {
        var template = _.cloneDeep(shablon);
        var result = [];
        shablon.forEach(function(row) {
            var ns = {};
            for (var key in row) {
                if (key == 'type' || key == 'time') {
                    ns[key] = row[key];
                } else {
                    ns[key] = self.balance(counters, row[key]);
                }
            }
            result.push(ns);
        })
        return result;
    }


    self.distribute = function(departmentId, month, year, done) {
        var monthDistribute = mongoose.model("monthdistribute");
        self.checkSubCircles(departmentId, function(err) {
            if (err) return done(err);
            self.getUsersForDistribution(departmentId, function(err, counters) {
                monthDistribute.findOne({ Month: month, Year: year, Type: "department" }).exec(function(err, departmentDistribution) {
                    var divided = self.makeDataObjects(counters, departmentDistribution.DataObjects);
                    self.getAuto(departmentId, month, year, function(err, autos) {
                        var toUpdate = [];
                        for (var circleId in autos){
                            var cdivided = _.cloneDeep(divided), res = [];
                            cdivided.forEach(function(row,ind){
                                var pu = {};
                                for (var k in row){
                                     if (k == 'type' || k == 'time') {
                                        pu[k] = row[k];
                                    } else {
                                        pu[k] = row[k][circleId];
                                    }
                                }
                                res.push(pu);
                            })
                            autos[circleId].DataObjects = res;
                            toUpdate.push(autos[circleId]);
                        }
                        async.each(toUpdate,function(m,cb){
                            m.save(cb);
                        },done);
                    })
                })
            })
        })
    }



    return self;
})






module.exports = (new function() {
    var self = this;


    self.correctDistribution = function(distributionId, userId, done) {

    }

    self.announceChange = function(circles, userId) {
        var params = {};
        if (circles) {
            if (!_.isArray(circles)) circles = [circles];
            if (!_.isEmpty(circles)) {
                params.circles = circles;
            }
        }
        if (userId) {
            params.user = userId;
        }
        if (!_.isEmpty(params)) {
            mongoose.model("user").findOne({ _id: userId }, "NameUser").lean().exec(function(err, userInfo) {
                mongoose.model("circle").find({ _id: { $in: circles } }, "Name").lean().exec(function(err, circles) {
                    params.infoString = userInfo.NameUser + " изменил распределение для круга: " + _.map(circles, "Name").join(", ");
                    socket.emitEventAll("distribution-change", params);
                })
            })
        }
    }



    self._circles = function(depId, circleId, done) {
        if (!depId) {
            if (!_.isArray(circleId)) {
                circleId = [circleId];
            }
            return done(null, circleId);
        }
        mongoose.model("circle").find({ DepartmentId: depId }, "_id").lean().exec(function(err, circles) {
            if (_.isEmpty(circles)) return done("Круги не найдены");
            return done(err, circles);
        })
    }

    self.manpowerDistribute = function(month, year, depId, circleId, userId, done, isDebug) {
        self._circles(depId, circleId, function(err, circles) {
            async.each(circles, function(circleId, next) {
                self.doManpowerDistribute(month, year, circleId, userId, next, isDebug);
            }, done);
        })
    }

    self.checkLocked = function(q,done){
        mongoose.model("circle").findOne({_id:q.circle_id}).lean().exec(function(err,circle){
            var query = {
                Month:Number(q.month),
                Year:Number(q.year),
                DepartmentId:circle.DepartmentId,
                CircleId:{$in:[null,circle._id]}
            };
            mongoose.model("lock").findOne(query).exec(function(err,lock){
                return done(err,!_.isEmpty(lock));
            })
        })
    }

    self.doManpowerDistribute = function(month, year, circleId, userId, done, isDebug) {
        var manpower = mongoose.model("manpower_distribution");
        var monthdistribute = mongoose.model("monthdistribute");
        var toRemove = { month: month, year: year };
        if (!_.isEmpty(circleId)) {
            toRemove.circle_id = circleId;
        }
        self.checkLocked(toRemove,function(err,isLocked){
            if (isLocked){
                return done();  
            } 
            manpower.remove(toRemove).exec(function(err) {
                var prog = __base + 'bin/manpower-distributer';
                const { spawn } = require('child_process');
                var args = ["--month", "01-" + month + "-" + year, "--mongodb", config.mongoUrl];
                if (isDebug){
                    args = args.concat(["--dont-maximize-worktime"]);
                }
                if (!_.isEmpty(circleId)) {
                    args = args.concat(["--circle_id", circleId]);
                }
                console.log(args, "ARGS");
                const ls = spawn(prog, args);
                ls.stdout.on('data', (data) => {
                    //console.log(`stdout: ${data}`);
                });
                ls.stderr.on('data', (data) => {
                    console.log(`stderr: ${data}`);
                });
                ls.on('close', (code) => {
                    console.log(`child process exited with code ${code}`, _.isFunction(done));
                    self.announceChange(circleId, userId);
                    return _.isFunction(done) && done();
                });
            })
        })
    }


    self.redistributeCircle = function(month, year, circleId, userId, done) {
        console.log("redistributeCircle",month, year, circleId, userId);
        self.manpowerDistribute(month, year, null, circleId, userId, done);
    }

    self.redistributeCircleDebug = function(month, year, circleId, userId, done) {
        self.manpowerDistribute(month, year, null, circleId, userId, done, true);
    }



    self.toSubCircles = function(departmentId, month, year, done) {
        return SubCirclesAuto.distribute(departmentId, month, year, done);
    }


    return self;
})