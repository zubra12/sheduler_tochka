var _ = require('lodash');
var async = require('async');
var router   = require('express').Router();
var mongoose   = require('mongoose');
var roleCheck = require(__base + "lib/role.js");
var roleHelper = require(__base + "lib/rolehelper.js");
var crudHelper = require(__base + "lib/crud.js");  
var manPowerHelper = require("./helper.js");  
var importXlsHelper = require("./import.js");



router.get('/locks', roleCheck(["admin","manager","teamlead"]), function(req, res, next) {
	mongoose.model("lock").find({DepartmentId:req.query.DepartmentId}).lean().exec(function(err,locks){
		return res.json(locks);
	})

})

router.get('/lock', roleCheck(["admin","manager","teamlead"]), function(req, res, next) {
	var q = {DepartmentId:req.query.DepartmentId,Month:req.query.Month,Year:req.query.Year};
	if (!_.isEmpty(req.query.CircleId)){
		q.CircleId = req.query.CircleId;
	}
	var model = mongoose.model("lock");
	model.findOne(q).lean().exec(function(err,lock){
		if (!lock){
			var ins = new model(q);
			ins.save(function(err){
				if (err) return next(err);
				return res.json({});
			})
		} else {
			return res.json({});
		}
	})
})

router.get('/unlock', roleCheck(["admin","manager","teamlead"]), function(req, res, next) {
	var q = {DepartmentId:req.query.DepartmentId,Month:req.query.Month,Year:req.query.Year};
	if (!_.isEmpty(req.query.CircleId)){
		q.CircleId = req.query.CircleId;
	}
	var model = mongoose.model("lock");
	model.findOne(q).exec(function(err,lock){
		if (!lock){
			if (!_.isEmpty(q.CircleId)){
				return next("Распределение заблокировано на уровне круга");	
			} else {
				return next("Распределение не заблокировано");
			}			
		} else {
			lock.remove(function(err){
				return res.json({});
			})			
		}
	})

})


router.get('/manpower', roleCheck(["admin","manager","teamlead"]), function(req, res, next) {
	var query = {};
	query.year = req.query.year;
	query.month = req.query.month;
	var departmentId = req.query.department;
	mongoose.model("circle").find({DepartmentId:departmentId},"_id").lean().exec(function(err,circles){
		query.circle_id = {$in:_.map(circles,"_id")};
		mongoose.model("manpower_distribution").find(query).lean().exec(function(err,manpowers){
			if (err) return next (err);
			return res.json({
				list:manpowers
			});
		})
	})
})  


router.delete('/distribution', roleCheck(["admin","manager","teamlead"]), function(req, res, next) {
	var distModel = mongoose.model("monthdistribute");
	var circModel = mongoose.model("circle");
	var manPowerModel = mongoose.model("manpower_distribution");
	distModel.findOne({_id:req.body._id}).exec(function(err,remover){
		if (!remover) return next("Распределение не найдено");
		circModel.find({DepartmentId:remover.DepartmentId},"_id").lean().exec(function(err,circlesRaw){
			if (err) return next(err);
			var circlesAll = _.map(circlesRaw,"_id");	
			var remQ = {}, powerQ = {};
			if (remover.Type == "department"){
				remQ = { "Month" : remover.Month,"Year" : remover.Year,"DepartmentId":remover.DepartmentId};
				powerQ = { "circle_id" : {$in:circlesAll}, "year" : remover.Year,"month" : remover.Month};
			} else {
				remQ = { "Month" : remover.Month,"Year" : remover.Year,"DepartmentId":remover.DepartmentId, Type:{$in:["circlemanual","circleauto"]}};
				powerQ = { "circle_id" : remover.CircleId, "year" : remover.Year,"month" : remover.Month};
			}
			distModel.remove(remQ).exec(function(err){
				if (err) return next(err);
				manPowerModel.remove(powerQ).exec(function(err){
					if (err) return next(err);
					return res.json({});
				})
			})
		})

	})

})

router.get('/users', roleCheck(["admin","manager","teamlead"]), function(req, res, next) {
	var query = {};
	["DepartmentId","Circles","Groups"].forEach(function(filter){
		if (!_.isEmpty(req.query[filter])){
			query[filter] =  req.query[filter];
		}
	})
	if (!roleHelper.isAdmin(req)){
		query["DepartmentId"] = req.user.DepartmentId;
		if (!req.user.DepartmentId) return next ("Не указан круг пользователя");
		if (roleHelper.isTeamLead(req)){
			if (_.isEmpty(req.user.Circles)) return next ("Не указан субкруг пользователя");
			query["Circles"] = req.user.Circles;
		}
	}
	console.log(query);
	mongoose.model("user").find(query).lean().sort({NameUser:1}).exec(function(err,users){
		if (err) return next (err);
		return res.json(users);
	})
})  

router.get('/workshifts', roleCheck(["admin","manager","teamlead"]), function(req, res, next) {
	mongoose.model("workshift").find({DepartmentId:req.query.DepartmentId}).lean().exec(function(err,workShifts){
		if (err) return next (err);
		return res.json(workShifts);
	})
})  

router.get('/worktemplates', roleCheck(["admin","manager","teamlead"]), function(req, res, next) {
	//$or:[{OwnerId:req.user._id}]
	mongoose.model("worktemplate").find({DepartmentId:req.query.DepartmentId}).lean().exec(function(err,workTemplates){
		if (err) return next (err);
		return res.json(workTemplates);
	})
})  


router.put('/user', roleCheck(["admin","manager","teamlead"]), crudHelper.createOrUpdate("user"))
router.delete('/user', roleCheck(["admin","manager","teamlead"]), crudHelper.delete("user"));


router.get('/distributelist',  roleCheck(["admin","manager","teamlead"]), function(req, res, next) {
	var depId = roleHelper.departmentId(req,req.query.DepartmentId);
	if (!depId) {
		return next ("Не указан круг пользователя");
	}
	mongoose.model("monthdistribute").find({DepartmentId:depId}).lean().exec(function(err,distributes){
		if (err) return next (err);
		return res.json(distributes);
	})
})  


router.post('/dummyxls', roleCheck(["admin"]), function(req, res, next) {
	mongoose.model("circle").count({}).exec(function(err,Counter){
		if (Counter) return next ("Импорт тестовых данных - уже был");
		var Import = require("./import.js");
		Import.import(null, 2018, 9, __base + "data/9.xlsx", function(err, data) {
  			  if (err) return next(err);
  			  return res.json({});
		})
	})
})


var hexToRgb = function(hex){

  return hex.replace(/^#?([a-f\d])([a-f\d])([a-f\d])$/i
             ,(m, r, g, b) => '#' + r + r + g + g + b + b)
    .substring(1).match(/.{2}/g)
    .map(x => parseInt(x, 16))
}


router.post('/xls', roleCheck(["admin","manager","teamlead"]), function(req, res, next) {
	var cells = req.body.cells;
	var styles = {
		vocations:"background:#606dbc; color:#FFFFFF",
		customworkshift:"background:#1976d2; color:#FFFFFF",
		customworkshiftclosed:"background:#e64a19; color:#FFFFFF",
		precustomworkshiftclosed:"background:#8e24aa; color:#FFFFFF",
		default:"background:#FFFFFF; color:#000000",
		header:"background:#FFFFFF; color:#000000"
	};
	var result = [], stylesFromCells = {}, styleNum = 1;
	cells.forEach(function(row,index1){
		var pu = [];
		row.forEach(function(cell,index2){
			var text = "", style = "";
			if (!_.isEmpty(cell.params)){
				if (!_.isEmpty(cell.params.distribute)){
					text = cell.params.distribute.join("\r\n");
					if (!stylesFromCells[cell.params.background]){
						style = "style"+(styleNum);
						stylesFromCells[cell.params.background] = style;
						styles[style] = "background:"+cell.params.background+"; color:#000000";
						styleNum++;
					} else {
						style = stylesFromCells[cell.params.background];
					}
				} else if (!_.isEmpty(cell.params.custom)){
					text = cell.params.custom.join("\r\n");
					if (cell.text=='П'){
						style = "precustomworkshiftclosed";
					}else if (cell.text=='З'){
						style = "customworkshiftclosed";
					} else {
						style = "customworkshift";
					}
				} else if (cell.params=='vocations'){
					text = cell.text;
					style = "vocations";
				} 
			} else {
				text = cell.text;
				if (index2!=0){
					style = "default";	
				} else {
					style = "header";	
				}				
			}
			if (style){
				pu.push({value:text,style:style});	
			} else {
				pu.push({value:text});	
			}
		})
		result.push({cells:pu});
	})
	var stylesReparse = [], fonts = [], fills = [];
	var blackBorder = {style: 'thin', color: '000000'}; 
	for (var name in styles){
		var pArr = styles[name].split(";")
		var bg = "", cl = "#000000";
		if (pArr[0]){
			bg = _.last(pArr[0].split(":")).trim();
		}
		if (pArr[1]){
			cl = _.last(pArr[1].split(":")).trim();
		}
		fonts.push({label:"font"+name,color:{rgb:cl.replace("#","")}});
		fills.push({label:"fill"+name, type: 'pattern', color:{rgb:bg.replace("#","")}});
		if (name=='header'){
			stylesReparse.push({label:name,font:"bold",fill:"fill"+name,border:"blackBorder",alignment: {wrapText: 1,"horizontal": "left","vertical": "center"}});
		} else {
			stylesReparse.push({label:name,font:"font"+name,fill:"fill"+name,border:"blackBorder",alignment: {wrapText: 1,"horizontal": "center","vertical": "center"}});	
		}		
	}
	var params = {
		  sheets: {
        	'График': {
        		rows:result,
        		columns: [{ width: 25 }].concat(_.map(_.repeat(".",100).split("."),function(rk){
        			return { width: 6 };
        		})),
          	}
          },
    	  styles: { 
    	  	borders: [{label: 'blackBorder', left: blackBorder, right: blackBorder, top: blackBorder, bottom: blackBorder}],
    	  	fonts:fonts.concat([{label: 'bold', bold: true}]),
    	  	fills:fills,
    	  	cellStyles: stylesReparse
		  }
	};

	var excellent = require(__base+'/lib/forks/excellent');
	var fs = require ("fs");
	var doc = excellent.create(params);
	var modelMongo = mongoose.model("htmlexport");
	var model = new modelMongo({});
	var uniqueId = model._id.toString();
	var os = require("os");
	var fileName = os.tmpdir()+"/"+uniqueId+".xlsx";
	fs.writeFileSync(fileName, doc.file);
	return res.json({link:uniqueId+".xlsx"});
})

router.get('/xls', roleCheck(["admin","manager","teamlead"]), function(req, res, next) {
	var os = require("os");
	var fileName = os.tmpdir()+"/"+req.query.fileName;
	res.sendFile(fileName);
})



router.post('/html', roleCheck(["admin","manager","teamlead"]), function(req, res, next) {
	var modelMongo = mongoose.model("htmlexport");
	var model = new modelMongo({
		Name:req.body.Name,
		Table:req.body.Table
	})
	model.save(function(err){
		if (err) return next(err);
		return res.json({code:model._id.toString()});
	})
})


router.get('/html', function(req, res, next) {
	var model = mongoose.model("htmlexport");
	model.findOne({_id:req.query.code}).lean().exec(function(err,found){
		if (err) return next(err);
		if (!found) return next("Экспорт не найден");
		return res.json(found);
	})
})


router.post('/forceredistribute', roleCheck(["admin","manager","teamlead"]), function(req, res, next) {
	var place = req.body;
	var isDebug = (place.isdebug=='true' || place.isdebug==true)? true:false;
	var circles = function(depId,circleId,done){
		if (!_.isEmpty(circleId)) return done(null,[circleId]);
		mongoose.model("circle").find({DepartmentId:depId},"_id").lean().exec(function(err,circles){
			return done(err,_.map(circles,"_id"));
		})
	}
	circles(place.departmentId,place.circleId,function(err,circles){

		if (_.isEmpty(circles)) return next("Нет данных для распределения (кругов, субкругов)");
		async.each(circles,function(cId,cb){
			if (isDebug){
				manPowerHelper.redistributeCircleDebug(place.month,place.year,cId,req.user._id,cb);	
			} else {
				manPowerHelper.redistributeCircle(place.month,place.year,cId,req.user._id,cb);	
			}
			
		},function(err){
			if (err) return next (err);
			return res.json({});
		})
	})	
})

router.post('/distribute', roleCheck(["admin","manager","teamlead"]), function(req, res, next) {
	var data = req.body.data, model = mongoose.model("monthdistribute");
	var reparse = function(objects){
		return _.map(objects,function(obj){
			for (var key in obj){
				if (["type","time"].indexOf(key)==-1){
					obj[key] = _.isEmpty(obj[key])?0:Number(obj[key]);
				}
			}
			return obj;
		});
	}
	data.DataObjects = reparse(data.DataObjects);
	var getExisted = function(modelData, done){
		var query = {_id:modelData._id};//{Month:Number(modelData.Month),Year:Number(modelData.Year),Type:modelData.Type,DepartmentId:modelData.DepartmentId};
		if (modelData.Type=='circlemanual'){
			query.CircleId = modelData.CircleId;
		}
		model.findOne(query).exec(function(err,existed){
			if (_.isEmpty(existed) || existed.Type=='circleauto'){
				var newModel = new model(_.omit(modelData,["_id"]));
				console.log("creating new");
				return done(err,newModel);
			} else {
				for (var k in modelData){
					existed[k] = modelData[k];
				}
				return done(err,existed);
			}
			
		})
	}
	getExisted(data,function(err, model2save){
		model2save.save(function(err){
            if (model2save.Type== "department"){
            	console.log("model2save.save 2 ","department");
                manPowerHelper.toSubCircles(model2save.DepartmentId,model2save.Month,model2save.Year,function(err){
                	mongoose.model("circle").find({DepartmentId:model2save.DepartmentId}).lean().exec(function(err,circles){
                		var circleIds = _.map(circles,"_id");
						console.log("toSubCircles",circleIds);
	                	manPowerHelper.redistributeCircle(model2save.Month,model2save.Year,circleIds,req.user._id,function(err){
	                		console.log("redistributeCircle");
	                		if (err) return next(err);
	                		return res.json({Type:"department"});
	                	});
                	})
                });
            } else if (model2save.Type== "circlemanual"){
                manPowerHelper.redistributeCircle(model2save.Month,model2save.Year,model2save.CircleId.toString(),req.user._id,function(err){
              		if (err) return next(err);
	          		return res.json({Type:"circlemanual"});
                });
	        } else {
           		if (err) return next(err);
          		return res.json({Type:"unknown"});
            }
		})
	})
})  




  

module.exports = router;
