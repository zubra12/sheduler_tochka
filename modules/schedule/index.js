var DayHelper = (new function(){
    var self = this;

    self.correctByUsers = function(){
        var result = Schedule.manpowerTableData();
        result.errors = {};
        var curentOverwork = Schedule.currentDistribute().NormalWorkHours()+Schedule.currentDistribute().AllowedOverWork();
        for (var userId in result.users){
            var totalInfo = self.userHours(userId);
            result.userSummsHalfMonth[userId] = totalInfo.half;
            result.userSummsMonth[userId] = totalInfo.all;
            if (totalInfo.all>curentOverwork){
                result.errors[userId] = true;
            } else {
                result.errors[userId] = false;
            }
        }
        Schedule.manpowerTableData(result);
        self.correctNumbers();
    }

    self.sumBad = ko.observable(0);

    self.correctNumbers = function(){
        self.sumBad(0);
        var sumBad = 0;
        var result = Schedule.manpowerTableData();
        var corrections = {};
        for (var userId in result.users){
            var correct = self.distributedByUser(userId);
            for (var day in correct){
                if (!_.isEmpty(correct[day])){
                    if (!corrections[day]){
                       corrections[day] = {total:0,start:{},end:{}};
                    }
                    var start = _.first(correct[day]); var end = moment(start,"HH:mm").add( _.last(correct[day])+1,"hours").format("HH:mm");
                    if (!corrections[day].start[start]){
                        corrections[day].start[start] = 0;
                    }
                    if (!corrections[day].end[end]){
                        corrections[day].end[end] = 0;
                    }
                    corrections[day].start[start]++;
                    corrections[day].end[end]++;
                    corrections[day].total++;
                }
            }
        }
        var days = Schedule.monthDays();
        var oldStart = result.start, oldEnd = result.end;
        days.forEach(function(d){
            if (!_.has(corrections,d)){
                corrections[d] = {total:0,start:{},end:{}};
            }
        })
        var startKeys = _.keys(result.start);
        var endKeys = _.keys(result.end);
        for (var day in corrections){
            startKeys.forEach(function(sK){
                if (!corrections[day].start[sK]) corrections[day].start[sK] = 0;
            })
            endKeys.forEach(function(eK){
                if (!corrections[day].end[eK]) corrections[day].end[eK] = 0;
            })
        }
        days.forEach(function(day,index){
            var cor = corrections[day];
            if (cor){
                for (var t in cor.start){
                    var osi = 0;
                    try{
                        osi = oldStart[t][index] || 0;
                    } catch(e){
                        ;
                    }
                    if (oldStart[t] && cor.start[t]!=osi){
                        sumBad += Math.abs(cor.start[t]-osi)
                        oldStart[t][index] = cor.start[t]+" ("+osi+")";
                    }
                }
                for (var t in cor.end){
                    var osei = 0;
                    try{
                        osei = oldEnd[t][index] || 0;
                    } catch(e){
                        ;
                    }
                    if (oldEnd[t] && cor.end[t]!=osei){
                        sumBad += Math.abs(cor.end[t]-osei)
                        oldEnd[t][index] = cor.end[t]+" ("+osei+")";
                    }
                }
            }
        })
        var recalcTotals = {};
        days.forEach(function(day,index){
            recalcTotals[day] =0;
            for (var t in result.start){
                recalcTotals[day] += Number(_.last((result.start[t][index]+"").split("(")).replace(")","").trim());
            }
        })
        result.allTotals = {};
        for (var day in result.total){
            var oldOne = recalcTotals[day] || 0;
            var newOne = (corrections[day] && corrections[day].total)? corrections[day].total:0;
            result.allTotals[day] = (oldOne!=newOne)?newOne+" ("+oldOne+")":newOne;
        }
        result.corrections = corrections;
        self.sumBad(sumBad);
        Schedule.manpowerTableData(result);
    }

    self.distributedByUser = function(userId){
        var result = Schedule.manpowerTableData().users[userId] || {};
        var user = _.find(Schedule.users(),function(u){
            return u._id()==userId;
        })
        if (user && !_.isEmpty(user.CustomWorkShifts())){
            var customs = user.toJS().CustomWorkShifts;
            var circle = _.find(Schedule.circles(),function(circ){
                return user.Circles().indexOf(circ._id())!=-1;
            })
            customs.forEach(function(custom){
                var date = moment(custom.DayStart,"DD.MM.YYYY");
                if (date.format("M")==Schedule.monthChoosed() && date.format("YYYY")==Schedule.yearChoosed()){
                    var ind = date.format("Dddd");
                    if (custom.Info=="customworkshift"){
                        result[ind] = toMskTime([custom.TimeStart, moment.duration(moment(custom.TimeEnd, "HH:mm").diff(moment(custom.TimeStart, "HH:mm"))).asHours() - 1], circle.IsMskTime());
                    } else {
                        result[ind] = [];
                    }
                }
            })
        }
        if (user && !_.isEmpty(user.Vacations())){
            var vocations = user.toJS().Vacations;
            vocations.forEach(function(v){
                var start = moment(v.DayStart,"DD.MM.YYYY");
                var end = moment(v.DayEnd,"DD.MM.YYYY");
                var b = 100;
                while(start<=end && (--b>0)){
                    if (start.format("M")==Schedule.monthChoosed() && start.format("YYYY")==Schedule.yearChoosed()){
                        result[start.format("Dddd")] = [];
                    }
                    start = start.add(1,"day");
                }
            })
        }
        return result;
    }

    self.userHours = function(userId){
        var result = {
            half:0,
            all:0
        }
        var dis = self.distributedByUser(userId);
        for (key in dis){
            var d = Number(key.replace(/[^0-9]*/g,""));
            if (!_.isEmpty(dis[key])){
                if (d<=15){
                    result.half+=_.last(dis[key]);
                }
                result.all +=_.last(dis[key]);
            }
            
        }
        return result;
    }

    self.halfTotal = function(userId){
        var dis = self.distributedByUser(userId);
        var r = 0;
        for (key in dis){
            var d = Number(key.replace(/[^0-9]*/g,""));
            if (!_.isEmpty(dis[key]) && d<=15){
                r+=_.last(dis[key]);
            }
        }
        return r;
    }

    self.total = function(userId){
        var dis = self.distributedByUser(userId);
        var r = 0;
        for (key in dis){
            if (!_.isEmpty(dis[key])){
                r+=_.last(dis[key]);
            }
        }
        return r;
    }

    self.infoByDay = function(day){



        return result;
    }


    return self;
})



var Schedule = (new function() {

    var self = new Module("schedule");

    self.currentMode = ko.observable().extend({ persistUrl: "mode" });


    self.isLocked = ko.observable(false);
    self.locks = ko.observableArray();

    self.loadLocks = function(){
        self.rGet("locks",{DepartmentId:Department.mainCircle()},function(data){
            self.locks(data);
            self.checkLocked();
        })
    }

    self.lock = function(){
        self.rGet("lock",{
            DepartmentId:Department.mainCircle(),
            CircleId:Circle.choosedCircleId(),
            Month:self.monthChoosed(),
            Year:self.yearChoosed()
        },function(){
            self.loadLocks(function(){
                self.checkLocked()
            })
        })
    }

    self.unlock = function(){
        self.confirm("Убрать блокировку?",function(){
            self.rGet("unlock",{
                DepartmentId:Department.mainCircle(),
                CircleId:Circle.choosedCircleId(),
                Month:self.monthChoosed(),
                Year:self.yearChoosed()
            },function(){
                self.loadLocks(function(){
                    self.checkLocked()
                })
            })
        })
    }

    self.checkLocked = function(){
        var isClosed = !_.isEmpty(_.find(self.locks(),{Month:Number(self.monthChoosed()),Year:Number(self.yearChoosed()),DepartmentId:Department.mainCircle(),CircleId:null}));
        if (!_.isEmpty(Circle.choosedCircleId())){
            isClosed = isClosed || !_.isEmpty(_.find(self.locks(),{Month:Number(self.monthChoosed()),Year:Number(self.yearChoosed()),DepartmentId:Department.mainCircle(),CircleId:Circle.choosedCircleId()}));
        }
        self.isLocked(isClosed);
    }

    self.currentDistribute = ko.observable();

    self.monthChoosed = ko.observable(moment().format("MM"), { persist: "lastmonthdistribute" }).extend({ persist: "lastmonthdistribute" });
    self.yearChoosed = ko.observable(moment().format("YYYY"), { persist: "lastyeardistribute" }).extend({ persist: "lastyeardistribute" });

    self.existedDistributes = ko.observableArray();
    self.workshiftsStartEnd = ko.observable();

    self.useDebugFlag = ko.observable(false);


    self.forceRemove = function (){
        self.confirm("Удалить распределение?",function(){
            self.rDelete("distribution",{_id:Schedule.currentDistribute()._id()},function(){
                self.modeAction();
            })
        })
    }

    self.isExportPage = function() {
        return window.location.pathname.indexOf("guest-show") != -1;
    }

    self.loadedExport = ko.observable();

    self.exportCode = ko.observable();

    self.loadExport = function() {
        if (_.isEmpty(self.exportCode())) {
            return self.notify("Не указан код");
        }
        self.rGet("html", { code: self.exportCode() }, function(data) {
            self.loadedExport(MModels.create("htmlexport", data));
        })
    }


    self.encode = function() {
        var table = $("table.manpower-distribute").wrap('<p/>').parent().html();
        var elements = $(table);
        elements.find('*').removeAttr('data-bind');
        $(elements).contents().each(function() {
            if (this.nodeType === Node.COMMENT_NODE) {
                $(this).remove();
            }
        });
        var str = $(elements).wrap('<p/>').parent().html();
        str = str.replace(/\s+/g, " ").replace(/<!--.*?-->/g, "");
        return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,
            function toSolidBytes(match, p1) {
                return String.fromCharCode('0x' + p1);
            }));
    }

    self.decode = function(str) {
        return decodeURIComponent(atob(str).split('').map(function(c) {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));
    }

    self.distibutionName = function() {
        var dep = _.find(Department.departments(), function(d) {
            return d._id() == Department.mainCircle()
        });
        var circ = _.find(Circle.circles(), function(c) {
            return c._id() == Circle.choosedCircleId()
        })
        return _.compact([(_.isEmpty(dep)) ? "" : dep.Name(), (_.isEmpty(circ)) ? "" : circ.Name()]).join(". ") + " (" + moment([Schedule.monthChoosed(), Schedule.yearChoosed()], "M.YYYY").format("MMMM YYYY") + ")";
    }


    self.toXls = function() {
        var table = $("table.manpower-distribute").wrap('<p/>').parent();
        var result = [];
        table.find("tr").each(function(ind, tr) {
            var chunk = [];
            var tds = $(tr).find("td,th");
            tds.each(function(cInd, tdRaw) {
                var td = $(tdRaw);
                chunk.push({ text: td.text().trim(), params: td.data("xls") });
            })
            result.push(chunk);
        })
        return result;
    }

    self.exportToHtml = function() {
        self.rPost("html", {
            Name: self.distibutionName(),
            Table: self.encode()
        }, function(data) {
            window.location.pathname = "/guest-show/" + data.code;
        })
    }





    self.colors = {
        user: {},
        workshift: {}
    }

    self.indexColors = function() {
        var grps = Group.groups();
        self.colors.user = {};
        Group.groups().forEach(function(grp) {
            self.colors.user[grp._id()] = grp.Color();
        })
        self.colors.workshift = {};
        self.workshifts().forEach(function(ws) {
            self.colors.workshift[ws.TimeStart() + ws.WorkingTime()] = ws.Color();
        })
    }

    self.userColor = function(data) {
        var result = "transparent";
        if (_.isEmpty(data)) return result;
        data.Groups().forEach(function(grp) {
            if (!_.isEmpty(self.colors.user[grp])) {
                result = self.colors.user[grp];
            }
        })
        return result;
    }

    self.workColor = function(data) {
        var result = "#FFF";
        if (_.isEmpty(data)) return result;
        var indexKey = data.join("");
        if (_.isEmpty(self.colors.workshift[indexKey])) return result;
        return self.colors.workshift[indexKey];
    }



    self.handsonConfig = ko.observable();

    self.dataObjects = [];

    self.importFromExcel = function(done) {
        self.rPost("dummyxls", {}, function(data) {
            self.reloadAll();
        })
    }

    self.exportToExcel = function() {
        self.rPost("xls", { cells: self.toXls() }, function(data) {
            window.location = self.base + "xls?fileName=" + data.link;
        })
    }

    self.checkDates = function(data) {
        var dateToCheck = data.DayStart();
        return (moment(dateToCheck, "DD.MM.YYYY").format("M") == self.monthChoosed());
    }


    self.circleUsers = function(circleId) {
        return _.filter(self.users(), function(user) {
            return user.Circles().indexOf(circleId) != -1;
        })
    }

    self.circles = function() {
        if (!_.isEmpty(Circle.choosedCircleId())) {
            return [_.find(Circle.circles(), function(circle) {
                return circle._id() == Circle.choosedCircleId();
            })];
        } else {
            return Circle.activeCircles();
        }
    }



    /* Эту - когда бушка починит продолжительность у себя
        self.workColor = function(data){
            if (_.isEmpty(data)) return "#FFF";
            if (_.isEmpty(self.colors)){
                var workShifts = Schedule.workshifts();
                workShifts.forEach(function(shift){
                    self.colors[[shift.TimeStart(),shift.WorkingTime()].join("-")] = shift.Color();
                })
            }
            return _.isEmpty(self.colors[data.join("-")])? "#FFF":self.colors[data.join("-")];
        }


        self.colors = {};
        self.workColor = function(data){
            if (_.isEmpty(data)) return "#FFF";
            if (_.isEmpty(self.colors)){
                var workShifts = Schedule.workshifts();
                workShifts.forEach(function(shift){
                    self.colors[shift.TimeStart()] = shift.Color();
                })
            }
            return _.isEmpty(self.colors[_.first(data)])? "#FFF":self.colors[_.first(data)];
        }

        self.userColor = function(data){
            if (_.isEmpty(data)) return "#FFF";
            if (_.isEmpty(self.colors)){
                var workShifts = Schedule.workshifts();
                workShifts.forEach(function(shift){
                    self.colors[shift.TimeStart()] = shift.Color();
                })
            }
            return _.isEmpty(self.colors[_.first(data)])? "#FFF":self.colors[_.first(data)];
        }

    */

    self.manpowerTableData = ko.observable();




    self.generateManPowerTableData = function() {
        var result = {
            start: {},
            end: {},
            users: {},
            userSummsMonth: {},
            userSummsHalfMonth: {},
            total: {}
        }
        var days = self.monthDays();
        var initial = Schedule.currentDistribute().toJS().DataObjects;
        initial.forEach(function(dataobject) {
            result[dataobject.type][dataobject.time] = [];
            var info = _.omit(dataobject, ["type", "time"]);
            for (var day in info) {
                result[dataobject.type][dataobject.time].push(info[day] ? info[day] : "");
            }
            result[dataobject.type][dataobject.time] = result[dataobject.type][dataobject.time].concat(["", ""]);
        })
        var byUsers = {};
        var rebuilded = {};
        self.manPowers().forEach(function(mpKo) {
            var mp = mpKo.toJS();
            var info = mp.data;
            info.forEach(function(inf) {
                if (!rebuilded[inf.man]) {
                    rebuilded[inf.man] = {};
                }
                rebuilded[inf.man][moment(inf.date, "YYYY-MM-DD").format("Ddd")] = inf;
            })
        })
        for (var userId in rebuilded) {
            for (var inf in rebuilded[userId]) {
                var mD = rebuilded[userId][inf];
                var duration = mD.duration / 60 / 60;
                if (!result.userSummsMonth[userId]) {
                    result.userSummsMonth[userId] = 0;
                }
                if (!result.userSummsHalfMonth[userId]) {
                    result.userSummsHalfMonth[userId] = 0;
                }
                if (Number(moment(mD.date, "YYYY-MM-DD").format("D")) <= 15) {
                    result.userSummsHalfMonth[userId] += duration;
                }
                result.userSummsMonth[userId] += duration;
            }
        }
        var totals = {};
        self.manPowers().forEach(function(mpKo) {
            var mp = mpKo.toJS();
            self.monthDays().forEach(function(mD) {
                self.users().forEach(function(user) {
                    var userId = user._id();
                    if (!byUsers[userId]) {
                        byUsers[userId] = {};
                    }
                    if (rebuilded[userId] && rebuilded[userId][mD]) {
                        byUsers[userId][mD] = [rebuilded[userId][mD].start, rebuilded[userId][mD].duration / 60 / 60];
                        if (!totals[mD]) totals[mD] = [];
                        if (totals[mD].indexOf(userId) == -1) totals[mD].push(userId);
                    } else {
                        byUsers[userId][mD] = [];
                    }
                })
            })
        })
        self.monthDays().forEach(function(mD) {
            if (totals[mD]) {
                result.total[mD] = totals[mD];
            } else {
                result.total[mD] = [];
            }
        })
        result.users = byUsers;
        self.manpowerTableData(result);
        DayHelper.correctByUsers();
    }


    self.filterDistributes = function(type) {
        var existedCircles = _.map(Schedule.circles(),function(c){
            return c._id()
        })
        var data = _.filter(self.existedDistributes(), function(dis) {
            return dis.Type() == type && dis.Month() == self.monthChoosed() && dis.Year() == self.yearChoosed();
        });
        if (type == 'department') {
            if (_.isEmpty(data)) return null;
            return ko.toJS(_.first(data).DataObjects());
        }
        var answer = {};
        data.forEach(function(d) {
            if (existedCircles.indexOf(d.CircleId())!=-1) {
                answer[d.CircleId()] = ko.toJS(d.DataObjects());
            }
        })
        return answer;

    }
    self._sum = function(ind, key, hash, type) {
        var result = 0;
        for (var circleId in hash) {
            result += hash[circleId][ind][key] || 0;
        }
        return result;
    }

    self.depErrors = ko.observable(null);

    self.compileErrors = function() {
        var auto = self.filterDistributes("circleauto");
        var manual = self.filterDistributes("circlemanual");

        for (var k in auto){
            if (!_.has(manual,k)){
                manual[k] = _.cloneDeep(auto[k]);
            }
        }

        var dep = self.filterDistributes("department");
        if (!dep || _.isEmpty(auto) || _.isEmpty(manual)) {
            self.depErrors(null);
            return;
        }
        var errorMatrix = null;
        dep.forEach(function(row, ind) {
            var p = {};
            for (var k in row) {
                if (k != 'type' && k != "time") {
                    var autoSum = self._sum(ind, k, auto, "auto");
                    var manualSum = self._sum(ind, k, manual, "manual");
                    if (autoSum > manualSum) {
                        if (!errorMatrix) {
                            errorMatrix = [];
                        }
                        if (!errorMatrix[ind]) {
                            errorMatrix[ind] = {};
                        }
                        errorMatrix[ind][k] = {
                            must: autoSum,
                            has: manualSum
                        }
                    }
                }
            }
        })
        self.depErrors(errorMatrix);
    }


    self.customRender = function(instance, td, row, col, prop, value, CellInfo) {
        var verdict = self.failForcedValue(row, col, prop, value, instance);
        if (verdict) {
            $(td).addClass("failed");
        }
        Handsontable.renderers.NumericRenderer.apply(this, arguments);
    }

    self.workshiftTimeRender = function(instance, td, row, col, prop, value, CellInfo) {
        Handsontable.renderers.TextRenderer.apply(this, arguments);
        if (!_.isEmpty(value) && Circle.choosedCircleId()) {
            var circle = _.find(Circle.circles(), function(c) {
                return c._id() == Circle.choosedCircleId();
            });
            $(td).html(toMskTime(value, circle.IsMskTime()));
        }
    }

    self.lastError = ko.observable();

    self.failForcedValue = function(row, col, prop, value, instance) {
        var result = false;
        if (self.distributeType() == 'department') {
            if (_.isEmpty(self.depErrors())) return result;
            var current = ko.toJS(self.currentDistribute().DataObjects());
            var breakIndex = _.findIndex(current, { type: "end" });
            var rowCompare = row;
            if (rowCompare >= breakIndex) {
                rowCompare--;
            }
            if (row != breakIndex && current[rowCompare] && current[rowCompare][prop] && _.isNumber(Number(current[rowCompare][prop]))) {
                try {
                    value = self.depErrors()[rowCompare][prop];
                } catch (e) {
                    //console.log(e);
                }
                if (!_.isEmpty(value)){
                    if (current[rowCompare][prop] > value.has) {
                        result = true;
                    } 
                } 
            } 
            return result;
        } else {
            var currentAuto = self.currentAuto();
            if (_.isEmpty(currentAuto)) return result;
            var breakIndex = _.findIndex(self.currentAuto(), { type: "end" });
            var rowCompare = row;
            if (rowCompare >= breakIndex) {
                rowCompare--;
            }
            if (row != breakIndex && currentAuto[rowCompare] && currentAuto[rowCompare][prop] && _.isNumber(Number(currentAuto[rowCompare][prop]))) {
                if (!_.isNumber(Number(value))) {
                    value = 0;
                }
                if (currentAuto[rowCompare][prop] > value) {
                    result = true;
                }
            }
        }
        return result;
    }


    self.changeCurrentCell = function(row, col) {
        var instance = this;
        var value = instance.getDataAtCell(row, col);
        var prop = instance.getColHeader(col);
        if (self.distributeType() == 'department') {
            if(_.isEmpty(self.depErrors())) return;
            var current = ko.toJS(self.currentDistribute().DataObjects());
            var breakIndex = _.findIndex(current, { type: "end" });
            var rowCompare = row;
            if (rowCompare >= breakIndex) {
                rowCompare--;
            }
            if (row != breakIndex && current[rowCompare] && current[rowCompare][prop] && _.isNumber(Number(current[rowCompare][prop]))) {
                try {
                    value = self.depErrors()[rowCompare][prop];
                } catch (e) {
                    //console.log(e);
                }
                if (!_.isEmpty(value)){
                    if (current[rowCompare][prop] > value.has) {
                        self.lastError("В субкругах обязательных смен: " + value.has)
                    } else {
                        self.lastError(null);
                    }
                } else {
                        self.lastError(null);
                }
            } else {
                self.lastError(null);
            }
        } else {
            var currentAuto = self.currentAuto();
            if (_.isEmpty(currentAuto)) return;
            var breakIndex = _.findIndex(self.currentAuto(), { type: "end" });
            var rowCompare = row;
            if (rowCompare >= breakIndex) {
                rowCompare--;
            }
            if (row != breakIndex && currentAuto[rowCompare] && currentAuto[rowCompare][prop] && _.isNumber(Number(currentAuto[rowCompare][prop]))) {
                if (!_.isNumber(Number(value))) {
                    value = 0;
                }
                if (currentAuto[rowCompare][prop] > value) {
                    self.lastError("Обязательных смен: " + currentAuto[rowCompare][prop])
                } else {
                    self.lastError(null);
                }
            } else {
                self.lastError(null);
            }
        }
    }

    self.ensure = function(objects){
        var days = self.daysList();
        var result = [];
        var workshifts = self.workshiftsStartEnd();
        var readOnlyRowIndex = -1,
            currentRowIndex = -1;        
        ["start", "end"].forEach(function(place, ind) {
            workshifts[place].forEach(function(time) {
                var cur = _.find(objects,{type: place, time: time}) || {};
                var dataObject = { type: place };
                dataObject.time = time;
                days.forEach(function(day) {
                    dataObject[day] = cur[day]||'';
                })
                result.push(dataObject);
                currentRowIndex++;
            })
            if (!ind) {
                readOnlyRowIndex = ++currentRowIndex;
                result.push({});
            }
        })
        return result;
    }


    self.generateHotSettings = function() {
        var days = self.daysList();
        var workshifts = self.workshiftsStartEnd();
        var readOnlyRowIndex = -1,
            currentRowIndex = -1;

        if (!_.isFunction(self.currentDistribute()._id) || _.isEmpty(self.currentDistribute()._id())) {
            self.dataObjects = [];
            ["start", "end"].forEach(function(place, ind) {
                workshifts[place].forEach(function(time) {
                    var dataObject = { type: place };
                    dataObject.time = time;
                    days.forEach(function(day) {
                        dataObject[day] = '';
                    })
                    self.dataObjects.push(dataObject);
                    currentRowIndex++;
                })
                if (!ind) {
                    readOnlyRowIndex = ++currentRowIndex;
                    self.dataObjects.push({});
                }
            })
        } else {
            readOnlyRowIndex = _.findIndex(self.dataObjects, { type: "end" });
            self.dataObjects.splice(readOnlyRowIndex, 0, {});
            self.dataObjects = self.ensure(self.dataObjects);
        }
        var columnNames = _.keys(_.first(self.dataObjects)).splice(2);
        var columns = [{
                data: "time",
                readOnly: true,
                type: 'text',
                width: 50,
                renderer: self.workshiftTimeRender
            }],
            colNames = [""];
        columnNames.forEach(function(colName) {
            colNames.push(colName);
            columns.push({
                data: colName,
                readOnly: false,
                renderer: self.customRender
            })
        })
        var settings = {
            data: self.dataObjects,
            columns: columns,
            colHeaders: colNames,
            colWidths: 40,
            maxCols: colNames.length,
            maxRows: self.dataObjects.length,
            afterSelection: self.changeCurrentCell,
            afterDeselect: function() {
                self.lastError(null);
            },
            cells: function(row, col, prop) {
                var cellProperties = {};
                if (row === readOnlyRowIndex) {
                    cellProperties.readOnly = true;
                }
                return cellProperties;
            }
        }
        self.handsonConfig(settings);
    }

    self.reloadDistribute = function() {
        self.loadDistribute();
    }

    self.loadDistribute = function(done) {
        if (_.isEmpty(Department.mainCircle())) return _.isFunction(done) && done();
        self.rGet("distributelist", { DepartmentId: Department.mainCircle() }, function(data) {
            self.existedDistributes(_.map(data, function(distribute) {
                return MModels.create("monthdistribute", distribute);
            }))
            self.loadWorkshifts(function() {
                var starts = [],
                    ends = [];
                self.workshifts().forEach(function(w) {
                    starts.push(w.TimeStart());
                    ends.push(w.TimeEnd());
                })
                self.workshiftsStartEnd({
                    start: _.uniq(starts),
                    end: _.uniq(ends)
                })
                self.currentAuto(null);
                self.updateCurrent();
                return _.isFunction(done) && done();
            });
        })
    }

    self.distributeTitle = ko.observable("");

    self.currentAuto = ko.observable();

    self.updateCurrent = function() {
        self.distributeType(_.isEmpty(Circle.choosedCircleId()) ? "department" : "circlemanual");
        var q = { Month: Number(self.monthChoosed()), Year: Number(self.yearChoosed()), DepartmentId: Department.mainCircle(), Type: self.distributeType() };
        var forced = {};
        if (self.distributeType() == "circlemanual") {
            q.CircleId = Circle.choosedCircleId();
            var forcedQ = _.clone(q);
            forcedQ.Type = "circleauto";
            forced = _.find(self.existedDistributes(), function(possible) {
                var isMatched = true;
                for (var key in forcedQ) isMatched = isMatched && _.isEqual(forcedQ[key], possible[key]());
                return isMatched;
            });
            if (!_.isEmpty(forced)) {
                var auto = ko.toJS(forced.DataObjects());
                self.currentAuto(_.map(auto, function(d) {
                    for (var k in d) {
                        if (["type", "time"].indexOf(k) == -1) {
                            d[k] = d[k] == 0 ? "" : Number(d[k]);
                        }
                    }
                    return d;
                }));
            }
        }
        var existed = _.find(self.existedDistributes(), function(possible) {
            var isMatched = true;
            for (var key in q) isMatched = isMatched && _.isEqual(q[key], possible[key]());
            return isMatched;
        });
        if (!existed && !_.isEmpty(forced)) {
            existed = forced;
        }
        if (existed) {
            self.currentDistribute(existed);
            var dos = ko.toJS(existed.DataObjects());
            self.dataObjects = _.map(dos, function(d) {
                for (var k in d) {
                    if (["type", "time"].indexOf(k) == -1) {
                        d[k] = d[k] == 0 ? "" : Number(d[k]);
                    }
                }
                return d;
            })
        } else {
            self.currentDistribute(MModels.create("monthdistribute", {
                CreatedBy: MSite.Me()._id(),
                Month: self.monthChoosed(),
                Year: self.yearChoosed(),
                DepartmentId: Department.mainCircle(),
                Type: (_.isEmpty(Circle.choosedCircleId())) ? "department" : "circleauto",
                CircleId: Circle.choosedCircleId()
            }));
        }
        self.loadManpower(function() {
            self.daysList(self.monthDays(self.monthChoosed(), self.yearChoosed()));
            if (self.distributeType() == 'department') {
                self.compileErrors();
            } else {
                self.depErrors(null);
            }
            self.generateHotSettings();
            self.loadLocks();
        });
    }

    self.manPowers = ko.observableArray();

    self.loadManpower = function(done) {
        if (self.currentMode() != "manpower") {
            return _.isFunction(done) && done();
        }
        self.rGet("manpower", { month: self.monthChoosed(), year: self.yearChoosed(), department: Department.mainCircle() }, function(data) {
            self.manPowers(_.map(data.list, function(mp) {
                return MModels.create("manpower_distribution", mp);
            }))
            self.generateManPowerTableData();
            return _.isFunction(done) && done();
        })
    }

    self.distributeType = ko.observable("");


    self.validateNeededWorkshifts = function() {
        var errors = 0;
        if (self.distributeType() == "department") {
            return errors;
        }
        var auto = self.currentAuto();
        var newData = self.dataObjects;
        if (_.isEmpty(auto)) {
            return 0;
        }
        var toArr = function(dos) {
            return _.compact(_.map(dos, function(d) {

                return _.isEmpty(d) ? null : _.values(d).slice(2);
            }));
        }
        var arrMust = toArr(auto);
        var arrHas = toArr(newData);
        arrMust.forEach(function(arrRow, ind1) {
            arrRow.forEach(function(el, ind2) {
                if (arrMust[ind1][ind2] > arrHas[ind1][ind2]) errors += arrMust[ind1][ind2] - arrHas[ind1][ind2];
            })
        })
        return errors;
    }

    self.saveDistribution = function() {
        var current = self.currentDistribute().toJS();
        current.DataObjects = self.dataObjects;
        current.DateModified = Date.now();
        current.Type = self.distributeType();
        if (!_.isEmpty(Circle.choosedCircleId())) {
            current.CircleId = Circle.choosedCircleId();
        }
        var errors = self.validateNeededWorkshifts();
        if (errors) {
            self.confirm("Не хватает обязательных смен: " + errors, function() {
                self.rPost("distribute", { data: current }, function() {
                    self.reloadDistribute();
                })
            }, {
                cancelButtonText: "Отмена",
                confirmButtonText: "Сохранить"
            })
        } else {
            self.rPost("distribute", { data: current }, function() {
                Bus.emit("distribution-updated");
                self.reloadDistribute();
            })
        }
    }




    self.monthChoosed.subscribe(self.updateCurrent);
    self.yearChoosed.subscribe(self.updateCurrent);

    self.daysList = ko.observableArray();

    self.monthDays = function(month, year) {
        month = month || self.monthChoosed();
        year = year || self.yearChoosed();
        var days = [],
            dayPointer = moment("1-" + month + "-" + year, "DD-MM-YYYY");
        var whileCounter = 40;
        while (dayPointer.month() == (month - 1)) {
            if (--whileCounter < 0) {

                break;
            }
            days.push(Number(dayPointer.format("DD")) + "" + dayPointer.format("dd"));
            dayPointer = dayPointer.add(1, "day");
        }
        return days;
    }


    self.distributeType.subscribe(function(type) {
        if (type == 'department') {
            self.distributeTitle("Обязательные смены для подразделения");
        } else {
            self.distributeTitle("Смены для круга");
        }
    })











    self.changeMode = function(mode) {
        self.currentMode(mode);
        self.modeAction();
    }

    self.modeAction = function() {
        var mode = self.currentMode();
        if (mode == 'manpower') {
            self.loadManpower();
        } else if (mode == 'distribute') {
            self.loadDistribute();
        }
    }


    self.editCellDay = ko.observable();
    self.cellCustomWorkShift = ko.observable();




    self.existedCustomWorkshiftDate = function() {
        return moment([self.editCellDay().replace(/[^0-9]/g, ""), self.monthChoosed(), self.yearChoosed()].join("."), "D.M.YYYY").format("DD.MM.YYYY");
    }
    self.existedCustomWorkshift = function() {
        var user = self.currentUser();
        var date = self.existedCustomWorkshiftDate();
        var arr = ko.toJS(user.CustomWorkShifts());
        var existed = _.find(arr, { DayStart: date });
        var existedIndex = -1;
        if (!_.isEmpty(existed)) {
            existedIndex = _.findIndex(arr, { DayStart: date });
        }
        return {
            index: existedIndex,
            existed: existed
        };
    }

    self.editCell = function(user, day) {
        self.editCellDay(day);
        self.currentUser(user);
        var ex = self.existedCustomWorkshift();
        var existed = ex.existed;
        if (_.isEmpty(existed)) {
            var date = self.existedCustomWorkshiftDate();
            existed = {
                IsAllDay: false,
                Info: "customworkshift",
                DayStart: date
            }
        }
        self.cellCustomWorkShift(MModels.create("timerange", existed));
        $("#edit_user_day").modal("open");
    }



    self.updateUserDay = function() {
        var added = self.cellCustomWorkShift();
        var ex = self.existedCustomWorkshift();
        if (ex.index != -1) {
            var cur = self.currentUser().CustomWorkShifts();
            cur.splice(ex.index, 1, ko.mapping.fromJS(self.cellCustomWorkShift().toJS()));
            self.currentUser().CustomWorkShifts(cur);
        } else {
            self.currentUser().CustomWorkShifts.push(ko.mapping.fromJS(self.cellCustomWorkShift().toJS()));
        }
        self.updateUserAction(function() {
            $("#edit_user_day").modal("close");
            self.reDistributeCircle(function() {})
        })
    }

    self.onUserDayModalClose = function() {
        self.editCellDay("");
        self.currentUser(null);
        self.cellCustomWorkShift(null);
    }


    self.currentModal = "";

    self.onModalClose = function() {
        self.currentModal = "";
    }

    self.currentUser = ko.observable(null);
    self.userModal = "#add_edit_user_modal";

    self.filter = function() {
        return {
            DepartmentId: Department.mainCircle(),
            Groups: Group.choosedGroupId(),
            Circles: !_.isEmpty(Department.mainCircle()) ? Circle.choosedCircleId() : null,
        };
    }

    self.addUser = function() {
        var filter = self.filter(),
            initer = {};
        ["Circles", "Groups"].forEach(function(filterKey) {
            if (!_.isEmpty(filter[filterKey])) initer[filterKey] = [filter[filterKey]];
        })
        self.currentModal = self.userModal;
        self.currentUser(MModels.create("user", initer));
        $(self.userModal).modal("open");
    }

    self.editUserModal = function(data) {
        self.currentUser(data);
        self.currentModal = self.userModal;
        $(self.userModal).modal("open");
    }

    self.removeUserAction = function() {
        self.confirm("Вы собираетесь удалить пользователя", self.removeUser.bind(self, self.currentUser()));
    }

    self.reDistributeCircle = function(done) {
        self.rPost("forceredistribute", {
            circleId: Circle.choosedCircleId(),
            departmentId: Department.choosedDepartmentId(),
            month: self.monthChoosed(),
            year: self.yearChoosed(),
            isdebug:self.useDebugFlag()
        }, function(data) {
            self.loadDistribute(done);
        });
    }

    self.removeUser = function(data) {
        self.rDelete("user", { _id: data._id() }, function() {
            $(self.userModal).modal("close");
            self.loadUsers();
        });
    }

    self.updateUserAction = function(done) {
        var force = { DepartmentId: Department.mainCircle() };
        var model = _.merge(self.currentUser().toJS(), force);
        if (_.isEmpty(model.LoginUser)) {
            model.LoginUser = model.NameUser.translit();
        }
        if (_.isEmpty(model.Roles)) {
            model.Roles = ["worker"];
        }
        var circlesAll = Circle.circles();
        if (circlesAll.length==1 && _.first(circlesAll).Name()=="[HIDDEN]"){
            model.Circles = [_.first(circlesAll)._id()];
        }
        self.rPut("user", { model: JSON.stringify(model) }, function() {
            if (!_.isEmpty(self.currentModal)) {
                $(self.currentModal).modal("close");
            }
            self.loadUsers(done);
        });
    }

    self.notifyDistributionChange = function(data) {
        var currentUser = MSite.Me();
        if (data.user == currentUser._id()) return;
        var doShow = false,
            withClick = false;
        if (ModuleManager.choosed() == 'schedule') {
            withClick = true;
        }
        if (currentUser.Roles().indexOf("manager") != -1 && !_.isEmpty(_.intersection(
                data.circles,
                _.map(Circle.circles(), function(c) { return c._id(); })
            ))) {
            doShow = true;
        }
        if (currentUser.Roles().indexOf("teamlead") != -1 && !_.isEmpty(_.intersection(
                data.circles,
                currentUser.Circles()
            ))) {
            doShow = true;
        }
        if (doShow) {
            var text = data.infoString;
            var click = null;
            if (withClick) {
                click = {
                    click: "Schedule.reloadAll(Schedule.removeToasts)",
                    text: "Обновить"
                }
            }
            self.toast(text, click);
        }

    }

    self.reloadAll = function(done) {
        self.loadUsers(function() {
            self.loadDistribute(function() {
                return _.isFunction(done) && done();
            });
        });
    }

    self.updateUserAndDistributeChange = function() {
        self.loadUsers();
        self.loadDistribute();
    }

    self.init = function(done) {
        if (_.isEmpty(self.currentMode())) {
            self.currentMode("workers");
        }
        Bus.on("department-change", self.updateUserAndDistributeChange);
        Bus.on("department-change", self.loadLocks);
        Bus.on("circle-change", self.updateUserAndDistributeChange);
        Bus.on("users-list-updated", self.loadUsers);
        Bus.on("group-list-updated", self.indexColors);
        Bus.on("group-change", self.loadUsers);
        Bus.on("group-edit-finish", self.loadUsers);
        Bus.on("circle-edit-finish", self.loadUsers);
        MSocket.registerEvent("distribution-change", self.notifyDistributionChange);
        MSocket.start("distribution-change");
        self.reloadAll(done);
    }

    self.isAvailable = function() {
        return Permissions.check(["admin", "manager", "teamlead"]);
    }

    self.beforeHide = function() {}

    self.beforeShow = function() {
        self.modeAction();
    }

    self.users = ko.observableArray();

    self.loadUsers = function(done) {
        var filter = self.filter();
        var query = {};
        for (var paramName in filter) {
            if (!_.isEmpty(filter[paramName])) {
                query[paramName] = filter[paramName];
            }
        }
        self.rGet("users", query, function(list) {
            self.users(_.map(list, function(userData) {
                return MModels.create("user", userData);
            }))
            return _.isFunction(done) && done();
        })
    }

    self.editVacations = function(data) {
        self.currentUser(data);
        self.currentModal = "#edit_vacations";
        $(self.currentModal).modal("open");
    }

    self.editWorkshifts = function(data) {
        self.currentUser(data);
        self.currentModal = "#edit_workshifts";
        self.loadWorkshifts(function() {
            $(self.currentModal).modal("open");
        })
    }

    self.editWorkTemplates = function(data) {
        self.currentModal = "#edit_worktemplates";
        self.loadWorktemplates(function() {
            var userTemplates = data.WorkTemplates();
            if (_.isString(_.first(userTemplates))) {
                data.WorkTemplates(_.map(data.WorkTemplates(), function(_id) {
                    return _.find(self.worktemplates(), function(t) {
                        return t._id() == _id;
                    })
                }))
            }
            self.currentUser(data);
            $(self.currentModal).modal("open");
        })
    }


    self.workshifts = ko.observableArray();

    self.loadWorkshifts = function(done) {
        self.rGet("workshifts", { DepartmentId: Department.mainCircle() }, function(data) {
            self.workshifts(_.map(_.sortBy(data, function(obj) {
                return Number(_.first(obj.TimeStart.split(":")));
            }), function(workshift) {
                return MModels.create("workshift", workshift);
            }))
            self.indexColors();
            return _.isFunction(done) && done();
        })
    }

    self.worktemplates = ko.observableArray();
    self.backupWorkTemplates = [];


    self.afterWorktemplateMove = function(item) {
        self.worktemplates(_.map(self.backupWorkTemplates, function(d) {
            return MModels.create("worktemplate", d);
        }))
    }

    self.removeUserWorkTemplate = function(data) {
        Schedule.currentUser().WorkTemplates.remove(data);
    }


    self.loadWorktemplates = function(done) {
        self.rGet("worktemplates", { DepartmentId: Department.mainCircle() }, function(data) {
            self.backupWorkTemplates = data;
            self.worktemplates(_.map(data, function(d) {
                return MModels.create("worktemplate", d);
            }));
            return _.isFunction(done) && done();
        })
    }

    self.editStudyTime = function(data) {
        self.currentModal = "#edit_study-time";
        self.currentUser(data);
        $(self.currentModal).modal("open");
    }

    self.editCustomWorkShifts = function(data) {
        self.currentModal = "#edit_custom-shift";
        self.currentUser(data);
        $(self.currentModal).modal("open");
    }

    self.editRules = function(data) {
        self.currentModal = "#edit_rules";
        self.currentUser(data);
        $(self.currentModal).modal("open");
    }

    self.addTimeRange = function(type) {
        var initer = {};
        switch (type) {
            case "CustomWorkShifts":
                initer.IsAllDay = false;
                initer.Info = "customworkshift";
                break;
            case "Vacations":
                initer.IsAllDay = true;
                initer.Info = type;
                break;
            case "StudyTime":
                initer.IsAllDay = false;
                initer.Info = type;
                initer.IsOneTime = true;
                initer.WeekDays = ["пн", "вт", "ср", "чт", "пт"];
                initer.TimeStart = "10:00";
                initer.TimeEnd = "14:00";
                break;
        }
        self.currentUser()[type].push(MModels.create("timerange", initer));
    }

    self.removeTimeRange = function(type, data) {
        self.currentUser()[type].remove(data);
    }



    return self;
})



ModuleManager.Modules.Schedule = Schedule;




ko.bindingHandlers.lastNameOnly = {
    update: function(element, valueAccessor, allBindingsAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        if (!_.isEmpty(value)) {
            $(element).html(_.first(_.compact(value.split(/\s/))));
        }
    }
}

ko.bindingHandlers.dayRangeText = {
    update: function(element, valueAccessor, allBindingsAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        if (!_.isEmpty(value)) {
            $(element).html(_.compact(_.uniq([_.first(value)(), _.last(value)()])).join(" - "));
        }
    }
}

ko.bindingHandlers.userColor = {
    update: function(element, valueAccessor, allBindingsAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        $(element).css("background-color", Schedule.userColor(value));
    }
}



ko.bindingHandlers.handsonEditor = {
    table: null,
    init: function(element, valueAccessor, allBindingsAccessor) {
        var config = ko.utils.unwrapObservable(valueAccessor());
        setTimeout(function() {
            ko.bindingHandlers.handsonEditor.table = new Handsontable(element, config);
        }, 0)
    },
    update: function(element, valueAccessor, allBindingsAccessor) {
        var config = ko.utils.unwrapObservable(valueAccessor());
        setTimeout(function() {
            ko.bindingHandlers.handsonEditor.table.updateSettings(config);
        }, 0)
    }
}


var isInDateRange = function(momentD, ds, de) {
    return moment(ds, "DD.MM.YYYY") <= momentD && moment(de, "DD.MM.YYYY") >= momentD;
}
var toMskTime = function(inArr, isMsk) {
    if (_.isArray(inArr)) {
        var textArr = _.cloneDeep(inArr);
        if (isMsk) {
            textArr[0] = moment(textArr[0], "HH:mm").add(-2, "hours").format("HH:mm");
        }
        return textArr;
    } else {
        if (isMsk) {
            if (inArr.indexOf("-") != -1) {
                var rs = inArr.split("-");
                return _.map(rs, function(r) {
                    return moment(r.trim(), "HH:mm").add(-2, "hours").format("HH:mm");
                }).join(" - ");
            } else {
                return moment(inArr, "HH:mm").add(-2, "hours").format("HH:mm");
            }
        } else {
            return inArr;
        }
    }
}


ko.bindingHandlers["shedule-list"] = {
    table: null,
    init: function(element, valueAccessor, allBindingsAccessor) {
        var data = ko.utils.unwrapObservable(valueAccessor());
        //DayHelper.info(data);
        var user = data.user.toJS();


        var cellSet = false;
        if (!_.isEmpty(user.Vacations)) {
            var currentDay = moment([data.day.replace(/[^0-9]+/g, ""), Schedule.monthChoosed(), Schedule.yearChoosed()], "DD.M.YYYY");
            var isOnVocations = false;
            user.Vacations.forEach(function(v) {
                if (isInDateRange(currentDay, v.DayStart, v.DayEnd)) {
                    isOnVocations = true;
                }
            })
            if (isOnVocations) {
                cellSet = true;
                $(element).html("О");
                $(element).addClass("vocations");
                $(element).data("xls", "vocations")
            }
        }
        if (!cellSet && !_.isEmpty(user.CustomWorkShifts)) {
            var css = "",
                text = [];
            var currentDay = moment([data.day.replace(/[^0-9]+/g, ""), Schedule.monthChoosed(), Schedule.yearChoosed()], "DD.M.YYYY").format("DD.MM.YYYY");
            user.CustomWorkShifts.forEach(function(v) {
                if (!cellSet && v.DayStart == currentDay) {
                    css = v.Info;
                    cellSet = true;
                    if (css == 'customworkshift') {
                        text = toMskTime([v.TimeStart, moment.duration(moment(v.TimeEnd, "HH:mm").diff(moment(v.TimeStart, "HH:mm"))).asHours() - 1], data.circle.IsMskTime());
                    } else if (css == "customworkshiftclosed") {
                        text.push("З");
                    } else if (css == "precustomworkshiftclosed") {
                        text.push("П");
                    }
                }
            })
            $(element).html(text.join("<br/>"));
            $(element).addClass(css);
            $(element).data("xls", { custom: text });
        }
        if (!_.isEmpty(data.distribute) && !cellSet) {
            var show = toMskTime(data.distribute, data.circle.IsMskTime());
            $(element).html(show.join("<br/>"));
            var color = Schedule.workColor(data.distribute);
            $(element).css("background-color", color);
            $(element).data("xls", { distribute: show, background: color });
        }
    }
}


ko.bindingHandlers.timeRangeText = {
    update: function(element, valueAccessor, allBindingsAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        if (!_.isEmpty(value)) {
            $(element).html(_.compact(_.uniq([_.first(value)(), _.last(value)()])).join(" - "));
        }
    }
}



ko.bindingHandlers["date-from-day"] = {
    update: function(element, valueAccessor, allBindingsAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        var date = [value.replace(/[^0-9]/g, ""), Schedule.monthChoosed(), Schedule.yearChoosed()].join(".");
        $(element).html(moment(date, "D.MM.YYYY").format("DD.MM.YYYY"));
    }
}




ko.bindingHandlers["totalText"] = {
    update: function(element, valueAccessor, allBindingsAccessor) {
        var id = ko.utils.unwrapObservable(valueAccessor());
        var v = DayHelper.total(id);
        var max = Schedule.currentDistribute().NormalWorkHours()+Schedule.currentDistribute().AllowedOverWork();
        $(element).text(v);
        if (max<v){
            $(element).addClass("danger");
        }
    }
}




ko.bindingHandlers.weekRangeText = {
    update: function(element, valueAccessor, allBindingsAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        var days = ["пн", "вт", "ср", "чт", "пт", "сб", "вс"];
        if (!_.isEmpty(value)) {
            value = _.sortBy(value, function(el) {
                return days.indexOf(el);
            })
            var _range = function(daysChoosed, day) {
                var result = [];
                var startDaysIndex = days.indexOf(day);
                var startValuesIndex = value.indexOf(day);
                var limit = 50;
                while (days[startDaysIndex] == value[startValuesIndex] && !_.isEmpty(days[startDaysIndex])) {
                    if (--limit < 0) {
                        console.log("limit range");
                        break;
                    }
                    result.push(days[startDaysIndex]);
                    startDaysIndex++;
                    startValuesIndex++;
                }
                return result;
            }
            var popper = _.clone(value);
            var toWrite = [],
                limit = 50;
            while (!_.isEmpty(popper)) {
                if (--limit < 0) {
                    console.log("limit popper");
                    break;
                }
                var range = _range(popper, _.first(popper));
                if (range.length < 3) {
                    var single = _.first(range);
                    toWrite.push(single);
                    popper = _.difference(popper, [single]);
                } else {
                    popper = _.difference(popper, range);
                    toWrite.push([_.first(range), "-", _.last(range)].join(""));
                }
            }
            $(element).html(toWrite.join(", "));
        }
    }
}