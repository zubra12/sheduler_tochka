var mongoose = require("mongoose");
var async = require("async");
var _ = require('lodash');
var search = require(__base + "lib/search.js");
var moment = require("moment");
var helper = require("./helper.js");




var RuleIndexer = (new function(){
    var self = this;

    self.reIndexCircle = function(circleId,done){
        mongoose.model("user").find({Circles:circleId},"_id").lean().exec(function(err,users){
            if (err) return done(err);
            async.each(_.map(users,"_id"),function(userId,next){
                self.reIndexCircleUser(userId,next);
            },done);
        })
    }

    self.reIndexCircleUser = function(userId,done){
        mongoose.model("user").findOne({_id:userId}).lean().exec(function(err,user){
            if (_.isEmpty(user.Circles)){
                return mongoose.model("user").findByIdAndUpdate(user._id,{$set:{IsInCircleDistribution:false}}).exec(done);
            } else {
                mongoose.model("circle").find({_id:{$in:user.Circles}},"IsInGeneral").lean().exec(function(err,circles){
                    var crNew = false;
                    circles.forEach(function(cr){
                        crNew = crNew || cr.IsInGeneral;
                    })
                    return mongoose.model("user").findByIdAndUpdate(user._id,{$set:{IsInCircleDistribution:crNew}}).exec(done);
                })
            }
        })
    }



   self.reIndexGroup = function(groupId,done){
        mongoose.model("user").find({Groups:groupId},"_id").lean().exec(function(err,users){
            async.each(_.map(users,"_id"),function(userId,next){
                self.reIndexUser(userId,next);
            },done);
        })
    }

    self.reIndexUser = function(userId,done){
        mongoose.model("user").findOne({_id:userId}).lean().exec(function(err,user){
            if (_.isEmpty(user.Groups)){
                return mongoose.model("user").findByIdAndUpdate(user._id,{$set:{IsGroupNew:false}}).exec(done);
            } else {
                mongoose.model("group").find({_id:{$in:user.Groups}},"IsNew").lean().exec(function(err,groups){
                    var grNew = false;
                    groups.forEach(function(gr){
                        grNew = grNew || gr.IsNew;
                    })
                    return mongoose.model("user").findByIdAndUpdate(user._id,{$set:{IsGroupNew:grNew}}).exec(done);
                })
            }
        })
    }

    return self;
})



module.exports = {
    models: {
    	"dayshift":{
            "fields": {
                "Day": {
                    "type": Date,
                    "default": Date.now, 
                    "template":"form_date"
                },
                "IsSetManually":{
                    "type": Boolean,
                    "default": false
                },
                "WorkShiftId":{
                    "type": mongoose.Schema.Types.ObjectId,
                    'ref': 'workshift',
                    "default": null  
                },
                "Priority":{
                    "type": Number,
                    "default": 0
                }          
            }
        },
        "lock":{
            "fields": {
                "Month": {
                    "type": Number,
                    "default": 1, 
                    "template":"form_month"
                },
                "Year": {
                    "type": Number,
                    "default": 2018, 
                    "template":"form_year"
                },
                "Type": {
                    "type": String,
                    "default": "",
                    "variants":["department","circleauto","circlemanual"]
                },
                "DepartmentId":{
                    "type": mongoose.Schema.Types.ObjectId,
                    'ref': 'department',
                    "default": null  
                },
                "CircleId":{
                    "type": mongoose.Schema.Types.ObjectId,
                    'ref': 'circle',
                    "default": null  
                }
            } 
        },
        "htmlexport":{
            "fields": {
                "Name": {
                    "type": String,
                    "default": ""
                },
                "Table":{
                    "type": String,
                    "default": ""
                }                          
            }
        },
        "manpower_distribution":{
  			"fields": {
                "circle_id": {
                    "type": mongoose.Schema.Types.ObjectId,
                    'ref': 'circle',
                    "default": null  
                },
                "year":{
                	"type": Number,
                    "default": 2018
                },
                "month":{
					"type": Number,
	            	"default": 1  
                },
                "data":{
                	"type": [mongoose.Schema.Types.Mixed],
                    "default": null
                }          
    		}
    	},
        "timerange": {
            "fields": {
                "DayStart": {
                    "type": String,
                    "default": "01.01.2018", 
                    "template":"form_date"
                },
                "DayEnd": {
                    "type": String,
                    "default": "31.01.2018",
                    "template":"form_date"
                },
                "IsAllDay": {
                    "type": Boolean,
                    "default": true,
                },
                "IsOneTime": {
                    "type": Boolean,
                    "default": true,
                },
                "TimeStart": {
                    "type": String,
                    "default": "08:00",
                },
                "TimeEnd": {
                    "type": String,
                    "default": "19:00",
                },
                "WeekDays": {
                    "type": Array,
                    "default": [],
                    "variants": ["пн","вт","ср","чт","пт","сб","вс"]
                },
                "Info": {
                    "type": String,
                    "default": "customworkshift",
                    "variants":["precustomworkshiftclosed","customworkshiftclosed","customworkshift"]
                }
            }
        },
        "monthdistribute": {
            "fields": {
                "Month": {
                    "type": Number,
                    "default": 1, 
                    "template":"form_month"
                },
                "Year": {
                    "type": Number,
                    "default": 2018, 
                    "template":"form_year"
                },
                "Type": {
                    "type": String,
                    "default": "",
                    "variants":["department","circleauto","circlemanual"]
                },
                "CreatedBy":{
                    "type": mongoose.Schema.Types.ObjectId,
                    'ref': 'user',
                    "default": null  
                },
                "UpdatedBy":{
                    "type": mongoose.Schema.Types.ObjectId,
                    'ref': 'user',
                    "default": null  
                },
                "DepartmentId":{
                    "type": mongoose.Schema.Types.ObjectId,
                    'ref': 'department',
                    "default": null  
                },
                "CircleId":{
                    "type": mongoose.Schema.Types.ObjectId,
                    'ref': 'circle',
                    "default": null  
                },
                "DateModified":{
                    "type": Date,
                    "default": Date.now 
                },
                "AllowedOverWork":{
                    "type": Number,
                    "default": 0
                },
                "NormalWorkHours":{
                    "type": Number,
                    "default": 0
                },
                "DataObjects":{
                    "type": [mongoose.Schema.Types.Mixed],
                    "default": []
                }
            }
        },
        "user": {
            "fields": {
                "Vacations": {
                    "type": Array,
                    "default": []
                },
                "StudyTime": {
                    "type": Array,
                    "default": []
                },
                "CustomWorkShifts": {
                    "type": Array,
                    "default": []
                },
                "IsInDistribution":{
                    "type": Boolean,
                    "default": false,
                    "template" :"form_switch"
                },
                "IsInCircleDistribution":{
                    "type": Boolean,
                    "default": false,
                    "template" :"form_switch"
                },/*
                "Rules": {
                    "type": Array,
                    "default": [],
                    "variants": ["IsNew", "IsFiveDays", "IsInDistribution"]
                },
                */
                "IsNew": {
                    "type": Boolean,
                    "default": false,
                    "template" :"form_switch"
                },
                "IsGroupNew": {
                    "type": Boolean,
                    "default": false,
                    "template" :"form_switch"
                }
            }
        },
        "group": {
            "fields": {
                "IsNew": {
                    "type": Boolean,
                    "default": false,
                    "template" :"form_switch"
                }/*,
                "Rules": {
                    "type": Array,
                    "default": [],
                    "variants": ["IsNew", "IsFiveDays", "IsInDistribution"],
                    "template" :"form_array"
                }*/
            }
        }
    },
    schema: {
        user: function(schema){
            schema.pre('save', function (next) {
                var isNew = this.isNew;
                var self = this;
                if (_.isEmpty(self.Circles)) return next();
                mongoose.model("circle").find({_id:{$in:self.Circles}},"IsInGeneral").lean().exec(function(err,circles){
                    var crNew = false;
                    circles.forEach(function(cr){
                        crNew = crNew || cr.IsInGeneral;
                    })
                    if (crNew && isNew){
                        self.IsInDistribution = true;
                    }
                    return next();
                })
            });
           /* schema.post('save',function(model, done){
                var self = this;
                RuleIndexer.reIndexUser(self._id,done);
            })
        */
            return schema;
        },
        group: function(schema){
           /* schema.post('save',function(model, done){
                var self = this;
                RuleIndexer.reIndexGroup(self._id,done);
            })
            */
            return schema;
        },
        circle: function(schema){
            /*
            schema.post('save',function(model, done){
                var self = this;
                RuleIndexer.reIndexCircle(self._id,done);
            })
            */
            return schema;

        },  
        monthdistribute: function(schema){
            schema.pre('save',function(next, done){
                var self = this;
                this.DateModified = Date.now();
                return next();
            })
            return schema;
        }
    }
}




