var _ = require('lodash');
var async = require('async');
var router   = require('express').Router();
var mongoose   = require('mongoose');

var roleCheck = require(__base + "lib/role.js");
var crudHelper = require(__base + "lib/crud.js");


router.get('/list', function(req, res, next) {
	mongoose.model("department").find({}).lean().exec(function(err,departments){
		if (err) return next (err);
		return res.json(departments);
	})
})

router.put('/department', roleCheck(["admin"]), crudHelper.createOrUpdate("department"))
router.delete('/department', roleCheck(["admin"]), crudHelper.delete("department"));







var ImportWorkers = (new function(){
	var self = this;

	self.parse = function(path,done){
		var XLSX = require('xlsx');
  	    var workbook = XLSX.readFile(path, { cellStyles: true, cellNF: true });
  	    var json = _.values(XLSX.utils.sheet_to_json(workbook.Sheets[_.first(_.keys(workbook.Sheets))], { raw: false }));
  	    var nameRegexp = /[А-ЯЁ][а-яё]+\s[А-ЯЁ][а-яё]+/;
  	    var usersList = [];
        json.forEach(function(row, index) {
           	var names = _.filter(_.values(row),function(d){
           		return d.match(nameRegexp);
           	})
           	if (!_.isEmpty(names)){
           		usersList = _.uniq(usersList.concat(_.map(names,function(name){
           			return name.trim();
           		})));
           	}
        })
        return self.checkExisted(usersList,done);
	}

	self.checkExisted = function(usersList,done){
		mongoose.model("user").find({NameUser:{$in:usersList}}).lean().exec(function(err,usersExisted){
			var existed = _.map(usersExisted,"NameUser");
			var answer = [];
			usersList.forEach(function(name){
				answer.push({name:name,isExisted:existed.indexOf(name)!=-1});
			})
			return done(err,answer);
		})


	}



	return self;
})










 
var multer = require('multer');
var os = require('os');

router.post("/importworkers", roleCheck(["admin"]), multer({ dest: os.tmpdir()}).single('file'), function(req, res, next){
    if (_.isEmpty(req.file)) return next("Файл не найден");
    ImportWorkers.parse(req.file.path,function(err,data){
        if (err) return next(err);
        return res.json(data);
    })
})



router.post("/import", roleCheck(["admin"]), multer({ dest: os.tmpdir()}).single('file'), function(req, res, next){
	var userModel = mongoose.model("user");
	if (_.isEmpty(req.body.users)) return res.json({});
	async.each(req.body.users,function(uName,cb){
		var us = new 	userModel({
			NameUser:uName,
			DepartmentId:req.body.DepartmentId
		});
		us.save(cb);
	},function(err){
		if (err) return next(err);
		return res.json({});
	})



})




module.exports = router;