var Department = (new function(){

	var self = new Module("department");


	self.choosedDepartmentId = ko.observable().extend({persistUrl:"department"});

	self.mainCircle = function(){
		if (MSite.Me().Roles().indexOf("admin")==-1){
			return  MSite.Me().DepartmentId();
		} else {
			return self.choosedDepartmentId();
		}
	}

	self.initDepartmentId = function(){
		self.choosedDepartmentId(self.mainCircle());	
	}


	self.chooseDepartment = function(data){
		//var isOnOtherPage = !_.isEmpty(_.compact(MBreadCrumbs.сurrentRoute()));
		//pager.navigate("/");
		if (self.choosedDepartmentId()==data._id()){
			if (!_.isEmpty(Circle.choosedCircleId())){// || isOnOtherPage
				Circle.choosedCircleId(null);
			} else {
				self.choosedDepartmentId(null);	
			}			
		} else {
			self.choosedDepartmentId(data._id());	
			Circle.choosedCircleId(null);
		}		
		Bus.emit("department-change");
	}

	self.modalName = "#add_edit_department_modal";

	self.isAvailable = function(){
		return Permissions.check("admin");
	}

	self.departments = ko.observableArray();
	self.currentDepartment = ko.observable();

	self.editDepartmentModal = function(data){
		self.currentDepartment(data);
		self.showModal();
	}

	self.addModal = function(){
		self.currentDepartment(MModels.create("department",{}));
		self.showModal();
	}

	self.removeDepartmentAction = function(){
		self.confirm("Вы собираетесь удалить отдел",self.removeDepartment.bind(self,self.currentDepartment()));
	}

	self.removeDepartment = function(data){
		self.rDelete("department",{_id:data._id()},function(){
			$(self.modalName).modal("close");
			self.load();
		});
	}
	
	self.updateDepartment = function(){
		self.rPut("department",{model:self.currentDepartment().toJS()},function(){
			$(self.modalName).modal("close");
			self.load();
		});
	}


	self.showModal = function(){
		$(self.modalName).modal("open");
	}

	self.init = function(done){
		console.log("Department init ");
		self.load(done);
		if (!_.isEmpty(self.choosedDepartmentId())){
			Bus.emit("department-change");
		}	
		Bus.on("user-inited",self.initDepartmentId);
	}

	self.unsetDepartment = function(){
		self.choosedDepartmentId(null);
	}

	self.load = function(done){
		self.rGet("list",{},function(data){
			self.departments(_.map(data,function(dep){
				return MModels.create("department",dep);
			}))			
			self.choosedDepartmentId.valueHasMutated();
			return _.isFunction(done) && done();
		})		
	}








 	self.importFile = ko.observable(null);
    self.importStep = ko.observable(1);
    self.importWorkers = ko.observableArray();
    self.choosedImportWorkers = ko.observableArray();

    self.startImport = function(){
        self.importStep(1);
        self.importFile(null);
        pager.navigate("/import");   
	}

    self.proceedImport = function(){
        if (!self.importFile()) return self.showError("Файл не выбран");
        var byTypes = {};
        FileAPI.upload({
            url: self.base+"importworkers",
            files: { file: self.importFile() },
            complete: function(err, xhr) {
                if (err) return console.log(err);
	            var res = JSON.parse(xhr.response)
	            self.importWorkers(res);
	            self.choosedImportWorkers(_.map(_.filter(res,function(r){
	            	return !r.isExisted;
	            }),"name"))
                self.importStep(2);
                self.importFile(null);
            }
        })        
    }

    self.doImport = function(){
        self.importStep(3);
        self.rPost("import",{users:self.choosedImportWorkers(),DepartmentId:self.mainCircle()},function(){
        	Bus.emit("users-list-updated");
        	setTimeout(function(){
        		pager.navigate("/");
        	},2000)
        })
    }






	return self;
})



ModuleManager.Modules.Department = Department;