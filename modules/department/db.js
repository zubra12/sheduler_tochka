var mongoose = require("mongoose");
var async = require("async");
var moment = require("moment");
var  _   = require('lodash');

module.exports = {
	models:{
		 "department": {
	        "fields": {
	            "Name": {
	                "type": String,
            		"default": ''
	            }
        	} 
        },
		 "user": {
	        "fields": {
	           "DepartmentId": {
	   				"type": mongoose.Schema.Types.ObjectId,
	    			'ref': 'department',
	        		"default": null
		        }
	        }           
        } 
	},
	schema: {
		  department: function(schema){
            schema.pre('remove',function(next){
            	var self = this;
            	mongoose.model("circle").find({DepartmentId:self._id}).exec(function(err,circles){
            		if (_.isEmpty(circles)) return next();
            		async.each(circles,function(circle,cb){
						circle.remove(cb);
            		},next);
            	})
            })
            return schema;
          }
	}
}

var cleanUp = (new function(){
	var self = this;

	self.clearCircles = function(done){
		mongoose.model("department").find({},"_id").lean().exec(function(err,existed){
			mongoose.model("circle").find({DepartmentId:{$nin:_.map(existed,"_id")}}).exec(function(err,toRemove){
				async.each(toRemove,function(obj,next){
					obj.remove(next);
				},done)
			})
		})
	}

	self.do = function(done){
		async.parallel([
			self.clearCircles
		],done)
	}


	return self;
})


setTimeout(function(){
	cleanUp.do(function(){
		console.log("cleanUp finished");
	})

},1000)