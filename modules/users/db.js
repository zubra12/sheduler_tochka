var mongoose = require("mongoose");
var  crypto   = require('crypto');
var  _   = require('lodash');
var  search   = require(__base+"lib/search.js");

module.exports = {
	models:{
		 "user": {
	        "fields": {
	            "UserPhoto": {
	                "type": String,
	                "default": "",  
	                "subtype": "gfsfile",
	                "template": "user_avatar"
	            },	            
	            "NameUser": {
	                "type": String,
	                "default": "",     
	                "role":"name"           
	            },
	            "LoginUser": {
	                "type": String,
	                "default": null
	            },
	            "Mail": {
	                "type": String,
	                "default": ""
	            },
	            "PassHash": {
	                "type": String,
	                "default": null,
	                "select":false
	            },
	            "PassSalt": {
	                "type": String,
	                "default": "",
	                "select":false
	            },
	            "Birthday": {
	                "type": Date,
	                "default": null,
	                "template": "date"
	            },
	            "MobilePhone": {
	                "type": String,
	                "default": null,
	                "trim" : true,
	                "mask":"+7 (999) 999-9999"
	            }
        	} 
        } 
	},
	schema: {
		user: function(schema){
			schema.pre('save',function(next, done){
				var self = this;
				var similarQ = [];
				if (!_.isEmpty(self.LoginUser)){
					similarQ.push({LoginUser:self.LoginUser,_id:{$ne:self._id}});
				}
				if (!_.isEmpty(self.Mail)){
					similarQ.push({Mail:self.Mail,_id:{$ne:self._id}});
				}
				console.log("AAAAAAAAAAAAAAAAAAAAAAAAAAAA",self.NewPassword);
				if (!_.isEmpty(self.NewPassword)){
					self.password = self.NewPassword;
					self.NewPassword = null;
				}
				mongoose.model("user").findOne({$or:similarQ},"-_id LoginUser Mail").exec(function(err,similar){
					if (similar) {
						var Errs = [];
						if (!_.isEmpty(similar.Mail) && similar.Mail==self.Mail) return done("почта "+similar.Mail+' уже используется');
						if (!_.isEmpty(similar.LoginUser) && similar.LoginUser==self.LoginUser) return done("логин "+similar.LoginUser+' уже используется');
					}
					return next();
				})
			});

			schema.path('LoginUser').set(function(val){  return (val+'').toLowerCase().trim(); });
			schema.path('Mail').set(function(val){ return (val+'').toLowerCase().trim(); });
			
			

			schema.statics.searchableFields = function(){
				return ["NameUser","LoginUser","MobilePhone","Mail"];
			}

			schema.plugin(search);

			schema.virtual('password')
			.set(function(password) {
				if (password) {
					this._plainPassword = password;
					this.PassSalt = Math.random() + '';
					this.PassHash = this.encryptPassword(password);
				}
			})
			.get(function() { return this._plainPassword; });
			schema.method({
				encryptPassword : function(password) {
				  return crypto.createHmac('sha512', this.PassSalt).update(password).digest('hex'); 
				},
				checkPassword : function(password) {
				  if ((this.PassHash)&&(password))
				  		return this.encryptPassword(password+'') === this.PassHash;
				  else return false;
				}
			});
			return schema; 
		}
	}
}

