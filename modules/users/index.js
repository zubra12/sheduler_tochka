var MUsers = (new function(){

	var self = new Module("users");

	self.isAvailable = function(){
		return Permissions.check(["admin","manager","teamlead"]);
	}

	self.saveNotify = function(){
		self.notify("Информация о пользователе обновлена");
	}

	self.beforeHide = function () {
		Bus.off("modelsaved",self.saveNotify);
		Bus.off("department-change",self.show);
		Bus.off("group-change",self.show);
		Bus.off("circle-change",self.show);
        ModelTableEdit.clear();
    }

    self.beforeShow = function () {
    	Bus.on("modelsaved",self.saveNotify);
		Bus.on("department-change",self.show);
		Bus.on("group-change",self.show);
		Bus.on("circle-change",self.show);
        self.show();
    }

    self.show = function(){
    	var filter = {}, me = MSite.Me().toJS();
    	if (Permissions.check(["admin"])){
    		if (!_.isEmpty(Department.choosedDepartmentId())){
    			filter.DepartmentId = Department.choosedDepartmentId();	
    		}
    		if (!_.isEmpty(Circle.choosedCircleId())){
    			filter.Circles = Circle.choosedCircleId();	
    		}
    		if (!_.isEmpty(Group.choosedGroupId())){
    			filter.Groups = Group.choosedGroupId();	
    		}
    	} else if (Permissions.check(["manager"])){
    		filter.DepartmentId = me.DepartmentId;
    	} else {
    		filter.Circles = me.Circles;
    	}
		ModelTableEdit.initModel({
			modelName:"user",
			tableFields:["UserPhoto","NameUser","Roles"],
			editFields:["UserPhoto","NameUser","LoginUser","Mail","NewPassword","Roles"],
			readOnlyFields:[],//"NameUser","Mail""Birthday"
			sort:{NameUser:1},
			filter:filter,
			limit:100
		});
    }

	return self;
})



ModuleManager.Modules.Users = MUsers;