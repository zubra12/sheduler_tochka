var ProgressBar = (new function(){
	var self = this;

	self.progressBars = {};

	self.create = function(id,onProgress,done){
		self.progressBars[id] = {
			onProgress:onProgress,
			onComplete:function(data){
				delete self.progressBars[id];
				done(data);
			}
		}
	};

	self.onMessage = function(data){
		if (data.id && self.progressBars[data.id]){
			if (data.status == 'progress'){
				self.progressBars[data.id].onProgress(data.body);
			} else if (data.status == 'complete'){
				self.progressBars[data.id].onComplete(data.body);
			} else {
				console.log("Unknown Progress Type",data);
			}
		}
	};

	return self;
})



var MSocket = (new function(){
	
	var self = this;

	self.socket = null;

	self.events = {};

	self.isOnline = ko.observable(false);

	self.init = function(done){
		self.socket = io.connect(document.location.host);
		self.socket.off();
		self.socket.connect();
		self.registerEvent('err',function(e){
			return self.trace("Er",JSON.stringify(e));
		})
		self.start("err");
		self.registerEvent('progress',ProgressBar.onMessage);
		self.start('progress');		
		self.socket.on("disconnect",function(){
			self.isOnline(false);
		})
		self.socket.on("connect",function(){
			self.isOnline(true);
			if (!_.isEmpty(self.delayedStart)){
				self.delayedStart.forEach(function(ev){
					self.start(ev);
				})
				self.delayedStart = [];
			}
		})
		Bus.on("unload",function(){
			self.socket && self.socket.disconnect();
		})
		return done();
	}

	self.destroy = function(){
		if (!self.socket) return;
		self.socket.off();
		self.socket.disconnect();
	}

	self.emit = function(eventName,Data){
		if (!self.socket) return;
		self.socket.emit(eventName,Data);
	}

	self.delayedStart = [];

	self.start = function(eventName){
		if (!self.socket) {
			self.delayedStart.push(eventName);
			return;
		}
		self.socket.removeListener(eventName);
		if (!self.events[eventName]) {
			;
		} else {
			self.socket.on(eventName,self.events[eventName]);
		}
	}

	self.stop = function(eventName){
		if (!self.socket) return;
		if (self.events[eventName]) {
			self.socket.removeListener(eventName,self.events[eventName]);
		}
	}

	self.registerEvent = function(eventName, callback) {
		self.events[eventName] = callback;
	}

	self.deleteEvent = function(eventName, callback) {
		self.socket.removeListener(eventName,callback);
		if (self.events[eventName]) delete self.events[eventName];
	}

	return self;

})


ModuleManager.Modules.Socket = MSocket;