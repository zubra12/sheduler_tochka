var MBreadCrumbs = (new function(){

	var self = new Module("breadcrumbs");

	self.pages = ko.observableArray();

	self.css = ko.observable();


	self.init = function(done){
		Bus.on("initialnavigate",self.breadcrumbsCompute);
		Bus.on("navigate",self.breadcrumbsCompute);
		Bus.on("initauth",self.breadcrumbsCompute);
		return done();
	}

	self.params = ko.observable();

	self.сurrentPath = ko.observable("");
	self.сurrentRoute = ko.observableArray();


	self.breadcrumbsCompute = function () {
		self.сurrentPath(window.location.pathname.substring(1));
		self.сurrentRoute(self.сurrentPath().split('/'));
		if (!_.isEmpty(window.location.search)){
			self.params(window.location.search.queryObj());
		} else {
			self.params(null);
		}		
		setTimeout(self._breadcrumbsFromPages,0);
	}
	


	self.сheckPath = function(path){
		if (path=='/'){
			return _.isEmpty(self.сurrentPath());
		} else {
			return self.сurrentPath().indexOf(_.trim(path,"/"))>=0;
		}
	}

	self.сheckPathAny = function(paths){
		var R = false;
		paths.forEach(function(p){
			R = R || self.сheckPath(p);
		})
		return R;
	}
	
	self._breadcrumbsFromPages = function(){
		if (!_.isFunction(pager.activePage$)) return;
		var pages = [];
		var page = pager.activePage$();
		var maxStack = 20;
		while(page){
			if (page.valueAccessor && page.valueAccessor().breadcrumbs){
				var r = page.valueAccessor().breadcrumbs;
				pages.unshift(r);
			}
			page = page.parentPage;
			if (--maxStack<0)  break;
		}
		self.pages(pages);
		Bus.emit("pagechanged");
	}

	
	return self;
})

ModuleManager.Modules.BreadCrumbs = MBreadCrumbs;